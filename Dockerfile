FROM python:3.8.5-alpine3.12

ENV STATIC_DIR /freelo/static
WORKDIR /freelo

# Copy Freelo backend projct files
COPY FreeloApp /freelo/FreeloApp
COPY apicore /freelo/apicore
COPY staticfiles /freelo/staticfiles
COPY frontend/templates/ /freelo/frontend/templates
COPY actividades_monotributo.json /freelo/actividades_monotributo.json
COPY listado_obra_sociales.json /freelo/listado_obra_sociales.json
COPY manage.py /freelo/manage.py
COPY requirements.txt /freelo/requirements.txt
COPY .env /freelo/.env

RUN apk add --no-cache --update --virtual .build-deps build-base postgresql-dev \
    && pip install -r requirements.txt \
    && apk del .build-deps


# install tools
RUN apk add --update --no-cache nginx bash postgresql-client \
	&& python manage.py collectstatic --no-input \
        && chown -R nginx:nginx $STATIC_DIR \
	&& mkdir -p /run/nginx

# Copy Docker Files
COPY docker/docker-start.sh /freelo/docker-start.sh
COPY docker/nginx.default /etc/nginx/conf.d/default.conf


CMD ["/freelo/docker-start.sh"]
