const routes = [
  // Rutas de inicio de sesion, formulario categorizador, etc
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/simulador",
        meta: { guest: true },
        component: () => import("pages/general/PreForm.vue")
      },
      {
        path: "/resultados",
        meta: { guest: true },
        component: () => import("pages/general/Resultados.vue")
      },
      {
        path: "/test",
        component: () => import("pages/TestApi.vue")
      },
      {
        name: "inscripcion",
        path: "/inscripcion",
        meta: { guest: true },
        component: () => import("pages/general/Inscripcion.vue")
      },
      {
        name: "confirmarEmail",
        path: "/confirmaremail",
        meta: { guest: true },
        component: () => import("pages/general/ConfirmarEmail.vue")
      },
      {
        name: "activarCuenta",
        path: "/activarCuenta/:uid/:token",
        meta: { guest: true },
        component: () => import("pages/general/ActivarCuenta.vue")
      },
      {
        name: "primerLogin",
        path: "/primerlogin",
        meta: { requiresAuth: true },
        component: () => import("pages/general/PrimerLogin.vue")
      },
      {
        name: "primerLoginMonotributo",
        path: "/primerlogin/monotributo",
        meta: { requiresAuth: true },
        component: () => import("pages/general/monotributo/Monotributo.vue")
      },
      // {
      //   path: "/primerlogin/monotributo/afip",
      //   component: () => import("pages/general/monotributo/Afip.vue")
      // },
      // {
      //   path: "/primerlogin/monotributo/altafinalizada",
      //   component: () => import("pages/general/monotributo/AltaFinalizada.vue")
      // },
      {
        name: "primerLoginIngresosBrutos",
        path: "/primerlogin/ingresosbrutos",
        meta: { requiresAuth: true },
        component: () =>
          import("pages/general/ingresosbrutos/IngresosBrutos.vue")
      },
      {
        name: "primerLoginArba",
        path: "/primerlogin/ingresosbrutos/arba",
        meta: { requiresAuth: true },
        component: () => import("pages/general/ingresosbrutos/AltaArba.vue")
      },
      {
        name: "primerLoginAgip",
        path: "/primerlogin/ingresosbrutos/agip",
        meta: { requiresAuth: true },
        component: () => import("pages/general/ingresosbrutos/AltaAgip.vue")
      },
      {
        path: "/primerlogin/ingresosbrutos/altafinalizada",
        meta: { requiresAuth: true },
        component: () =>
          import("pages/general/ingresosbrutos/AltaFinalizada.vue")
      },
      {
        name: "login",
        path: "",
        meta: { guest: true },
        component: () => import("pages/general/Login.vue")
      },
      {
        name: "recuperarPasss",
        path: "/recuperarcontrasena/:uid/:token",
        meta: { guest: true },
        component: () => import("pages/general/RecuperarContrasena.vue")
      }
    ]
  },
  // Rutas una vez que ya está logeado, mis facturas, mi monotributo, etc
  {
    path: "/usuario",
    component: () => import("layouts/UserLayout.vue"),
    meta: { requiresAuth: true },
    children: [
      {
        name: "homeDashboard",
        path: "/",
        component: () => import("pages/user/Home.vue")
      },
      {
        name: "monotributo",
        path: "monotributo",
        component: () => import("pages/user/MiMonotributo.vue")
      },
      {
        name: "ingresosBrutos",
        path: "ingresosbrutos",
        component: () => import("pages/user/IIBB.vue")
      },
      {
        name: "facturas",
        path: "facturas",
        component: () => import("pages/user/MisFacturas.vue")
      },
      {
        name: "miCuenta",
        path: "micuenta",
        component: () => import("pages/user/MiCuenta.vue")
      }
    ]
  },
  {
    path: "/admin",
    component: () => import("layouts/AdminLayout.vue"),
    meta: { is_admin: true, requiresAuth: true },
    children: [
      {
        name: "adminHome",
        path: "/",
        component: () => import("pages/admin/Home.vue")
      },
      {
        name: "adminUsuarios",
        path: "usuarios/:estadousuario",
        component: () => import("pages/admin/Usuarios.vue")
      },
      {
        name: "adminUsuario",
        path: "usuarios/:estadousuario/:pk",
        component: () => import("pages/admin/Usuario.vue")
      },
      {
        name: "notificacionesAdmin",
        path: "notificaciones",
        component: () => import("pages/admin/Notificaciones.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
