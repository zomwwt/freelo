import Vue from "vue";
import VueRouter from "vue-router";
import axios from "axios";
import routes from "./routes";
import { Cookies } from "quasar";

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function(/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  Router.beforeEach((to, from, next) => {
    const loggedIn = Cookies.get("usuario");
    if (!!loggedIn) {
      // console.log(loggedIn, "cookies");
      axios.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${loggedIn.access}`;
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (!loggedIn) {
        next({ name: "login" });
      } else {
        next();
        if (to.matched.some(record => record.meta.is_admin)) {
          if (loggedIn.es_admin) {
            next();
          } else {
            next({ name: "homeDashboard" });
          }
        }
      }
    } else if (to.matched.some(record => record.meta.guest)) {
      if (!loggedIn) {
        next();
      } else {
        next({ name: "homeDashboard" });
      }
    } else {
      next();
    }
  });

  return Router;
}
