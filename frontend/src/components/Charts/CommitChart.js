import { Bar } from "vue-chartjs";
import ChartJsAnnotation from "chartjs-plugin-annotation";

export default {
  extends: Bar,
  props: ["data", "options"],
  mounted() {
    this.addPlugin({ id: "ChartJsAnnotation" });
    this.renderChart(this.data, this.options);
  }
};
