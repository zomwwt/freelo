export function ALMACENAR_ESTADO_MONOTRIBUTO(state, payload) {
  state.estadoMonotributo = payload;
}

export function ESTADO_MONOTRIBUTO_EXITO(state, payload) {
  state.estadoMonotributoExito = payload;
}

export function ESTADO_MONOTRIBUTO_ERROR(state, payload) {
  state.estadoMonotributoError = payload;
}

export function ALMACENAR_FACTURACIONES_MONOTRIBUTO(state, payload) {
  state.facturaciones = payload;
}

export function ESTADO_FACTURACIONES_EXITO(state, payload) {
  state.estadoFacturacionesExito = payload;
}

export function ESTADO_FACTURACIONES_ERROR(state, payload) {
  state.estadoFacturacionesError = payload;
}
