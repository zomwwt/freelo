export default function() {
  return {
    estadoMonotributo: null,
    estadoMonotributoExito: null,
    estadoMonotributoError: null,

    facturaciones: null,
    estadoFacturacionesExito: null,
    estadoFacturacionesError: null
  };
}
