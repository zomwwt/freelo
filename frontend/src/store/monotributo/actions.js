import axios from "axios";
import { apiRest } from "../APIs";

export function EstadoMonotributo({ commit }) {
  const endpoint = "estado-monotributo-usuario/";

  commit("ALMACENAR_ESTADO_MONOTRIBUTO", null);
  commit("ESTADO_MONOTRIBUTO_EXITO", false);
  commit("ESTADO_MONOTRIBUTO_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_ESTADO_MONOTRIBUTO", data);
      commit("ESTADO_MONOTRIBUTO_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_MONOTRIBUTO_ERROR", true);
    });
}

export function FacturacionesMonotributo({ commit }) {
  const endpoint = "facturaciones-monotributo-usuario/";

  commit("ALMACENAR_FACTURACIONES_MONOTRIBUTO", null);
  commit("ESTADO_FACTURACIONES_EXITO", false);
  commit("ESTADO_FACTURACIONES_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_FACTURACIONES_MONOTRIBUTO", data);
      commit("ESTADO_FACTURACIONES_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_FACTURACIONES_ERROR", true);
    });
}
