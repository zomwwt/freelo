export function facturasMonotributo(state) {
  let facturas = {};
  facturas = {
    tableColumns: [
      {
        name: "periodo",
        required: true,
        label: "Período",
        align: "center",
        field: row => row.name,
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "vencimiento",
        label: "Fecha de vto",
        align: "center",
        field: "vencimiento",
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "totalmes",
        label: "Total del mes",
        align: "center",
        field: "totalmes",
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "concepto",
        label: "Concepto",
        align: "center",
        field: "concepto",
        format: val => `${val}`,
        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "pago",
        label: "Pago realizado",
        align: "center",
        field: "pago",
        format: val => `${val}`,
        headerStyle: "font-size: 16px; text-transform:uppercase"
      }
    ],
    tableData: []
  };
  state.facturaciones.forEach(fact => {
    if (fact.concepto === "Cuota Mensual" || fact.concepto === "Intereses") {
      let fecha = new Date(fact.mes_facturado);
      let mes = fecha.toLocaleString("es-AR", { month: "long" });
      let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
      let year = fecha.getFullYear();
      let yearVenc = fact.fecha_vencimiento.slice(0, 4);

      let mesVenc = fact.fecha_vencimiento.slice(5, 7);

      let diaVenc = fact.fecha_vencimiento.slice(8, 11);

      let factura = {
        name: mesCapitalizado + " " + year,
        vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
        totalmes: "$ " + fact.monto_facturado,
        pagada: fact.pago_realizado,
        descargafactura: fact.cur_pdf,
        concepto: fact.concepto
      };
      facturas.tableData.unshift(factura);
    }
  });
  return facturas;
}

export function facturasMensuales(state) {
  let facturas = [];
  state.facturaciones.forEach(fact => {
    if (fact.concepto === "Cuota Mensual") {
      let fechaVencimiento = new Date(fact.fecha_vencimiento);
      let yearVenc = fact.fecha_vencimiento.slice(0, 4);

      let mesVenc = fact.fecha_vencimiento.slice(5, 7);

      let diaVenc = fact.fecha_vencimiento.slice(8, 11);

      let factura = {
        monto: fact.monto_facturado,
        pago: fact.cur_pdf,
        vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
        pagado: fact.pago_realizado
      };

      facturas.unshift(factura);
    }
  });

  if (facturas.length === 0) {
    let sinDatos = {
      monto: 0,
      vencimiento: "S/D"
    };
    facturas.push(sinDatos);
  }
  return facturas;
}

export function categoria(state) {
  if (state.estadoMonotributo) {
    let categoria = state.estadoMonotributo.categoria;
    categoria = categoria[categoria.length - 1];
    return categoria;
  }
  return "Z";
}

export function limiteFacturacionAnual(state) {
  if (state.estadoMonotributo) {
    return state.estadoMonotributo.limite_facturacion_anual;
  }
  return 0;
}

export function facturacionAcumulada(state) {
  let facturacionAcumulada = 0;
  if (state.estadoMonotributo && state.facturaciones) {
    state.facturaciones.forEach(fact => {
      if (fact.concepto === "Facturacion") {
        let year = fact.mes_facturado.slice(0, 4);
        let actualYear = new Date();
        actualYear = actualYear.getFullYear();
        if (year == actualYear) {
          facturacionAcumulada = facturacionAcumulada + fact.monto_facturado;
        }
      }
    });
  }
  return facturacionAcumulada;
}

export function facturacionPorMes(state) {
  let facturacionMes = [];
  state.facturaciones.forEach(fact => {
    if (fact.concepto === "Facturacion") {
      facturacionMes.push(fact);
    }
  });
  if (facturacionMes.length === 0) {
    let sinDatos = {
      monto_facturado: 0,
      fecha_vencimiento: "S/D"
    };
    facturacionMes.push(sinDatos);
  }
  return facturacionMes;
}

export function opcionesGrafico(state) {
  let options = {
    title: {
      display: false,
      text: "Facturación Acumulada"
    },
    legend: {
      display: false
    },
    scales: {
      xAxes: [
        {
          maxBarThickness: 70,
          gridLines: {
            color: "black",
            drawBorder: true,
            tickMarkLength: 5,
            drawOnChartArea: false
          },
          ticks: {
            fontColor: "black",
            callback: function(value) {
              return value.substr(0, 3);
            }
          }
        }
      ],
      yAxes: [
        {
          gridLines: {
            color: "black",
            tickMarkLength: 5,
            drawBorder: true,
            drawOnChartArea: false
          },
          ticks: {
            min: 0,
            beginAtZero: true,
            fontColor: "black",
            precision: 0,

            suggestedMax: state.estadoMonotributo.limite_facturacion_anual + 20,
            callback: function(value, index, values) {
              for (let val in values) {
                if (val > 1e6) {
                  return value / 1e6 + "mill";
                } else {
                  return value / 1e3;
                }
              }
            }
          }
        }
      ]
    },
    maintainAspectRatio: false,
    annotation: {
      annotations: [
        {
          id: "hline",
          drawTime: "afterDraw",
          id: "a-line-1",
          type: "line",
          mode: "horizontal",
          scaleID: "y-axis-0",
          value: state.estadoMonotributo.limite_facturacion_anual,
          borderColor: "#BE724D",
          borderWidth: 2
        },
        {
          type: "line",
          id: "call-count-1",
          mode: "horizontal",
          scaleID: "y-axis-0",
          value: state.estadoMonotributo.limite_facturacion_anual,
          borderColor: "transparent",
          label: {
            backgroundColor: "#FFF",
            fontFamily: "Poppins",
            fontSize: 12,
            fontColor: "#000",
            xPadding: 12,
            yPadding: 6,
            cornerRadius: 4,
            position: "left",
            yAdjust: -12,
            enabled: true,
            content:
              "Límite facturación anual:" +
              " $" +
              state.estadoMonotributo.limite_facturacion_anual
          }
        }
      ]
    }
  };
  return options;
}

export function datosGrafico(state, getters) {
  let datosChart = {
    labels: [],
    datasets: [
      {
        fontColor: "#fff",
        backgroundColor: [
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979",
          "#F9C979"
        ],
        data: []
      }
    ],
    option: {
      height: 300
    }
  };
  let facturaciones = getters.facturacionPorMes;
  for (let index = 0; index < facturaciones.length; index++) {
    let year = facturaciones[index].mes_facturado.slice(0, 4);
    let actualYear = new Date();
    actualYear = actualYear.getFullYear();
    if (year == actualYear) {
      let fecha = new Date(facturaciones[index].mes_facturado);
      let mes = fecha.toLocaleString("es-AR", { month: "long" });
      let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
      datosChart.labels.push(mesCapitalizado);

      datosChart.datasets[0].data[index] = Math.trunc(
        facturaciones[index].monto_facturado +
          (datosChart.datasets[0].data[index - 1] || 0)
      );
    }
  }
  return datosChart;
}
