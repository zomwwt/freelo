export function notificaciones(state) {
  let notificaciones = [];
  state.notificaciones.forEach(notif => {
    let fechaEmision = notif.fecha_emision.replace(/-/g, "/").replace(",", "");
    fechaEmision = new Date(fechaEmision);

    let tiempoActualEnUtc = new Date();
    tiempoActualEnUtc = tiempoActualEnUtc.toISOString();
    tiempoActualEnUtc = tiempoActualEnUtc
      .replace(/-/g, "/")
      .replace("T", " ")
      .replace("Z", "")
      .slice(0, -4);
    tiempoActualEnUtc = new Date(tiempoActualEnUtc);

    let diferenciaTiempo = tiempoActualEnUtc - fechaEmision;
    diferenciaTiempo /= 1000;

    let segundos = Math.abs(Math.round(diferenciaTiempo % 60));
    diferenciaTiempo = Math.floor(diferenciaTiempo / 60);

    let minutos = Math.abs(Math.round(diferenciaTiempo % 60));
    diferenciaTiempo = Math.floor(diferenciaTiempo / 60);

    let horas = Math.abs(Math.round(diferenciaTiempo % 60));
    diferenciaTiempo = Math.floor(diferenciaTiempo / 24);

    let dias = diferenciaTiempo;

    let tiempoNotificacion = "Hace ";
    if (dias) {
      tiempoNotificacion = tiempoNotificacion + dias + "d";
    } else if (horas > 0) {
      tiempoNotificacion = tiempoNotificacion + horas + "h";
    } else {
      tiempoNotificacion = tiempoNotificacion + minutos + "m";
    }
    let notificacion = {
      contenido: notif.contenido,
      titulo: notif.titulo,
      key: notif.pk_notificacion,
      tiempo: tiempoNotificacion,
      leido: notif.leido
    };
    notificaciones.unshift(notificacion);
  });
  return notificaciones;
}

export function notificacionesSinLeer(state) {
  let sinLeer = [];

  sinLeer = state.notificaciones.filter(notif => !notif.leido);
  return sinLeer.length;
}
