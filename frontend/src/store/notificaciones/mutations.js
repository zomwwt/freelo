export function ALMACENAR_NOTIFICACIONES(state, payload) {
  state.notificaciones = payload;
}

export function ESTADO_NOTIFICACIONES_EXITO(state, payload) {
  state.notificacionesExito = payload;
}

export function ESTADO_NOTIFICACIONES_ERROR(state, payload) {
  state.notificacionesError = payload;
}

export function ALMACENAR_RESPUESTA(state, payload) {
  state.respuesta = payload;
}

export function MARCAR_NOTIFICACION_LEIDA(state, payload) {
  state.notificaciones.forEach(notificacion => {
    if (notificacion.pk_notificacion === payload) {
      notificacion.leido = true;
    }
  });
}
