import axios from "axios";
import { apiRest } from "../APIs";

export function Notificaciones({ commit }) {
  const endpoint = "notificaciones-usuario/";

  commit("ALMACENAR_NOTIFICACIONES", null);
  commit("ESTADO_NOTIFICACIONES_EXITO", false);
  commit("ESTADO_NOTIFICACIONES_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_NOTIFICACIONES", data);
      commit("ESTADO_NOTIFICACIONES_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_NOTIFICACIONES_ERROR", true);
    });
}

export function MarcarNotificacionLeida({ commit, dispatch }, id) {
  const endpoint = "notificacion-marcar-leido/" + id + "/";

  return axios
    .post(apiRest + endpoint)
    .then(({ data }) => {
      commit("MARCAR_NOTIFICACION_LEIDA", id);
    })
    .catch(err => {});
}
