// Provincias
export function SET_PROVINCIAS(state, payload) {
  state.provincias = payload;
}

export function SET_PROVINCIAS_EXITO(state, payload) {
  state.provinciasExito = payload;
}

export function SET_PROVINCIAS_PENDIENTE(state, payload) {
  state.provinciasPendiente = payload;
}

export function SET_PROVINCIAS_ERROR(state, payload) {
  state.provinciasError = payload;
}

// Localidades
export function SET_LOCALIDADES(state, payload) {
  state.localidades = payload;
}

export function SET_LOCALIDADES_EXITO(state, payload) {
  state.localidadesExito = payload;
}

export function SET_LOCALIDADES_PENDIENTE(state, payload) {
  state.localidadesPendiente = payload;
}

export function SET_LOCALIDADES_ERROR(state, payload) {
  state.localidadesError = payload;
}

// Partidos
export function SET_PARTIDOS(state, payload) {
  state.partidos = payload;
}

export function SET_PARTIDOS_EXITO(state, payload) {
  state.partidosExito = payload;
}

export function SET_PARTIDOS_PENDIENTE(state, payload) {
  state.partidosPendiente = payload;
}

export function SET_PARTIDOS_ERROR(state, payload) {
  state.partidosError = payload;
}

// Actividades monotributo
export function SET_ACTIVIDADES(state, payload) {
  state.actividades = payload;
}

export function SET_ACTIVIDADES_EXITO(state, payload) {
  state.actividadesExito = payload;
}

export function SET_ACTIVIDADES_PENDIENTE(state, payload) {
  state.actividadesPendiente = payload;
}

export function SET_ACTIVIDADES_ERROR(state, payload) {
  state.actividadesError = payload;
}

// Obras sociales
export function SET_OBRAS_SOCIALES(state, payload) {
  state.obrasSociales = payload;
}

export function SET_OBRAS_SOCIALES_EXITO(state, payload) {
  state.obrasSocialesExito = payload;
}

export function SET_OBRAS_SOCIALES_PENDIENTE(state, payload) {
  state.obrasSocialesPendiente = payload;
}

export function SET_OBRAS_SOCIALES_ERROR(state, payload) {
  state.obrasSocialesError = payload;
}
