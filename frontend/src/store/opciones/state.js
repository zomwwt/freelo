export default function() {
  return {
    // Provincias, localidades y partidos/departamentos - opciones que tiene el usuario, traidas de api publica
    provincias: null,
    provinciasExito: null,
    provinciasPendiente: null,
    provinciasError: null,

    localidades: null,
    localidadesExito: null,
    localidadesPendiente: null,
    localidadesError: null,

    partidos: null,
    partidosExito: null,
    partidosPendiente: null,
    partidosError: null,

    // Obras sociales REST API
    obrasSociales: null,
    obrasSocialesExito: null,
    obrasSocialesPendiente: null,
    obrasSocialesError: null,

    // Actividades Monotributo REST API
    actividades: null,
    actividadesExito: null,
    actividadesPendiente: null,
    actividadesError: null,

    // Opciones categorizador
    pregunta1: [
      {
        label: "Prestación de servicios",
        value: 1
      },
      {
        label: "Venta de productos",
        value: 2
      },
      {
        label: "Alquiler de propiedad/maquinaria de trabajo",
        value: 3
      }
    ],
    pregunta2: [
      {
        label: "Hasta 17395 por mes",
        value: 208739.25,
        cuota: 168.97,
        cuotaOS: 1955.68,
        categoria: "A"
      },
      {
        label: "Entre 17395 y 26092 por mes",
        value: 313108.87,
        cuota: 325.54,
        cuotaOS: 2186.8,
        categoria: "B"
      },
      {
        label: "Entre 26092 y 34790 por mes",
        value: 417478.51,
        cuota: 566.54,
        cuotaOS: 2499.91,
        categoria: "C"
      },
      {
        label: "Entre 34790 y 52185 por mes",
        value: 626217.78,
        cuota: 914.47,
        cuotaOS: 2947.94,
        categoria: "D"
      },
      {
        label: "Entre 52185 y 69580 por mes",
        value: 834957.0,
        cuota: 1739.48,
        cuotaOS: 3872.48,
        categoria: "E"
      },
      {
        label: "Entre 69580 y 86975 por mes",
        value: 1043696.27,
        cuota: 2393.05,
        cuotaOS: 4634.89,
        categoria: "F"
      },
      {
        label: "Entre 86975 y 104370 por mes",
        value: 1252435.53,
        cuota: 3044.12,
        cuotaOS: 5406.02,
        categoria: "G"
      },
      {
        label: "Entre 104370 y 144958 por mes",
        value: 1739493.79,
        cuota: 6597.96,
        cuotaOS: 9451.93,
        categoria: "H"
      },
      {
        label: "más de 144958 por mes",
        value: "I"
      }
    ],
    pregunta5: [
      {
        label: "No",
        value: 0
      },
      {
        label: "Si, trabajo en relación de dependencia",
        value: 1
      },
      {
        label: "Si, aporto a cajas de previsión para profesionales",
        value: 2
      },
      {
        label: "Si, recibo una jubilación",
        value: 3
      }
    ],
    pregunta6: [
      {
        label: "No quiero adherir a nadie",
        value: 0
      },
      {
        label: "1",
        value: 1
      },
      {
        label: "2",
        value: 2
      },
      {
        label: "3",
        value: 3
      },
      {
        label: "4",
        value: 4
      },
      {
        label: "5",
        value: 5
      },
      {
        label: "6",
        value: 6
      },
      {
        label: "7",
        value: 7
      },
      {
        label: "8",
        value: 8
      },
      {
        label: "9",
        value: 9
      },
      {
        label: "9",
        value: 9
      }
    ],

    // Opciones meses
    meses: [
      { label: "Enero", value: 1 },
      { label: "Febrero", value: 2 },
      { label: "Marzo", value: 3 },
      { label: "Abril", value: 4 },
      { label: "Mayo", value: 5 },
      { label: "Junio", value: 6 },
      { label: "Julio", value: 7 },
      { label: "Agosto", value: 8 },
      { label: "Septiembre", value: 9 },
      { label: "Octubre", value: 10 },
      { label: "Noviembre", value: 11 },
      { label: "Diciembre", value: 12 }
    ],
    // Opciones sexo
    sexo: [
      { label: "Masculino", value: "M" },
      { label: "Femenino", value: "F" }
    ],
    // Opciones estado civil
    estado: [
      { label: "Soltero", value: "Sol" },
      { label: "Casado", value: "Cas" },
      { label: "Viudo", value: "Viu" },
      { label: "Separado", value: "Sep" },
      { label: "Divorciado", value: "Div" }
    ],
    //opciones nacionalidad
    nacionalidad: [
      { label: "Argentina", value: "ARG" },
      { label: "Extranjera", value: "EXT" }
    ],
    // opciones país de origen
    paisOrigen: [
      { label: "Bolivia", value: "BO" },
      { label: "Brasil", value: "BR" },
      { label: "Chile", value: "CH" },
      { label: "Paraguay", value: "PY" },
      { label: "Peru", value: "PE" },
      { label: "Uruguay", value: "UY" },
      { label: "Venezuela", value: "VE" },
      { label: "Otro", value: "OT" }
    ],
    // opciones regimen simplificado
    simplificado: [
      { label: "A", value: 138127.99 },
      { label: "B", value: 207191.98 },
      { label: "C", value: 276255.98 },
      { label: "D", value: 414383.98 },
      { label: "E", value: 552511.95 },
      { label: "F", value: 690639.95 },
      { label: "G", value: 828767.94 },
      { label: "H", value: 1151066.58 }
    ],
    //Declaracion categorizador
    declaracionCategorizador: [
      {
        label:
          "No tengo más de 3 actividades, fuentes de ingreso y/o unidades de explotación",
        value: 1
      },
      {
        label:
          "No importo bienes y/o servicios (ni importé en ultimos 12 meses) para su comercialización",
        value: 2
      },
      {
        label:
          "No me inscribo para recibir ingresos de una sociedad (S.A., S.R.L, etc ni por ser administrador de la misma)",
        value: 3
      }
    ]
  };
}
