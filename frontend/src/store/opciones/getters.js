export function opcionesProvincias(state) {
  let opcionesProvincias = [];
  if (state.provinciasExito) {
    state.provincias.provincias.forEach(provincia => {
      let prov = { label: provincia.nombre, value: provincia.id };
      opcionesProvincias.push(prov);
    });
  }
  return opcionesProvincias;
}

export function provinciasExito(state) {
  return state.provinciasExito;
}

export function provinciasPendiente(state) {
  return state.provinciasPendiente;
}

export function provinciasError(state) {
  return state.provinciasError;
}

export function opcionesLocalidades(state) {
  let opcionesCiudades = [];
  if (state.localidadesExito) {
    state.localidades.localidades.forEach(localidad => {
      let local = { label: localidad.nombre, value: localidad.id };
      opcionesCiudades.push(local);
    });
  }
  return opcionesCiudades;
}

export function localidadesExito(state) {
  return state.localidadesExito;
}

export function localidadesPendiente(state) {
  return state.localidadesPendiente;
}

export function localidadesError(state) {
  return state.localidadesError;
}

export function opcionesPartidos(state) {
  let opcionesPartidos = [];
  if (state.partidosExito) {
    state.partidos.departamentos.forEach(departamento => {
      let depar = { label: departamento.nombre, value: departamento.id };
      opcionesPartidos.push(depar);
    });
  }
  return opcionesPartidos;
}

export function partidosExito(state) {
  return state.partidosExito;
}

export function partidosPendiente(state) {
  return state.partidosPendiente;
}

export function partidosError(state) {
  return state.partidosError;
}

export function pregunta1(state) {
  return state.pregunta1;
}
export function pregunta2(state) {
  return state.pregunta2;
}
export function pregunta5(state) {
  return state.pregunta5;
}
export function pregunta6(state) {
  return state.pregunta6;
}

export function opcionesActividades(state) {
  let opcionesActividades = [];
  if (state.actividadesExito) {
    state.actividades.forEach(actividad => {
      let act = { label: actividad.actividad, value: actividad.codigo };
      opcionesActividades.push(act);
    });
  }

  return opcionesActividades;
}

export function opcionesOS(state) {
  let opcionesOS = [];
  if (state.obrasSocialesExito) {
    state.obrasSociales.forEach(obraSocial => {
      let OS = { label: obraSocial.denominacion, value: obraSocial.codigo };
      opcionesOS.push(OS);
    });
  }

  return opcionesOS;
}
