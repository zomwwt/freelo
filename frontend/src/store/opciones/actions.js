import axios from "axios";
import { geoRef, apiRest } from "../APIs";

export function GetProvincias({ commit }, payload) {
  const endpoint = "provincias?orden=nombre";
  commit("SET_PROVINCIAS_PENDIENTE", true);
  commit("SET_PROVINCIAS_EXITO", false);
  commit("SET_PROVINCIAS", []);

  return axios
    .get(geoRef + endpoint)
    .then(({ data }) => {
      commit("SET_PROVINCIAS", data);
      commit("SET_PROVINCIAS_EXITO", true);
      commit("SET_PROVINCIAS_ERROR", false);
    })
    .catch(err => {
      commit("SET_PROVINCIAS", []);
      commit("SET_PROVINCIAS_EXITO", false);
      commit("SET_PROVINCIAS_ERROR", true);
    })
    .finally(() => {
      commit("SET_PROVINCIAS_PENDIENTE", false);
    });
}

export function GetLocalidadesPorProvincia({ commit }, payload) {
  const endpoint = `localidades?provincia=${payload.provincia}&campos=nombre&max=5000&orden=nombre`;
  commit("SET_LOCALIDADES_PENDIENTE", true);
  commit("SET_LOCALIDADES_EXITO", false);
  commit("SET_LOCALIDADES", []);

  return axios
    .get(geoRef + endpoint)
    .then(({ data }) => {
      commit("SET_LOCALIDADES", data);
      commit("SET_LOCALIDADES_EXITO", true);
      commit("SET_LOCALIDADES_ERROR", false);
    })
    .catch(err => {
      commit("SET_LOCALIDADES", []);
      commit("SET_LOCALIDADES_EXITO", false);
      commit("SET_LOCALIDADES_ERROR", true);
    })
    .finally(() => {
      commit("SET_LOCALIDADES_PENDIENTE", false);
    });
}

export function GetPartidosPorProvincia({ commit }, payload) {
  const endpoint = `departamentos?provincia=${payload.provincia}&campos=nombre,id&max=2500&orden=nombre`;
  commit("SET_PARTIDOS_PENDIENTE", true);
  commit("SET_PARTIDOS_EXITO", false);
  commit("SET_PARTIDOS", []);

  return axios
    .get(geoRef + endpoint)
    .then(({ data }) => {
      commit("SET_PARTIDOS", data);
      commit("SET_PARTIDOS_EXITO", true);
      commit("SET_PARTIDOS_ERROR", false);
    })
    .catch(err => {
      commit("SET_PARTIDOS", []);
      commit("SET_PARTIDOS_EXITO", false);
      commit("SET_PARTIDOS_ERROR", true);
    })
    .finally(() => {
      commit("SET_PARTIDOS_PENDIENTE", false);
    });
}

export function GetActividades({ commit }, payload) {
  const endpoint = "actividades-monotributo";
  commit("SET_ACTIVIDADES_PENDIENTE", true);
  commit("SET_ACTIVIDADES_EXITO", false);
  commit("SET_ACTIVIDADES", []);

  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("SET_ACTIVIDADES", data);
      commit("SET_ACTIVIDADES_EXITO", true);
      commit("SET_ACTIVIDADES_ERROR", false);
    })
    .catch(err => {
      commit("SET_ACTIVIDADES", []);
      commit("SET_ACTIVIDADES_EXITO", false);
      commit("SET_ACTIVIDADES_ERROR", true);
    })
    .finally(() => {
      commit("SET_ACTIVIDADES_PENDIENTE", false);
    });
}

export function GetActividadesArba({ commit }, payload) {
  const endpoint = "actividades-arba";
  commit("SET_ACTIVIDADES_PENDIENTE", true);
  commit("SET_ACTIVIDADES_EXITO", false);
  commit("SET_ACTIVIDADES", []);

  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("SET_ACTIVIDADES", data);
      commit("SET_ACTIVIDADES_EXITO", true);
      commit("SET_ACTIVIDADES_ERROR", false);
    })
    .catch(err => {
      commit("SET_ACTIVIDADES", []);
      commit("SET_ACTIVIDADES_EXITO", false);
      commit("SET_ACTIVIDADES_ERROR", true);
    })
    .finally(() => {
      commit("SET_ACTIVIDADES_PENDIENTE", false);
    });
}

export function GetActividadesAgip({ commit }, payload) {
  const endpoint = "actividades-agip";
  commit("SET_ACTIVIDADES_PENDIENTE", true);
  commit("SET_ACTIVIDADES_EXITO", false);
  commit("SET_ACTIVIDADES", []);

  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("SET_ACTIVIDADES", data);
      commit("SET_ACTIVIDADES_EXITO", true);
      commit("SET_ACTIVIDADES_ERROR", false);
    })
    .catch(err => {
      commit("SET_ACTIVIDADES", []);
      commit("SET_ACTIVIDADES_EXITO", false);
      commit("SET_ACTIVIDADES_ERROR", true);
    })
    .finally(() => {
      commit("SET_ACTIVIDADES_PENDIENTE", false);
    });
}

export function GetObrasSociales({ commit }, payload) {
  const endpoint = "obra-social";
  commit("SET_OBRAS_SOCIALES_PENDIENTE", true);
  commit("SET_OBRAS_SOCIALES_EXITO", false);
  commit("SET_OBRAS_SOCIALES", []);

  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("SET_OBRAS_SOCIALES", data);
      commit("SET_OBRAS_SOCIALES_EXITO", true);
      commit("SET_OBRAS_SOCIALES_ERROR", false);
    })
    .catch(err => {
      commit("SET_OBRAS_SOCIALES", []);
      commit("SET_OBRAS_SOCIALES_EXITO", false);
      commit("SET_OBRAS_SOCIALES_ERROR", true);
    })
    .finally(() => {
      commit("SET_OBRAS_SOCIALES_PENDIENTE", false);
    });
}
