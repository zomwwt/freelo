export default function() {
  return {
    // Para rellenar con los datos que envia la api al registrarse, intentar recuperar la contraseña o registrar un noapto
    respuesta: null,

    // Para el login
    usuario: null,
    // usuarioExito: null,
    // usuarioPendiente: null,
    // usuarioError: null,

    // Monotributo activo
    datosMonotributo: null,

    condicionUsuario: null,
    condicionExito: null,
    condicionError: null
  };
}
