import { Cookies } from "quasar";

export function loggedIn(state) {
  let usuario = Cookies.get("usuario");
  return usuario || state.usuario ? true : false;
}

export function usuario(state) {
  if (state.usuario) {
    return state.usuario;
  } else {
    let usuario = Cookies.get("usuario");
    if (usuario) {
      return usuario;
    }
  }
}

export function dadoDeBaja(state) {
  let usuario = Cookies.get("usuario");
  return !usuario.es_apto_uso_sistema;
}

export function claveAfipDesactualizada(state) {
  return state.condicionUsuario.clave_afip_desactualizada;
}

export function claveIibbDesactualizada(state) {
  return state.condicionUsuario.clave_iibb_desactualizada;
}

export function gestionInactiva(state) {
  if (state.condicionUsuario) {
    return !state.condicionUsuario.gestion_activa_freelo;
  } else {
    return false;
  }
}

export function telefono(state, getters) {
  let telefono = getters.usuario.telefono;
  if (telefono) {
    telefono =
      telefono.slice(0, 2) +
      " " +
      telefono.slice(2, 6) +
      "-" +
      telefono.slice(6, telefono.length);
    return telefono;
  }
  return "Cargando";
}

export function cuit(state, getters) {
  let cuit = getters.usuario.cuit;
  if (cuit) {
    cuit =
      cuit.slice(0, 2) +
      "-" +
      cuit.slice(2, 10) +
      "-" +
      cuit.slice(10, cuit.length);
    return cuit || "Cargando";
  }
  return "Cargando";
}

export function recibeMail(state) {
  let usuario = Cookies.get("usuario");
  return usuario.recibe_mail;
}
