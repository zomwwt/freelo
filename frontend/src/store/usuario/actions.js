import axios from "axios";
import { apiRest } from "../APIs";
import { Cookies } from "quasar";
import jwt from "jwt-decode";

export function RegistrarUsuario({ commit }, credenciales) {
  const endpoint = "registro";
  commit("ALMACENAR_RESPUESTA", null);

  return axios
    .post(apiRest + endpoint, credenciales)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function RecuperarContrasena({ commit }, credenciales) {
  const endpoint = "recuperar-contraseña";
  commit("ALMACENAR_RESPUESTA", null);

  return axios.post(apiRest + endpoint, credenciales).then(({ data }) => {
    commit("ALMACENAR_RESPUESTA", data);
  });
}

export function RegistrarNuevaContrasena({ commit }, credenciales) {
  const endpoint =
    "registrar-nueva-contrasena/" +
    credenciales.uidb64 +
    "/" +
    credenciales.token +
    "/";
  commit("ALMACENAR_RESPUESTA", null);

  return axios.post(apiRest + endpoint, credenciales).then(({ data }) => {
    commit("ALMACENAR_RESPUESTA", data);
  });
}

export function CambiarContrasena({ commit }, credenciales) {
  const endpoint = "cambiar-contraseña/";
  commit("ALMACENAR_RESPUESTA", null);

  return axios
    .post(apiRest + endpoint, credenciales)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function ActivarCuenta({ commit }, credenciales) {
  const endpoint =
    "activar-cuenta/" + credenciales.uidb64 + "/" + credenciales.token + "/";
  commit("ALMACENAR_RESPUESTA", null);

  return axios.post(apiRest + endpoint).then(({ data }) => {
    commit("ALMACENAR_RESPUESTA", data);
  });
}

export function Login({ commit }, credenciales) {
  const endpoint = "login/";
  return axios.post(apiRest + endpoint, credenciales).then(({ data }) => {
    commit("SET_USUARIO", data);
  });
}

export function Logout({ commit }) {
  commit("LOGOUT");
}

export function ActualizarToken({ commit }) {
  let user = Cookies.get("usuario");

  if (user) {
    let token = user.access;
    let decodedToken = jwt(token);
    let decodeExp = decodedToken.exp;
    let now = Date.now() / 1000;
    if (decodeExp - now < 1800) {
      const endpoint = "auth/token-refresh";
      const refreshToken = user.refresh;

      return axios
        .post(apiRest + endpoint, {
          refresh: refreshToken
        })
        .then(({ data }) => {
          commit("SET_NUEVO_ACCESS", data);
        });
    }
  }
}

export function RegistrarNoApto({ commit }, datos) {
  const endpoint = "registrar-no-apto/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function RegistrarMonotributoActivo({ commit }, datos) {
  const endpoint = "registrar-monotributo-activo/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function RegistrarMonotributoNuevo({ commit }, datos) {
  const endpoint = "registrar-monotributo-nuevo/";
  commit("ALMACENAR_RESPUESTA", null);

  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function RegistrarIIIBBActivo({ commit }, datos) {
  const endpoint = "registrar-iibb-activo/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function RegistrarAltaAgip({ commit }, datos) {
  const endpoint = "registrar-iibb-agip/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function RegistrarAltaArba({ commit }, datos) {
  const endpoint = "registrar-iibb-arba/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function ActualizarDatosPersonales({ commit }, datos) {
  const endpoint = "actualizar-info-personal/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
      if (data.success) {
        commit("ACTUALIZAR_USUARIO", datos);
      }
    })
    .catch(err => {});
}

export function ActualizarClaves({ commit }, datos) {
  const endpoint = "actualizar-claves/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, datos)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function CondicionUsuario({ commit }) {
  const endpoint = "condicion-usuario/";

  commit("ALMACENAR_CONDICION_USUARIO", null);
  commit("ESTADO_CONDICION_ERROR", false);
  commit("ESTADO_CONDICION_EXITO", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_CONDICION_USUARIO", data);
      commit("ESTADO_CONDICION_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_CONDICION_ERROR", true);
    });
}

export function BajaMonotributo({ commit }, payload) {
  const endpoint = "baja-monotributo/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, payload)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function BajaFreelo({ commit }, payload) {
  const endpoint = "baja-freelo/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, payload)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function DescargarFactura({ commit }, link) {
  console.log(link);
  return axios
    .get(link, { responseType: "blob" })
    .then(response => {
      console.log("descargar", response);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "file.pdf");
      document.body.appendChild(link);
      link.click();
    })
    .catch(err => {});
}
