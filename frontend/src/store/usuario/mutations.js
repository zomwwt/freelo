import { Cookies } from "quasar";
import axios from "axios";

export function ALMACENAR_RESPUESTA(state, payload) {
  state.respuesta = payload;
}

export function SET_USUARIO(state, payload) {
  state.usuario = payload;
  Cookies.set("usuario", payload, { sameSite: "Lax", path: "/" });
  axios.defaults.headers.common["Authorization"] = `Bearer ${payload.access}`;
}

export function SET_NUEVO_ACCESS(state, payload) {
  let user = Cookies.get("usuario");
  user.access = payload.access;
  state.usuario = user;
  Cookies.set("usuario", user, { sameSite: "Lax", path: "/" });
  axios.defaults.headers.common["Authorization"] = `Bearer ${payload.access}`;
}

export function SET_USUARIO_EXITO(state, payload) {
  state.usuarioExito = payload;
}

export function SET_USUARIO_PENDIENTE(state, payload) {
  state.usuarioPendiente = payload;
}

export function SET_USUARIO_ERROR(state, payload) {
  state.usuarioError = payload;
}

export function LOGOUT() {
  Cookies.remove("usuario", { sameSite: "Lax", path: "/" });

  delete axios.defaults.headers.common["Authorization"];
  location.reload();
}

export function ALMACENAR_CONDICION_USUARIO(state, payload) {
  state.condicionUsuario = payload;
}

export function ESTADO_CONDICION_EXITO(state, payload) {
  state.condicionExito = payload;
}

export function ESTADO_CONDICION_ERROR(state, payload) {
  state.condicionError = payload;
}

export function ACTUALIZAR_USUARIO(state, payload) {
  let user = Cookies.get("usuario");
  user.recibe_mail = payload.recibe_email;
  user.telefono = payload.telefono;
  user.email = payload.email;
  state.usuario = user;
  Cookies.set("usuario", user, { sameSite: "Lax", path: "/" });
}
