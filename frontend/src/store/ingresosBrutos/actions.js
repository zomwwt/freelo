import axios from "axios";
import { apiRest } from "../APIs";

export function TipoIibb({ commit }) {
  const endpoint = "tipo-iibb-usuario/";

  commit("ALMACENAR_TIPO_IIBB", null);
  commit("ESTADO_TIPO_EXITO", false);
  commit("ESTADO_TIPO_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_TIPO_IIBB", data);
      commit("ESTADO_TIPO_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_TIPO_ERROR", true);
    });
}

export function FacturacionesIibb({ commit }) {
  const endpoint = "facturaciones-iibb-usuario/";

  commit("ALMACENAR_FACTURACIONES_IIBB", null);
  commit("ESTADO_FACTURACIONES_EXITO", false);
  commit("ESTADO_FACTURACIONES_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_FACTURACIONES_IIBB", data);
      commit("ESTADO_FACTURACIONES_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_FACTURACIONES_ERROR", true);
    });
}
