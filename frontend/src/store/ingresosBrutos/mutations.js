export function ALMACENAR_TIPO_IIBB(state, payload) {
  state.tipoIibb = payload;
}

export function ESTADO_TIPO_EXITO(state, payload) {
  state.estadoTipoIibbExito = payload;
}

export function ESTADO_TIPO_ERROR(state, payload) {
  state.estadoTipoIibbExito = payload;
}

export function ALMACENAR_FACTURACIONES_IIBB(state, payload) {
  state.facturacionesIibb = payload;
}

export function ESTADO_FACTURACIONES_EXITO(state, payload) {
  state.estadoFacturacionesIibbExito = payload;
}

export function ESTADO_FACTURACIONES_ERROR(state, payload) {
  state.estadoFacturacionesIibbError = payload;
}
