export default function() {
  return {
    tipoIibb: null,
    estadoTipoIibbExito: null,
    estadoTipoIibbError: null,

    facturacionesIibb: null,
    estadoFacturacionesIibbExito: null,
    estadoFacturacionesIibbError: null
  };
}
