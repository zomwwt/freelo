export function facturasIibb(state) {
  let facturas = {};
  facturas = {
    tableColumns: [
      {
        name: "periodo",
        required: true,
        label: "Período",
        align: "center",
        field: row => row.name,
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "vencimiento",
        label: "Fecha de vencimiento",
        align: "center",
        field: "vencimiento",
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "totalmes",
        label: "Total del mes",
        align: "center",
        field: "totalmes",
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "concepto",
        label: "Concepto",
        align: "center",
        field: "concepto",
        format: val => `${val}`,
        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "pago",
        label: "Pago realizado",
        align: "center",
        field: "pago",
        format: val => `${val}`,
        headerStyle: "font-size: 16px; text-transform:uppercase"
      }
    ],
    tableData: []
  };
  state.facturacionesIibb.forEach(fact => {
    if (fact.concepto === "Cuota Mensual" || fact.concepto === "Intereses") {
      let fecha = new Date(fact.mes_facturado);
      let mes = fecha.toLocaleString("es-AR", { month: "long" });
      let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
      let year = fecha.getFullYear();
      let yearVenc = fact.fecha_vencimiento.slice(0, 4);

      let mesVenc = fact.fecha_vencimiento.slice(5, 7);

      let diaVenc = fact.fecha_vencimiento.slice(8, 11);

      let factura = {
        name: mesCapitalizado + " " + year,
        vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
        totalmes: "$ " + fact.monto_facturado,
        pagada: fact.pago_realizado,
        descargafactura: fact.link_pago,
        concepto: fact.concepto
      };
      facturas.tableData.unshift(factura);
    }
  });
  return facturas;
}

export function facturasMensualesIibb(state) {
  let facturas = [];
  state.facturacionesIibb.forEach(fact => {
    if (fact.concepto === "Cuota Mensual") {
      let yearVenc = fact.fecha_vencimiento.slice(0, 4);

      let mesVenc = fact.fecha_vencimiento.slice(5, 7);

      let diaVenc = fact.fecha_vencimiento.slice(8, 11);

      let factura = {
        monto: fact.monto_facturado,
        pago: fact.vep_pdf,
        vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
        retencion: fact.retencion,
        pagado: fact.pago_realizado
      };

      facturas.unshift(factura);
    }
  });

  if (facturas.length === 0) {
    let sinDatos = {
      monto: 0,
      vencimiento: "S/D",
      retencion: 0,
      pagado: null,
      fechaVenc: "S/D"
    };
    facturas.push(sinDatos);
  }
  return facturas;
}

export function agenteRecaudador(state) {
  return state.tipoIibb.agente_recaudador;
}

export function regimen(state) {
  if (state.tipoIibb.regimen_simplificado) {
    return "Régimen Simplificado";
  } else {
    return "Régimen General";
  }
}

export function arba(state) {
  return state.tipoIibb.agente_recaudador === "ARBA";
}

export function simplificado(state) {
  return state.tipoIibb.regimen_simplificado;
}

export function categoriaIibb(state) {
  if (state.tipoIibb.categoria_agip) {
    return state.tipoIibb.categoria_agip;
  }
  return "Z";
}

export function limiteFacturacionIibb(state) {
  if (state.tipoIibb.limite_facturacion_anual) {
    return state.tipoIibb.limite_facturacion_anual;
  }
  return 0;
}
