import Vue from "vue";
import Vuex from "vuex";
import opciones from "./opciones";
import respuestas from "./respuestas";
import usuario from "./usuario";
import monotributo from "./monotributo";
import ingresosBrutos from "./ingresosBrutos";
import freelo from "./freelo";
import notificaciones from "./notificaciones";
import admin from "./admin";

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
      opciones,
      respuestas,
      usuario,
      monotributo,
      ingresosBrutos,
      freelo,
      notificaciones,
      admin
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
