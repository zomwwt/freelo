export function monotributo(state) {
  if (state.preguntasBasicas.monotributo === "alta") {
    return true;
  } else {
    return false;
  }
}

export function arba(state) {
  if (state.preguntasBasicas.arba === "alta") {
    return true;
  } else {
    return false;
  }
}

export function jurisdiccion(state) {
  return state.ingresosBrutosInicial.jurisdiccion;
}

export function resultadosCategorizador(state) {
  let resultados = { categoria: null, cuota: null, limiteFacturacion: null };
  resultados.categoria = state.categorizador.pregunta2.categoria;
  resultados.limiteFacturacion = state.categorizador.pregunta2.value;
  let cuota;
  if (state.categorizador.pregunta5.value === 0) {
    cuota = state.categorizador.pregunta2.cuotaOS;
  } else {
    cuota = state.categorizador.pregunta2.cuota;
  }
  if (state.categorizador.pregunta6) {
    cuota =
      cuota + state.cuotaPorPersonaOS * state.categorizador.pregunta6.value;
  }
  resultados.cuota = cuota;
  return resultados;
}

export function esNoApto(state) {
  return (
    !!state.noAptoPreg1.respuesta ||
    !!state.noAptoPregAnexo.respuesta ||
    !!state.noAptoPreg2.respuesta ||
    !!state.noAptoPreg3.respuesta ||
    !!state.noAptoPreg4.respuesta ||
    !!state.noAptoPreg7.respuesta
  );
}

export function motivosNoApto(state) {
  let motivo = "";
  if (state.noAptoPreg1.respuesta) {
    motivo =
      motivo +
      state.noAptoPreg1.pregunta +
      ": " +
      state.noAptoPreg1.respuesta.label +
      " || ";
  }
  if (state.noAptoPregAnexo.respuesta) {
    motivo =
      motivo +
      state.noAptoPregAnexo.pregunta +
      ": " +
      state.noAptoPregAnexo.respuesta.label +
      " || ";
  }
  if (state.noAptoPreg2.respuesta) {
    motivo =
      motivo +
      state.noAptoPreg2.pregunta +
      ": " +
      state.noAptoPreg2.respuesta.label +
      " || ";
  }
  if (state.noAptoPreg3.respuesta) {
    motivo =
      motivo +
      state.noAptoPreg3.pregunta +
      ": " +
      state.noAptoPreg3.respuesta.label +
      " || ";
  }
  if (state.noAptoPreg4.respuesta) {
    motivo =
      motivo +
      state.noAptoPreg4.pregunta +
      ": " +
      state.noAptoPreg4.respuesta.label +
      " || ";
  }
  if (state.noAptoPreg7.respuesta) {
    motivo =
      motivo +
      state.noAptoPreg7.pregunta +
      ": " +
      state.noAptoPreg7.respuesta.label +
      " || ";
  }
  return motivo;
}
