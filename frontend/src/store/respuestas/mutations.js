export function SET_PREGUNTAS_BASICAS(state, payload) {
  state.preguntasBasicas = payload;
}

export function SET_INGRESOS_BRUTOS_INICIAL(state, payload) {
  state.ingresosBrutosInicial = {};
  state.ingresosBrutosInicial.jurisdiccion = payload.jurisdiccion;
  state.ingresosBrutosInicial.claveFiscal = payload.claveFiscal;
}

export function SET_CATEGORIZADOR(state, payload) {
  state.categorizador = payload;
}

export function SET_NO_APTO_1(state, payload) {
  state.noAptoPreg1.respuesta = payload;
}

export function SET_NO_APTO_ANEXO(state, payload) {
  state.noAptoPregAnexo.respuesta = payload;
}

export function SET_NO_APTO_2(state, payload) {
  state.noAptoPreg2.respuesta = payload;
}

export function SET_NO_APTO_3(state, payload) {
  state.noAptoPreg3.respuesta = payload;
}

export function SET_NO_APTO_4(state, payload) {
  state.noAptoPreg4.respuesta = payload;
}

export function SET_NO_APTO_7(state, payload) {
  state.noAptoPreg7.respuesta = payload;
}

export function SET_MONOTRIBUTO(state, payload) {
  state.monotributo = {};

  if (payload.tengoMonotributo) {
    state.monotributo.claveFiscal = payload.claveFiscal;
  } else {
    state.monotributo.afip = payload.afip;
    state.monotributo.domicilios = payload.domicilios;
    state.monotributo.jurisdiccion = payload.jurisdiccion;
    state.monotributo.actividades = payload.actividades;
    state.monotributo.monotributo1 = payload.monotributo1;
    state.monotributo.monotributo2 = payload.monotributo2;
    state.monotributo.monotributo3 = payload.monotributo3;
  }
}

export function SET_ALTA_IIBB(state, payload) {
  state.ingresosBrutos = {};
  state.ingresosBrutos = payload;
}
