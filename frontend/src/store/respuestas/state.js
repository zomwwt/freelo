export default function() {
  return {
    // Preguntas que responde el usuario al logearse por primera vez (preguntas en pantalla de primer login)
    preguntasBasicas: null,

    // Respuesta de jurisdiccion, clave fiscal arba o agip segun corresponda
    ingresosBrutosInicial: null,

    // Respuestas de alta AGIP
    ingresosBrutos: null,

    // Respuestas formulario categorizador
    categorizador: null,
    noAptoPreg1: {
      pregunta: "Cual es tu actividad",
      respuesta: null
    },
    noAptoPregAnexo: {
      pregunta: "Facturás en otra jurisdicción?",
      respuestA: null
    },
    noAptoPreg2: {
      pregunta: "Ingresos mensuales",
      respuesta: null
    },
    noAptoPreg3: {
      pregunta: "Espacio fisico para tu actividad",
      respuesta: null
    },
    noAptoPreg4: {
      pregunta: "Jurisdiccion donde ejerces tus actividades",
      respuesta: null
    },
    noAptoPreg7: {
      pregunta: "Tenes o vas a tener empleados",
      respuesta: null
    },

    cuotaPorPersonaOS: 1042.61,

    // Respuestas del formulario alta monotributo
    monotributo: null

    // Clave fiscal iibb
  };
}
