import axios from "axios";
import { apiRest } from "../APIs";

export function setPreguntasBasicas({ commit }, payload) {
  commit("SET_PREGUNTAS_BASICAS", payload);
}

export function setIngresosBrutosInicial({ commit }, payload) {
  commit("SET_INGRESOS_BRUTOS_INICIAL", payload);
}

export function setCategorizador({ commit }, payload) {
  commit("SET_CATEGORIZADOR", payload);

  if (payload.pregunta1.value === 2) {
    commit("SET_NO_APTO_1", payload.pregunta1);
  }
  if (payload.preguntaanexo.value === 1) {
    commit("SET_NO_APTO_ANEXO", payload.preguntaanexo);
  }
  if (payload.pregunta2.value === "I") {
    commit("SET_NO_APTO_2", payload.pregunta2);
  }
  if (payload.pregunta3.value === 1) {
    commit("SET_NO_APTO_3", payload.pregunta3);
  }
  if (payload.pregunta7.value === 1) {
    commit("SET_NO_APTO_7", payload.pregunta7);
  }
  if (payload.pregunta4.value === "06" || payload.pregunta4.value === "02") {
    return;
  } else {
    commit("SET_NO_APTO_4", payload.pregunta4);
  }
}

export function setMonotributo({ commit }, payload) {
  commit("SET_MONOTRIBUTO", payload);
}

export function setAltaIibb({ commit }, payload) {
  commit("SET_ALTA_IIBB", payload);
}
