export function ALMACENAR_USUARIOS_ACTIVOS(state, payload) {
  state.usuariosActivos = payload;
}

export function ESTADO_ACTIVOS_EXITO(state, payload) {
  state.activosExito = payload;
}

export function ESTADO_ACTIVOS_ERROR(state, payload) {
  state.activosError = payload;
}

export function ALMACENAR_USUARIOS_INACTIVOS(state, payload) {
  state.usuariosInactivos = payload;
}

export function ESTADO_INACTIVOS_EXITO(state, payload) {
  state.inactivosExito = payload;
}

export function ESTADO_INACTIVOS_ERROR(state, payload) {
  state.inactivosError = payload;
}

export function ALMACENAR_USUARIOS_NO_APTOS(state, payload) {
  state.usuariosNoAptos = payload;
}

export function ESTADO_NO_APTOS_EXITO(state, payload) {
  state.noAptosExito = payload;
}

export function ESTADO_NO_APTOS_ERROR(state, payload) {
  state.noAptosError = payload;
}

export function ALMACENAR_USUARIO(state, payload) {
  state.usuario = payload;
}

export function ESTADO_USUARIO_EXITO(state, payload) {
  state.usuarioExito = payload;
}

export function ESTADO_USUARIO_ERROR(state, payload) {
  state.usuarioError = payload;
}

export function ALMACENAR_FACTURAS_MONOTRIBUTO(state, payload) {
  state.facturasMonotributo = payload;
}

export function ESTADO_FACTURAS_MONOTRIBUTO_EXITO(state, payload) {
  state.facturasMonotributoExito = payload;
}

export function ESTADO_FACTURAS_MONOTRIBUTO_ERROR(state, payload) {
  state.facturasMonotributoError = payload;
}

export function ELIMINAR_FACTURA_MONOTRIBUTO(state, payload) {
  state.facturasMonotributo = state.facturasMonotributo.filter(
    factura => payload !== factura.pk
  );
}

export function ALMACENAR_FACTURAS_INGRESOS_BRUTOS(state, payload) {
  state.facturasIibb = payload;
}

export function ESTADO_FACTURAS_INGRESOS_BRUTOS_EXITO(state, payload) {
  state.facturasIibbExito = payload;
}

export function ESTADO_FACTURAS_INGRESOS_BRUTOS_ERROR(state, payload) {
  state.facturasIibbError = payload;
}

export function ELIMINAR_FACTURA_INGRESOS_BRUTOS(state, payload) {
  state.facturasIibb = state.facturasIibb.filter(
    factura => payload !== factura.pk
  );
}

export function ALMACENAR_FACTURAS_FREELO(state, payload) {
  state.facturasFreelo = payload;
}

export function ESTADO_FACTURAS_FREELO_EXITO(state, payload) {
  state.facturasFreeloExito = payload;
}

export function ESTADO_FACTURAS_FREELO_ERROR(state, payload) {
  state.facturasFreeloError = payload;
}

export function ELIMINAR_FACTURA_FREELO(state, payload) {
  state.facturasFreelo = state.facturasFreelo.filter(
    factura => payload !== factura.pk
  );
}

export function ALMACENAR_RESPUESTA(state, payload) {
  state.respuesta = payload;
}

export function ESTADO_EDICION_EXITO(state, payload) {
  state.edicionExito = payload;
}

export function ESTADO_EDICION_ERROR(state, payload) {
  state.edicionError = payload;
}

export function ACTUALIZAR_DATOS_PERSONALES_USUARIO(state, payload) {
  let usuario = state.usuario;
  usuario.datos_personales = payload.edicion.datos_personales;
  state.usuario = usuario;
}

export function ACTUALIZAR_DOMICILIO_GENERAL_USUARIO(state, payload) {
  let usuario = state.usuario;
  usuario.datos_personales.direccion.domicilio_general =
    payload.edicion.datos_personales.domicilio.domicilio_general;
  state.usuario = usuario;
}

export function ACTUALIZAR_DOMICILIO_ARBA_USUARIO(state, payload) {
  let usuario = state.usuario;
  usuario.datos_personales.direccion.domicilio_arba =
    payload.edicion.datos_personales.domicilio.domicilio_arba;
  state.usuario = usuario;
}

export function ACTUALIZAR_DOMICILIO_AGIP_USUARIO(state, payload) {
  let usuario = state.usuario;
  usuario.datos_personales.direccion.domicilio_agip =
    payload.edicion.datos_personales.domicilio.domicilio_agip;
  state.usuario = usuario;
}

export function ACTUALIZAR_DATOS_MONOTRIBUTO_USUARIO(state, payload) {
  let usuario = state.usuario;
  usuario.datos_monotributo = payload.edicion.datos_monotributo;
  state.usuario = usuario;
}

export function ACTUALIZAR_DATOS_INGRESOS_BRUTOS_USUARIO(state, payload) {
  let usuario = state.usuario;
  usuario.organismo_recaudacion.datos =
    payload.edicion.organismo_recaudacion.datos;
  state.usuario = usuario;
}

export function AGREGAR_FACTURA_FREELO(state, payload) {
  state.facturasFreelo.push(payload);
}

export function AGREGAR_FACTURA_MONOTRIBUTO(state, payload) {
  state.facturasMonotributo.push(payload);
}

export function AGREGAR_FACTURA_INGRESOS_BRUTOS(state, payload) {
  state.facturasIibb.push(payload);
}

export function EDITAR_FACTURA_FREELO(state, payload) {
  state.facturasFreelo = state.facturasFreelo.filter(
    factura => payload.pk !== factura.pk
  );
  state.facturasFreelo.push(payload);
}

export function EDITAR_FACTURA_MONOTRIBUTO(state, payload) {
  state.facturasMonotributo = state.facturasMonotributo.filter(
    factura => payload.pk !== factura.pk
  );
  state.facturasMonotributo.push(payload);
}

export function EDITAR_FACTURA_INGRESOS_BRUTOS(state, payload) {
  state.facturasIibb = state.facturasIibb.filter(
    factura => payload.pk !== factura.pk
  );
  state.facturasIibb.push(payload);
}

export function ELIMINAR_USUARIO(state, payload) {
  state.usuariosActivos = state.usuariosActivos.filter(
    usuario => usuario.pk !== payload
  );
  state.usuariosInactivos = state.usuariosInactivos.filter(
    usuario => usuario.pk !== payload
  );
}

export function ALMACENAR_DATOS_SECUNDARIOS(state, payload) {
  state.datosSecundarios = payload;
}

export function ESTADO_DATOS_SECUNDARIOS_EXITO(state, payload) {
  state.datosSecundariosExito = payload;
}

export function ESTADO_DATOS_SECUNDARIOS_ERROR(state, payload) {
  state.datosSecundariosError = payload;
}

export function ACTUALIZAR_DATOS_FREELO_USUARIO(state, payload) {
  state.usuario.datos_freelo = payload.datos_freelo;
}
