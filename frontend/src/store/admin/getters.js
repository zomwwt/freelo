export function usuariosActivos(state) {
  if (state.usuariosActivos) {
    return state.usuariosActivos;
  }
  return [
    {
      pk: null,
      nombre: "S/D",
      apellido: "S/D",
      cuit: "S/D",
      telefono: "s/d",
      email: "s/d@s/d.com"
    }
  ];
}

export function usuariosInactivos(state) {
  if (state.usuariosInactivos) {
    return state.usuariosInactivos;
  }
  return [
    {
      pk: null,
      nombre: "S/D",
      apellido: "S/D",
      cuit: "S/D",
      telefono: "s/d",
      email: "s/d@s/d.com"
    }
  ];
}

export function usuariosNoAptos(state) {
  if (state.usuariosNoAptos) {
    return state.usuariosNoAptos;
  }
  return [{ pk: null, nombre: "S/D", email: "s/d@s/d.com", motivos: "S/D" }];
}

export function facturasMonotributo(state) {
  if (state.facturasMonotributo) {
    let facturasMonotributo = [];
    state.facturasMonotributo.forEach(fact => {
      if (fact.concepto !== "Facturacion") {
        let fecha = new Date(fact.periodo);
        let mes = fecha.toLocaleString("es-AR", { month: "long" });
        let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
        let year = fecha.getFullYear();
        let fechaVencimiento = new Date(fact.fecha_vencimiento);
        let diaVenc = fechaVencimiento.getUTCDate();
        let mesVenc = fechaVencimiento.getMonth() + 1;
        let yearVenc = fechaVencimiento.getFullYear();
        if (diaVenc < 10) {
          diaVenc = "0" + diaVenc;
        }
        if (mesVenc < 10) {
          mesVenc = "0" + mesVenc;
        }
        let factura = {
          pk: fact.pk,
          periodo: mesCapitalizado + " " + year,
          fecha_vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
          monto: fact.monto,
          estado_pago: fact.estado_pago,
          comprobante_pago: fact.comprobante_pago,
          concepto: fact.concepto
        };
        facturasMonotributo.push(factura);
      }
    });
    return facturasMonotributo;
  }
  return [
    {
      pk: null,
      monto: 0,
      comprobante_pago: null,
      periodo: "S/D",
      fecha_vencimiento: "S/D",
      concepto: "S/D",
      estado_pago: false
    }
  ];
}

export function datosPersonales(state) {
  if (state.usuario && state.usuario.datos_personales) {
    return state.usuario.datos_personales;
  }
  return {
    nombre: null,
    apellido: null,
    email: null,
    cuit: null,
    direccion: {
      domicilio_general: {
        provincia: null,
        localidad: null,
        codigo_postal: null,
        calle: null,
        numero: null,
        piso: null,
        oficina: null,
        sector: null,
        torre: null,
        manzana: null,
        pk: 1
      },
      domicilio_arba: {
        provincia: null,
        localidad: null,
        codigo_postal: null,
        calle: null,
        numero: null,
        ruta: null,
        km: null,
        partido: null,
        piso: null,
        oficina: null,
        sector: null,
        torre: null,
        manzana: null,
        pk: null
      },
      domicilio_agip: {
        calle: null,
        numero: null,
        barrio: null,
        ruta: null,
        km: null,
        piso: null,
        oficina: null,
        sector: null,
        torre: null,
        manzana: null,
        pk: null
      }
    },
    domicilio_fiscal: null,
    jurisdiccion: null,
    fecha_registro: null,
    telefono: null,
    notificaciones_mail: null
  };
}

export function datosFreelo(state) {
  if (state.usuario && state.usuario.datos_freelo) {
    return state.usuario.datos_freelo;
  }
  return { gestion_activa: null };
}

export function datosMonotributo(state) {
  if (state.usuario && state.usuario.datos_monotributo) {
    let monotributo = state.usuario.datos_monotributo;
    return monotributo;
  }
  return {
    categoria: null,
    clave: null,
    clave_desactualizada: null,
    limite_facturacion: null,
    cuit: null
  };
}

export function datosAgenteRecaudador(state) {
  if (state.usuario && state.usuario.organismo_recaudacion) {
    return state.usuario.organismo_recaudacion;
  }
  return "S/D";
}

export function fechaRegistro(state) {
  if (state.usuario && state.usuario.datos_personales) {
    let fechaRegistro = state.usuario.datos_personales.fecha_registro;
    fechaRegistro = fechaRegistro
      .replace(/-/g, "/")
      .replace("T", " ")
      .replace("Z", "")
      .slice(0, -7);
    let year = fechaRegistro.slice(0, 4);
    let mes = fechaRegistro.slice(5, 7);
    let dia = fechaRegistro.slice(8, 10);
    return dia + "/" + mes + "/" + year;
  }
  return "00/00/0000";
}

export function agenteRegimen(state) {
  if (state.usuario && state.usuario.organismo_recaudacion) {
    if (state.usuario.organismo_recaudacion.entidad === "arba") {
      return "ARBA";
    } else {
      if (state.usuario.organismo_recaudacion.regimen_simplificado) {
        return "AGIP Régimen Simplificado";
      } else {
        return "AGIP Régimen General";
      }
    }
  }
  return "S/D";
}

export function jurisdiccionProvincia(state) {
  if (state.usuario && state.usuario.organismo_recaudacion) {
    if (state.usuario.organismo_recaudacion.entidad === "arba") {
      return "Provincia de Buenos Aires";
    } else {
      return "Ciudad Autónoma de Buenos Aires";
    }
  }
  return "S/D";
}

export function facturasIngresosBrutos(state) {
  if (state.facturasIibb) {
    let facturasIibb = [];
    state.facturasIibb.forEach(fact => {
      let fecha = new Date(fact.periodo);
      let mes = fecha.toLocaleString("es-AR", { month: "long" });
      let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
      let year = fecha.getFullYear();
      let fechaVencimiento = new Date(fact.fecha_vencimiento);
      let diaVenc = fechaVencimiento.getUTCDate();
      let mesVenc = fechaVencimiento.getMonth() + 1;
      let yearVenc = fechaVencimiento.getFullYear();
      if (diaVenc < 10) {
        diaVenc = "0" + diaVenc;
      }
      if (mesVenc < 10) {
        mesVenc = "0" + mesVenc;
      }
      let factura = {
        pk: fact.pk,
        periodo: mesCapitalizado + " " + year,
        fecha_vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
        monto: fact.monto,
        estado_pago: fact.estado_pago,
        comprobante_pago: fact.comprobante_pago,
        concepto: fact.concepto
      };
      facturasIibb.push(factura);
    });
    return facturasIibb;
  }
  return [
    {
      pk: null,
      monto: 0,
      comprobante_pago: null,
      periodo: "S/D",
      fecha_vencimiento: "S/D",
      concepto: "S/D",
      estado_pago: false
    }
  ];
}

export function facturasFreelo(state) {
  if (state.facturasFreelo) {
    let facturasFreelo = [];
    state.facturasFreelo.forEach(fact => {
      let fecha = new Date(fact.periodo);
      let mes = fecha.toLocaleString("es-AR", { month: "long" });
      let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
      let year = fecha.getFullYear();
      let fechaVencimiento = new Date(fact.fecha_vencimiento);
      let diaVenc = fechaVencimiento.getUTCDate();
      let mesVenc = fechaVencimiento.getMonth() + 1;
      let yearVenc = fechaVencimiento.getFullYear();
      if (diaVenc < 10) {
        diaVenc = "0" + diaVenc;
      }
      if (mesVenc < 10) {
        mesVenc = "0" + mesVenc;
      }
      let factura = {
        pk: fact.pk,
        facturacion: fact.facturacion_mes,
        periodo: mesCapitalizado + " " + year,
        fecha_vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
        monto: fact.monto_pago,
        link: fact.link_pago,
        estado_pago: fact.estado_pago,
        comprobante_pago: fact.factura
      };
      facturasFreelo.push(factura);
    });
    return facturasFreelo;
  }
  return [
    {
      pk: null,
      facturacion: "",
      periodo: "S/D",
      fecha_vencimiento: "S/D",
      link: null,
      monto: 0,
      comprobante_pago: null,
      estado_pago: false
    }
  ];
}

export function respuestaEdicion(state) {
  return state.respuesta;
}

export function fechaVencimientoFreelo(state) {
  return pk => {
    let factura = state.facturasFreelo.filter(fact => fact.pk === pk);

    return factura[0].fecha_vencimiento;
  };
}

export function periodoFreelo(state) {
  return pk => {
    let factura = state.facturasFreelo.filter(fact => fact.pk === pk);

    return factura[0].periodo;
  };
}

export function fechaVencimientoMonotributo(state) {
  return pk => {
    let factura = state.facturasMonotributo.filter(fact => fact.pk === pk);

    return factura[0].fecha_vencimiento;
  };
}

export function periodoMonotributo(state) {
  return pk => {
    let factura = state.facturasMonotributo.filter(fact => fact.pk === pk);

    return factura[0].periodo;
  };
}

export function fechaVencimientoIibb(state) {
  return pk => {
    let factura = state.facturasIibb.filter(fact => fact.pk === pk);

    return factura[0].fecha_vencimiento;
  };
}

export function periodoIibb(state) {
  return pk => {
    let factura = state.facturasIibb.filter(fact => fact.pk === pk);

    return factura[0].periodo;
  };
}

export function usuariosNotificaciones(state) {
  let usuariosNotificaciones = [];
  if (state.usuariosActivos) {
    state.usuariosActivos.forEach(user => {
      let usuario = {
        label:
          user.apellido + " " + user.nombre + " - " + (user.cuit || "Sin cuit"),
        value: user.pk
      };
      usuariosNotificaciones.push(usuario);
    });
  }
  if (state.usuariosInactivos) {
    state.usuariosInactivos.forEach(user => {
      let usuario = {
        label:
          user.apellido + " " + user.nombre + " - " + (user.cuit || "Sin cuit"),
        value: user.pk
      };
      usuariosNotificaciones.push(usuario);
    });
  }

  if (usuariosNotificaciones.length === 0) {
    return [{ label: "S/D", value: null }];
  }
  return usuariosNotificaciones;
}

export function obraSocial(state) {
  let obraSocial = null;
  if (state.datosSecundarios) {
    state.datosSecundarios.forEach(element => {
      if (element.obra_social) {
        obraSocial = element.obra_social;
      }
    });
  }
  if (obraSocial) {
    return obraSocial;
  } else {
    return { codigo: 0, denominacion: "S/D" };
  }
}

export function familiares(state) {
  let familiares = null;
  if (state.datosSecundarios) {
    state.datosSecundarios.forEach(element => {
      if (element.familiares) {
        familiares = element.familiares;
      }
    });
  }
  if (familiares) {
    return familiares;
  } else {
    return [
      {
        apellido: "S/D",
        cuit: "S/D",
        nombre: "S/D",
        unificacion_aporte: null,
        vinculo: "S/D"
      }
    ];
  }
}

export function actividadesMonotributo(state) {
  let actividades = null;
  if (state.datosSecundarios) {
    state.datosSecundarios.forEach(element => {
      if (element.actividades_monotributo) {
        actividades = element.actividades_monotributo;
      }
    });
  }
  if (actividades) {
    return actividades;
  } else {
    return [
      {
        codigo: 0,
        fecha_inicio: "AAAA/MM/DD",
        nombre: "S/D",
        primaria: "S/D"
      }
    ];
  }
}

export function actividadesAgip(state) {
  let actividades = null;
  if (state.datosSecundarios) {
    state.datosSecundarios.forEach(element => {
      if (element.actividades_agip) {
        actividades = element.actividades_agip;
      }
    });
  }
  if (actividades) {
    return actividades;
  } else {
    return [
      {
        codigo: 0,
        fecha_inicio: "AAAA/MM/DD",
        nombre: "S/D",
        primaria: "S/D"
      }
    ];
  }
}

export function actividadesArba(state) {
  let actividades = null;
  if (state.datosSecundarios) {
    state.datosSecundarios.forEach(element => {
      if (element.actividades_arba) {
        actividades = element.actividades_arba;
      }
    });
  }
  if (actividades) {
    return actividades;
  } else {
    return [
      {
        codigo: 0,
        fecha_inicio: "AAAA/MM/DD",
        nombre: "S/D",
        primaria: "S/D"
      }
    ];
  }
}

export function facturacionAcumulada(state) {
  let facturacionAcumulada = 0;
  if (state.facturasFreelo) {
    state.facturasFreelo.forEach(fact => {
      let year = fact.periodo.slice(0, 4);
      let actualYear = new Date();
      actualYear = actualYear.getFullYear();
      if (year == actualYear) {
        facturacionAcumulada =
          facturacionAcumulada + parseFloat(fact.facturacion_mes);
      }
    });
  }
  return facturacionAcumulada;
}
