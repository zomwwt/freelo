export default function() {
  return {
    usuariosActivos: null,
    activosExito: null,
    activosError: null,

    usuariosInactivos: null,
    inactivosExito: null,
    inactivosError: null,

    usuariosNoAptos: null,
    noAptosExito: null,
    noAptosError: null,

    usuario: null,
    usuarioExito: null,
    usuarioError: null,

    datosSecundarios: null,
    datosSecundariosExito: null,
    datosSecundariosError: null,

    facturasMonotributo: null,
    facturasMonotributoExito: null,
    facturasMonotributoError: null,

    facturasIibb: null,
    facturasIibbExito: null,
    facturasIibbError: null,

    facturasFreelo: null,
    facturasFreeloExito: null,
    facturasFreeloError: null,

    respuesta: null,
    edicionExito: null,
    edicionError: null
  };
}
