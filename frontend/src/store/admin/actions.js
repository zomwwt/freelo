import axios from "axios";
import { apiRest } from "../APIs";

export function UsuariosActivos({ commit }) {
  const endpoint = "usuarios-activos";
  commit("ALMACENAR_USUARIOS_ACTIVOS", null);
  commit("ESTADO_ACTIVOS_EXITO", false);
  commit("ESTADO_ACTIVOS_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_USUARIOS_ACTIVOS", data);
      commit("ESTADO_ACTIVOS_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_ACTIVOS_ERROR", true);
    });
}

export function UsuariosInactivos({ commit }) {
  const endpoint = "usuarios-no-activos";
  commit("ALMACENAR_USUARIOS_INACTIVOS", null);
  commit("ESTADO_INACTIVOS_EXITO", false);
  commit("ESTADO_INACTIVOS_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_USUARIOS_INACTIVOS", data);
      commit("ESTADO_INACTIVOS_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_INACTIVOS_ERROR", true);
    });
}

export function UsuariosNoAptos({ commit }) {
  const endpoint = "usuarios-no-aptos";

  commit("ALMACENAR_USUARIOS_NO_APTOS", null);
  commit("ESTADO_NO_APTOS_EXITO", false);
  commit("ESTADO_NO_APTOS_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_USUARIOS_NO_APTOS", data);
      commit("ESTADO_NO_APTOS_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_NO_APTOS_ERROR", true);
    });
}

export function EliminarUsuario({ commit }, pk) {
  const endpoint = `usuario/${pk}/eliminar/`;

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
      if (data.success) {
        commit("ELIMINAR_USUARIO", pk);
      }
    })
    .catch(err => {});
}

export function DatosUsuario({ commit }, pk) {
  const endpoint = `usuario/${pk}`;

  commit("ALMACENAR_USUARIO", null);
  commit("ESTADO_USUARIO_EXITO", false);
  commit("ESTADO_USUARIO_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_USUARIO", data.response);
      commit("ESTADO_USUARIO_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_USUARIO_ERROR", true);
    });
}

export function DatosSecundarios({ commit }, pk) {
  const endpoint = `datos-secundarios/usuario/${pk}`;

  commit("ALMACENAR_DATOS_SECUNDARIOS", null);
  commit("ESTADO_DATOS_SECUNDARIOS_EXITO", false);
  commit("ESTADO_DATOS_SECUNDARIOS_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_DATOS_SECUNDARIOS", data.response);
      commit("ESTADO_DATOS_SECUNDARIOS_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_DATOS_SECUNDARIOS_ERROR", true);
    });
}

export function FacturasMonotributo({ commit }, pk) {
  const endpoint = `facturaciones-monotributo/usuario/${pk}`;

  commit("ALMACENAR_FACTURAS_MONOTRIBUTO", null);
  commit("ESTADO_FACTURAS_MONOTRIBUTO_EXITO", false);
  commit("ESTADO_FACTURAS_MONOTRIBUTO_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_FACTURAS_MONOTRIBUTO", data);
      commit("ESTADO_FACTURAS_MONOTRIBUTO_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_FACTURAS_MONOTRIBUTO_ERROR", true);
    });
}

export function FacturasIngresosBrutos({ commit }, pk) {
  const endpoint = `facturaciones-iibb/usuario/${pk}`;

  commit("ALMACENAR_FACTURAS_INGRESOS_BRUTOS", null);
  commit("ESTADO_FACTURAS_INGRESOS_BRUTOS_EXITO", false);
  commit("ESTADO_FACTURAS_INGRESOS_BRUTOS_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_FACTURAS_INGRESOS_BRUTOS", data);
      commit("ESTADO_FACTURAS_INGRESOS_BRUTOS_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_FACTURAS_INGRESOS_BRUTOS_ERROR", true);
    });
}

export function FacturasFreelo({ commit }, pk) {
  const endpoint = `facturaciones-freelo/usuario/${pk}`;

  commit("ALMACENAR_FACTURAS_FREELO", null);
  commit("ESTADO_FACTURAS_FREELO_EXITO", false);
  commit("ESTADO_FACTURAS_FREELO_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_FACTURAS_FREELO", data.response);
      commit("ESTADO_FACTURAS_FREELO_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_FACTURAS_FREELO_ERROR", true);
    });
}

export function EditarDatosPersonales({ commit }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DATOS_PERSONALES_USUARIO", payload);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarDomicilioGeneral({ commit, state }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  payload.edicion.datos_personales.notificaciones_mail =
    state.usuario.datos_personales.notificaciones_mail;
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DOMICILIO_GENERAL_USUARIO", payload);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarDomicilioArba({ commit, state }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  payload.edicion.datos_personales.notificaciones_mail =
    state.usuario.datos_personales.notificaciones_mail;
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DOMICILIO_ARBA_USUARIO", payload);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarDomicilioAgip({ commit, state }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  payload.edicion.datos_personales.notificaciones_mail =
    state.usuario.datos_personales.notificaciones_mail;
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DOMICILIO_AGIP_USUARIO", payload);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarDatosMonotributo({ commit }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DATOS_MONOTRIBUTO_USUARIO", payload);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarDatosIngresosBrutos({ commit }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DATOS_INGRESOS_BRUTOS_USUARIO", payload);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarDatosFreelo({ commit }, payload) {
  const endpoint = `usuario/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  return axios
    .post(apiRest + endpoint, payload.edicion)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ACTUALIZAR_DATOS_FREELO_USUARIO", data.response);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function AgregarFacturaFreelo({ commit }, payload) {
  const endpoint = "facturacion-freelo/";

  let form = new FormData();
  form.append("persona_id", payload.persona_id);
  form.append("monto_pago", payload.monto_pago);
  form.append("factura", payload.factura);
  form.append("link_pago", payload.link_pago);
  form.append("facturacion_mes", payload.facturacion_mes);
  form.append("estado_pago", payload.estado_pago);
  form.append("periodo", payload.periodo);
  form.append("fecha_vencimiento", payload.fecha_vencimiento);
  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);

  return axios
    .post(apiRest + endpoint, form, {
      headers: { "Content-Type": "multipart/form-data" }
    })

    .then(({ data }) => {
      if (data.success) {
        commit("AGREGAR_FACTURA_FREELO", data.response);
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarFacturaFreelo({ commit }, payload) {
  const endpoint = `facturacion-freelo/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  let form = new FormData();
  form.append("persona_id", payload.persona_id);
  form.append("monto_pago", payload.monto_pago);
  form.append("factura", payload.factura ? payload.factura : "");
  form.append("link_pago", payload.link_pago);
  form.append("facturacion_mes", payload.facturacion_mes);
  form.append("estado_pago", payload.estado_pago);
  form.append("periodo", payload.periodo);
  form.append("fecha_vencimiento", payload.fecha_vencimiento);

  return axios
    .post(apiRest + endpoint, form, {
      headers: { "Content-Type": "multipart/form-data" }
    })

    .then(({ data }) => {
      if (data.success) {
        commit("EDITAR_FACTURA_FREELO", data.response);
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EliminarFacturaFreelo({ commit }, pk) {
  const endpoint = `facturacion-freelo/${pk}/eliminar/`;
  commit("ALMACENAR_RESPUESTA", null);

  return axios
    .post(apiRest + endpoint)
    .then(({ data }) => {
      if (data.success) {
        commit("ELIMINAR_FACTURA_FREELO", pk);
        commit("ALMACENAR_RESPUESTA", data);
      }
    })
    .catch(err => {});
}

export function AgregarFacturaMonotributo({ commit }, payload) {
  const endpoint = "facturacion-monotributo/";

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  let form = new FormData();
  form.append("persona_id", payload.persona_id);
  form.append("monto", payload.monto);
  form.append("comprobante_pago", payload.comprobante_pago);
  form.append("concepto", payload.concepto);
  form.append("estado_pago", payload.estado_pago);
  form.append("periodo", payload.periodo);
  form.append("fecha_vencimiento", payload.fecha_vencimiento);

  return axios
    .post(apiRest + endpoint, form, {
      headers: { "Content-Type": "multipart/form-data" }
    })

    .then(({ data }) => {
      if (data.success) {
        commit("AGREGAR_FACTURA_MONOTRIBUTO", data.response);
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarFacturaMonotributo({ commit }, payload) {
  const endpoint = `facturacion-monotributo/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  let form = new FormData();
  form.append("persona_id", payload.persona_id);
  form.append("monto", payload.monto);
  form.append(
    "comprobante_pago",
    payload.comprobante_pago ? payload.comprobante_pago : ""
  );
  form.append("concepto", payload.concepto);
  form.append("estado_pago", payload.estado_pago);
  form.append("periodo", payload.periodo);
  form.append("fecha_vencimiento", payload.fecha_vencimiento);

  return axios
    .post(apiRest + endpoint, form, {
      headers: { "Content-Type": "multipart/form-data" }
    })

    .then(({ data }) => {
      if (data.success) {
        commit("EDITAR_FACTURA_MONOTRIBUTO", data.response);
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EliminarFacturaMonotributo({ commit }, pk) {
  const endpoint = `facturacion-monotributo/${pk}/eliminar/`;
  commit("ALMACENAR_RESPUESTA", null);

  return axios
    .post(apiRest + endpoint)
    .then(({ data }) => {
      if (data.success) {
        commit("ELIMINAR_FACTURA_MONOTRIBUTO", pk);
        commit("ALMACENAR_RESPUESTA", data);
      }
    })
    .catch(err => {});
}

export function AgregarFacturaIngresosBrutos({ commit }, payload) {
  const endpoint = "facturacion-iibb/";

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);
  let form = new FormData();
  form.append("persona_id", payload.persona_id);
  form.append("monto", payload.monto);
  form.append("comprobante_pago", payload.comprobante_pago);
  form.append("concepto", payload.concepto);
  form.append("estado_pago", payload.estado_pago);
  form.append("periodo", payload.periodo);
  form.append("fecha_vencimiento", payload.fecha_vencimiento);
  return axios
    .post(apiRest + endpoint, form, {
      headers: { "Content-Type": "multipart/form-data" }
    })

    .then(({ data }) => {
      if (data.success) {
        commit("AGREGAR_FACTURA_INGRESOS_BRUTOS", data.response);
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EditarFacturaIngresosBrutos({ commit }, payload) {
  const endpoint = `facturacion-iibb/${payload.pk}/editar/`;

  commit("ALMACENAR_RESPUESTA", null);
  commit("ESTADO_EDICION_EXITO", false);
  commit("ESTADO_EDICION_ERROR", false);

  let form = new FormData();
  form.append("persona_id", payload.persona_id);
  form.append("monto", payload.monto);
  form.append(
    "comprobante_pago",
    payload.comprobante_pago ? payload.comprobante_pago : ""
  );
  form.append("concepto", payload.concepto);
  form.append("estado_pago", payload.estado_pago);
  form.append("periodo", payload.periodo);
  form.append("fecha_vencimiento", payload.fecha_vencimiento);
  return axios
    .post(apiRest + endpoint, form, {
      headers: { "Content-Type": "multipart/form-data" }
    })

    .then(({ data }) => {
      if (data.success) {
        commit("EDITAR_FACTURA_INGRESOS_BRUTOS", data.response);
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}

export function EliminarFacturaIibb({ commit }, pk) {
  const endpoint = `facturacion-iibb/${pk}/eliminar/`;
  commit("ALMACENAR_RESPUESTA", null);

  return axios
    .post(apiRest + endpoint)
    .then(({ data }) => {
      if (data.success) {
        commit("ELIMINAR_FACTURA_INGRESOS_BRUTOS", pk);
        commit("ALMACENAR_RESPUESTA", data);
      }
    })
    .catch(err => {});
}

export function EnviarNotificacion({ commit }, payload) {
  const endpoint = "notificacion/";
  if (payload.lista_completa) {
    delete payload.persona_id;
  }

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, payload)
    .then(({ data }) => {
      commit("ALMACENAR_RESPUESTA", data);
    })
    .catch(err => {});
}

export function CambiarRegimen({ commit }, payload) {
  const endpoint = "cambio-regimen-jurisdiccion/";

  commit("ALMACENAR_RESPUESTA", null);
  return axios
    .post(apiRest + endpoint, payload)
    .then(({ data }) => {
      if (data.success) {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_EXITO", true);
      } else {
        commit("ALMACENAR_RESPUESTA", data);
        commit("ESTADO_EDICION_ERROR", true);
      }
    })
    .catch(err => {
      commit("ESTADO_EDICION_ERROR", true);
    });
}
