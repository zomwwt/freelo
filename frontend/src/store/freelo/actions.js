import axios from "axios";
import { apiRest } from "../APIs";

export function FacturacionesFreelo({ commit }) {
  const endpoint = "facturaciones-freelo-usuario/";

  commit("ALMACENAR_FACTURACIONES_FREELO", null);
  commit("ESTADO_FACTURACIONES_EXITO", false);
  commit("ESTADO_FACTURACIONES_ERROR", false);
  return axios
    .get(apiRest + endpoint)
    .then(({ data }) => {
      commit("ALMACENAR_FACTURACIONES_FREELO", data);
      commit("ESTADO_FACTURACIONES_EXITO", true);
    })
    .catch(err => {
      commit("ESTADO_FACTURACIONES_ERROR", true);
    });
}
