export function facturasFreelo(state) {
  let facturas = {};
  facturas = {
    tableColumns: [
      {
        name: "periodo",
        required: true,
        label: "Período",
        align: "center",
        field: row => row.name,
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "vencimiento",
        label: "Fecha de vencimiento",
        align: "center",
        field: "vencimiento",
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "totalmes",
        label: "Total del mes",
        align: "center",
        field: "totalmes",
        format: val => `${val}`,

        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "descargafactura",
        label: "Descarga tu factura",
        align: "center",
        field: "descargafactura",
        format: val => `${val}`,
        headerStyle: "font-size: 16px; text-transform:uppercase"
      },
      {
        name: "pago",
        label: "Pago realizado",
        align: "center",
        field: "pago",
        format: val => `${val}`,
        headerStyle: "font-size: 16px; text-transform:uppercase"
      }
    ],
    tableData: []
  };
  state.facturacionesFreelo.forEach(fact => {
    let fecha = new Date(fact.mes_facturado);
    let mes = fecha.toLocaleString("es-AR", { month: "long" });
    let mesCapitalizado = mes[0].toUpperCase() + mes.slice(1, mes.length);
    let year = fecha.getFullYear();

    let yearVenc = fact.fecha_vencimiento.slice(0, 4);

    let mesVenc = fact.fecha_vencimiento.slice(5, 7);

    let diaVenc = fact.fecha_vencimiento.slice(8, 11);

    let factura = {
      name: mesCapitalizado + " " + year,
      vencimiento: diaVenc + "/" + mesVenc + "/" + yearVenc,
      totalmes: "$ " + fact.monto_facturado,
      pagada: fact.pago_realizado,
      descargafactura: fact.freelo_pdf
    };
    facturas.tableData.unshift(factura);
  });
  return facturas;
}

export function facturacionesFreelo(state) {
  if (state.facturacionesFreelo.length === 0) {
    let sinDatos = [];
    let fact = {
      monto_facturado: 0,
      fecha_vencimiento: "S/D"
    };
    sinDatos.push(fact);
    return sinDatos;
  }
  return state.facturacionesFreelo;
}
