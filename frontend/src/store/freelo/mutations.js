export function ALMACENAR_FACTURACIONES_FREELO(state, payload) {
  state.facturacionesFreelo = payload;
}

export function ESTADO_FACTURACIONES_EXITO(state, payload) {
  state.estadoFacturacionesFreeloExito = payload;
}

export function ESTADO_FACTURACIONES_ERROR(state, payload) {
  state.estadoFacturacionesFreeloError = payload;
}
