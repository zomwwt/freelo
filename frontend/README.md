# Freelo Frontend

## Requirements

- NPM v6.13+
- Quasar

## Install NPM

```bash
npm install npm -g
```

More info on https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

## Install Quasar

```bash
npm install -g @quasar/cli
```

More info on https://quasar.dev/quasar-cli/installation

## Install the dependencies

```bash
npm install
```

### Just for development: start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Production: Change the API adress for production

```
In prototipo/frontend/src/store/APIs.js change the const apiRest = "https://apifreelo.herokuapp.com/" to the desired API address, keeping the const name as apiRest.
```

### Production: build the app for production

```bash
quasar build
```

<!-- ### Copy \_redirects file to build folder

```
Once the build is finished the _redirects file, located in /frontend, must be copied to the build folder. The final path of _redirects'll be /dist/spa/_redirects -->
<!-- ``` -->

### Set up nginx for history mode

```
Add
location / {
  try_files $uri $uri/ /index.html;
}
to nginx configuration
```

Config example for nginx https://quasar.dev/quasar-cli/developing-spa/deploying#General-deployment

The build will end up in the folder prototipo/frontend/dist

More info on https://quasar.dev/quasar-cli/commands-list#Build

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
