# Freelo Api Back-end + Vue.js 
> Aplicación desarrollada en Python con Arquitectura Api-Rest
## Requirements:
* Python 3.8.2
* Django 3.0.6
* Django Rest Framework 3.11.0

> Instalar todas las dependencias: $ pip install -r /path/to/projectfolder/requirements.txt


## Popular datos obligatorios a la base(1 sola vez):

* python manage.py loaddata actividades_monotributo.json
* python manage.py loaddata listado_obra_sociales.json

## Carpetas importantes:
* archivos_cur
* archivos_freelo
* archivos_vep

> Estos archivos se populan a medida que se usa el sistema, con archivos pdf y se alojarían en el servidor(en el entorno en donde está buildeado). Tener cuidado al realizar un deploy automático que estas carpetas no se vacíen, lo recomendable es realizar una copia del build corriendo en producción con estas carpetas para no perder el contenido interno.

## Deploy:

> Archivo .env contiene variables de entorno para frontend_url, allowed_host y database_url, modificar si es necesario para utilizar los servicios correctos

# Importante:

> Servir frontend/templates para consumir los archivos HTML para envio de emails. Están alojados en el proyecto frontend, pero son los únicos archivos necesarios que consume el back-end.

