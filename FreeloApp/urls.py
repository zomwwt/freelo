"""FreeloApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from apicore.views import *
from django.conf.urls import url
from rest_framework import routers
from apicore import views, views_dashboard
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from django.conf.urls.static import static
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions
router = routers.DefaultRouter()
router.register(r'usuarios', views.PersonaViewSet)
router.register(r'condiciones', views.CondicionPersonaViewSet)
router.register(r'estados-monotributo', views.EstadoMonotributoViewSet)
router.register(r'facturaciones-freelo', views.FacturacionFreeloViewSet)
router.register(r'facturaciones-monotributo', views.FacturacionMonotributoViewSet)
router.register(r'facturaciones-iibb', views.FacturacionIIBBViewSet)
router.register(r'actividades-monotributo', views.ActividadesMonotributoViewSet)
router.register(r'obra-social', views.ObraSocialViewSet)
router.register(r'actividades-arba', views.ActividadesArbaViewSet)
router.register(r'actividades-agip', views.ActividadesAgipViewSet)

schema_view = get_schema_view(
   openapi.Info(
    title="Freelo API",
    default_version='v1',
    description="Rest API builed by http://xeracto.com/",
    terms_of_service="",
    contact=openapi.Contact(email="matias.iz71@gmail.com"),
   ),public=True,permission_classes=(permissions.AllowAny,),
)
    ## los endpoints para el usuario logueado, están definidos como x-usuario
    ## no hace falta acceder a un endpoint con id usuario ya que recupera su data
    ## siempre y cuando esté logueado 
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    ## registro nuevo usuario y activar cuenta
    path('registro', registrar_cuenta, name="registro"),
    url(r'^activar-cuenta/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', activar_cuenta, name="activar_cuenta"),

    ## contraseñas endpoints
    path('recuperar-contraseña', RecuperarContraseña.as_view(), name="recuperar-contraseña"),
    url(r'^registrar-nueva-contrasena/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', registrar_nueva_contraseña, name="registrar_nueva_contraseña"),
    url(r'^cambiar-contraseña/$', CambiarContraseña.as_view(), name="cambiar-contraseña"),

    ## registros de forms 
    url(r'^registrar-no-apto/$', RegistrarNoAptos.as_view(), name="registar-no-aptos"),
    url(r'^registrar-monotributo-nuevo/$', RegistrarMonotributoNuevo.as_view(), name="registrar-monotributo"),
    url(r'^registrar-monotributo-activo/$', RegistrarMonotributoActivo.as_view(), name="registrar-monotributo-activo"),

    ## autenticacion con jwt, urls: obtener y refresh token
    path('auth/token', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token-refresh', TokenRefreshView.as_view(), name='token_refresh'),
    ## Permite a los usuarios de API verificar tokens firmados por HMAC sin tener acceso a su clave de firma
    path('auth/token-verify', TokenVerifyView.as_view(), name='token_verify'),

    ## login, logout
    url(r'^login/$', ObtenerTokenView.as_view(), name='login'),
    url(r'^logout/$', LogOutUsuario.as_view(),name='logout'),

    ## registro iibb 
    url(r'^registrar-iibb-activo/$', RegistroIIBBActivo.as_view(), name="registar-iibb"),
    url(r'^registrar-iibb-arba/$', RegistroIIBBArba.as_view(), name="registar-iibb-arba"),
    url(r'^registrar-iibb-agip/$', RegistroIIBBAgip.as_view(), name="registar-iibb-agip"),

    ## endpoints de datos personales de los usuarios(Personas) como ser también facturas, estado de cuenta, etc
    ## facturaciones-monotributo-usuario se mapea como facturaciones-monotributo
    url(r'^facturaciones-monotributo-usuario/$', FacturacionMonotributoPersona.as_view(), name="facturaciones-monotributo-usuario"),
    url(r'^estado-monotributo-usuario/$', EstadoMonotributoPersona.as_view(), name="estado-monotributo-usuario"),
    url(r'^condicion-usuario/$', CondicionPersonaData.as_view(), name="condicion-usuario"),

    url(r'^facturaciones-freelo-usuario/$', FacturacionFreeloPersona.as_view(), name="facturaciones-freelo-usuario"),


    url(r'^actualizar-info-personal/$', ActualizarDatosPersonales.as_view(),name="actualizar-info-personal"),
    url(r'^actualizar-claves/$', ActualizarClavesFiscales.as_view(),name="actualizar-claves"),

    ## actividades
    url(r'^actividades-monotributo-usuario/$', ActividadesMonotributoPersonaData.as_view(),name="actividades-monotributo"),
    url(r'^actividades-arba-usuario/$', ActividadesArbaPersonaData.as_view(),name="actividades-arba"),
    url(r'^actividades-agip-usuario/$', ActividadesAgipPersonaData.as_view(),name="actividades-agip"),
    url(r'^actualizar-actividades/$', ActualizarActividades.as_view(),name="actualizar-actividades"),

    url(r'^baja-monotributo/$', BajaMonotributo.as_view(),name="baja-monotributo"),
    url(r'^baja-freelo/$', BajaCuentaFreelo.as_view(),name="baja-freelo"),

    ## datos IIBB
    url(r'^tipo-iibb-usuario/$', TipoIIBBPersona.as_view(),name="tipo-iibb-usuario"),
    url(r'^facturaciones-iibb-usuario/$', FacturacionesIIBBPersona.as_view(),name="facturaciones-iibb"),

    ## obtener archivos PDFS por Usuario
    ## nombres deben ser sin ningún caracter especial
    url(r'^facturaciones-monotributo-usuario/archivos_cur/(?P<nombre_archivo>[0-9A-Za-z_]{1,100}.pdf)$', descargar_cur, name="descargar_cur"),
    url(r'^facturaciones-iibb-usuario/archivos_vep/(?P<nombre_archivo>[0-9A-Za-z_]{1,100}.pdf)$', descargar_vep, name="descargar_vep"),
    url(r'^facturaciones-freelo-usuario/archivos_freelo/(?P<nombre_archivo>[0-9A-Za-z_]{1,100}.pdf)$', descargar_freelo, name="descargar_freelo"),

    ## endpoints para todos los users
    url(r'^facturaciones-monotributo/usuario/archivos_cur/(?P<nombre_archivo>[0-9A-Za-z_]{1,100}.pdf)$', descargar_cur, name="descargar_cur"),
    url(r'^facturaciones-iibb/usuario/archivos_vep/(?P<nombre_archivo>[0-9A-Za-z_]{1,100}.pdf)$', descargar_vep, name="descargar_vep"),
    url(r'^facturaciones-freelo/usuario/archivos_freelo/(?P<nombre_archivo>[0-9A-Za-z_]{1,100}.pdf)$', descargar_freelo, name="descargar_freelo"),

    ## notificaciones de usuarios
    url(r'^notificaciones-usuario/$', NotificacionPersonaData.as_view(),name="notificaciones-usuario"),
    url(r'^notificacion-marcar-leido/(?P<pk_notificacion>[0-9]+)/$', NotificacionLeida.as_view(),name="notificacion-marcar-leido"),
    ## swagger endpoints
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),

    ## documentacion de la API
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    ## dashboard admin

    # usuarios
    url(r'^usuario/(?P<pk>[0-9]+)/$',UsuarioVM.as_view()),
    url(r'^usuario/(?P<pk>[0-9]+)/editar/$',UsuarioVMEditar.as_view()),
    url(r'^usuario/(?P<pk>[0-9]+)/eliminar/$',UsuarioVMEliminar.as_view()),
    url(r'^usuarios-activos$', views_dashboard.UsuariosActivos.as_view(), name='usuarios-activos'),
    url(r'^usuarios-no-activos$', views_dashboard.UsuariosNoActivos.as_view(), name='usuarios-no-activos'),
    url(r'^usuarios-no-aptos$', views_dashboard.UsuariosNoAptos.as_view(), name='usuarios-no-aptos'),

    ## facturaciones
    url(r'^facturaciones-monotributo/usuario/(?P<pk>[0-9]+)$', views_dashboard.FacturacionesMonotributoUsuario.as_view(),),
    url(r'^facturaciones-iibb/usuario/(?P<pk>[0-9]+)$', views_dashboard.FacturacionesIIBBUsuario.as_view(),),
    url(r'^facturaciones-freelo/usuario/(?P<pk>[0-9]+)$', FacturacionesFreeloVMUsuario.as_view(),),

    ## editar facturaciones
    url(r'^facturacion-monotributo/(?P<pk>[0-9]+)/editar/$', views_dashboard.FacturacionMonotributoEditar.as_view(),),
    url(r'^facturacion-monotributo/(?P<pk>[0-9]+)/eliminar/$', FacturacionMonotributoEliminar.as_view(),),
    url(r'^facturacion-iibb/(?P<pk>[0-9]+)/editar/$', views_dashboard.FacturacionIIBBEditar.as_view(),),
    url(r'^facturacion-iibb/(?P<pk>[0-9]+)/eliminar/$', FacturacionIIBBEliminar.as_view(),),
    url(r'^facturacion-freelo/(?P<pk>[0-9]+)/editar/$', views_dashboard.FacturacionFreeloEditar.as_view(),),
    url(r'^facturacion-freelo/(?P<pk>[0-9]+)/eliminar/$', FacturacionFreeloEliminar.as_view(),),

    ## crear facturaciones
    url(r'^facturacion-freelo/$', views_dashboard.FacturacionFreeloCrear.as_view(),),
    url(r'^facturacion-iibb/$', views_dashboard.FacturacionIIBBCrear.as_view(),),
    url(r'^facturacion-monotributo/$', views_dashboard.FacturacionMonotributoCrear.as_view(),),

    ## cambio de régimen/jurisdicción
    url(r'^cambio-regimen-jurisdiccion/$', views_dashboard.CambioRegimen.as_view(),),

    ## datos secundarios
    url(r'^datos-secundarios/usuario/(?P<pk>[0-9]+)$', views_dashboard.DatosSecundarios.as_view(),),

    ## notifificaciones dashboard
    url(r'^notificacion/$', views_dashboard.NotificacionesCrear.as_view(),),
    url(r'^notificacion/usuario/(?P<pk>[0-9]+)$', views_dashboard.NotificacionesUsuarioCreadas.as_view(),),
]
