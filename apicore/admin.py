from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import *
from django.contrib.admin import DateFieldListFilter

filtro_por_apellido_nombre_related_fk = ['persona__apellido','persona__nombre','persona__cuit',]
filtro_por_apellido_nombre_codigo_actividad_related_fk = ['persona__apellido','persona__nombre','persona__cuit','actividad__codigo']
## Modelos registrados en el site admin para la total 
## manipulacion de datos de los usuarios registrados en el sistema
@admin.register(Usuario)
class UserAdmin(DjangoUserAdmin):
    ## Definicion del modelo admin para usuario custom con email como usuername

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)

    def has_delete_permission(self,request,objects=None):
        return False
    
# @admin.register(Persona)
# class PersonaAdmin(admin.ModelAdmin):
#     search_fields = ['apellido','nombre',]
#     list_display = ('__str__','email','telefono','cuit','clave_afip','es_apto_uso_sistema')
#     list_filter = ('es_apto_uso_sistema','primer_login',)
#     ordering = ('apellido',)

# @admin.register(CondicionPersona)
# class CondicionPersonaAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_filter = ('gestion_activa_freelo','es_monotributista','recibe_email','declara_iibb','clave_afip_desactualizada','clave_iibb_desactualizada')
#     list_display = ('__str__','gestion_activa_freelo','es_monotributista','recibe_email','declara_iibb',)
#     ordering = ('persona__apellido',)

# @admin.register(EstadoMonotributo)
# class EstadoMonotributoAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('__str__','cuota_mensual','limite_facturacion_anual','categoria')
#     ordering = ('persona__apellido',)

# @admin.register(FacturacionFreelo)
# class FacturacionFreeloAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_filter = ('pago_realizado',('mes_facturado', DateFieldListFilter),('fecha_vencimiento', DateFieldListFilter),)
#     list_display = ('persona','monto','mes_facturado','fecha_vencimiento','pago_realizado')
#     ordering = ('persona__apellido',)

# @admin.register(FacturacionMonotributo)
# class FacturacionMonotributoAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_filter = ('pago_realizado',('mes_facturado', DateFieldListFilter),('fecha_vencimiento', DateFieldListFilter),)
#     list_display = ('persona','monto','mes_facturado','periodo','fecha_vencimiento','concepto','pago_realizado')
#     ordering = ('persona__apellido',)

# @admin.register(FacturacionIIBB)
# class FacturacionIIBBAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_filter = ('pago_realizado',('mes_facturado', DateFieldListFilter),('fecha_vencimiento', DateFieldListFilter),)
#     list_display = ('persona','monto_deuda','retencion','mes_facturado','fecha_vencimiento','concepto','pago_realizado')
#     ordering = ('persona__apellido',)

@admin.register(MotivoBajaFreelo)
class MotivoBajaFreeloAdmin(admin.ModelAdmin):
    search_fields = ['apellido','nombre']
    list_display = ('__str__','email','telefono','fecha_baja','razon')
    ordering = ('apellido',)

@admin.register(ActividadesMonotributo)
class ActividadesMonotributoAdmin(admin.ModelAdmin):
    search_fields = ['codigo','actividad',]
    list_display = ('codigo','actividad')
    ordering = ('codigo',)

# @admin.register(Domicilio)
# class DomicilioAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('persona','calle','numero','provincia','localidad','codigo_postal')
#     ordering = ('persona__apellido',)

# @admin.register(AfipPersona)
# class AfipPersonaAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_filter = ('alta_afip','actividades_en_otras_jurisdicciones','mes_actual_inicio_actividades',)
#     list_display = ('persona','cuit_empleador','cuit_caja','ley_jubilado','obra_social','alta_afip','actividades_en_otras_jurisdicciones')
#     ordering = ('persona__apellido',)

@admin.register(ActividadesMonotributoPersona)
class ActividadesMonotributoPersonaAdmin(admin.ModelAdmin):
    search_fields = filtro_por_apellido_nombre_codigo_actividad_related_fk
    list_display = ('persona','actividad','tipo_actividad','fecha_inicio')
    ordering = ('persona__apellido',)

# @admin.register(Familiar)
# class FamiliarAdmin(admin.ModelAdmin):
#     search_fields = ['apellido','nombre','persona__apellido','persona__nombre']
#     list_display =('nombre','apellido','cuit','vinculo','persona')
#     ordering = ('apellido',)

# @admin.register(MotivoPersonaNoApta)
# class MotivoPersonaNoAptaAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('persona','motivos')
#     ordering = ('persona__apellido',)

@admin.register(ObraSocial)
class ObraSocialAdmin(admin.ModelAdmin):
    search_fields = ['codigo','denominacion']
    list_display =('codigo','denominacion')
    ordering = ('codigo',)
## Arba
# @admin.register(ArbaPersona)
# class ArbaPersonaAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('persona','fecha_nacimiento','sexo','estado_civil','nacionalidad','clave_fiscal_arba')
#     ordering = ('persona__apellido',)

# @admin.register(DomicilioArba)
# class DomicilioArbaAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('persona','calle','numero','provincia','localidad','partido','codigo_postal')
#     ordering = ('persona__apellido',)

@admin.register(ActividadesArbaPersona)
class ActividadesArbaPersonaAdmin(admin.ModelAdmin):
    search_fields = filtro_por_apellido_nombre_codigo_actividad_related_fk
    list_display = ('persona','actividad','tipo_actividad','fecha_inicio')
    ordering = ('persona__apellido',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "persona":
            arba_persona = ArbaPersona.objects.all()
            queryset = Persona.objects.none()
            for arba in arba_persona:
                queryset |= Persona.objects.filter(pk=arba.persona.pk)
            kwargs["queryset"] = queryset
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
    
# Agip
# @admin.register(AgipPersona)
# class AgipPersonaAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('persona','categoria_agip','clave_fiscal_agip','limite_facturacion_anual','regimen_simplificado')
#     list_filter = ('regimen_simplificado',)
#     ordering = ('persona__apellido',)

# @admin.register(DomicilioAgip)
# class DomicilioAgipAdmin(admin.ModelAdmin):
#     search_fields = filtro_por_apellido_nombre_related_fk
#     list_display = ('persona','calle','numero','ruta','km','barrio',)
#     ordering = ('persona__apellido',)

@admin.register(ActividadesAgipPersona)
class ActividadesAgipPersonaAdmin(admin.ModelAdmin):
    search_fields = filtro_por_apellido_nombre_codigo_actividad_related_fk
    list_display = ('persona','actividad','tipo_actividad',)
    ordering = ('persona__apellido',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "persona":
            agip_persona = AgipPersona.objects.all()
            queryset = Persona.objects.none()
            for agip in agip_persona:
                queryset |= Persona.objects.filter(pk=agip.persona.pk)
            kwargs["queryset"] = queryset
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

# @admin.register(Notificacion)
# class NotificacionAdmin(admin.ModelAdmin):
#     list_display = ('titulo','contenido','persona','lista_completa_personas')
#     list_filter = ('lista_completa_personas',)

@admin.register(ActividadesAgip)
class ActividadesAgipAdmin(admin.ModelAdmin):
    search_fields = ['codigo','actividad',]
    list_display = ('codigo','actividad')
    ordering = ('codigo',)

@admin.register(ActividadesArba)
class ActividadesArbaAdmin(admin.ModelAdmin):
    search_fields = ['codigo','actividad',]
    list_display = ('codigo','actividad')
    ordering = ('codigo',)

