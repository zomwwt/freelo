from django.db import models
from django.contrib.auth.models import User, AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.core.exceptions import ValidationError
from decimal import Decimal
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.utils.timezone import activate, localtime
import datetime
import pytz
arg = pytz.timezone('America/Argentina/Buenos_Aires')
fecha_actual = arg.localize(datetime.datetime.now(),is_dst=None)
fecha_actual = datetime.datetime.now()
email_emisor = settings.EMAIL_HOST_USER
eleccion_estado_monotributo = (('ALTA','Alta'), ('BAJA','Baja'))
eleccion_categoria = (('Categoria A', 'A'), ('Categoria B', 'B'),('Categoria C', 'C'), ('Categoria D', 'D'),('Categoria E', 'E'), ('Categoria F', 'F'),('Categoria G', 'G'), ('Categoria H', 'H'), ('Categoria I', 'I'),('Sin Categoria', 'Sin Categoria'))
eleccion_tipo_actividad = (('Principal','Principal'), ('Secundaria','Secundaria'))
eleccion_concepto_monotributo = (('Facturacion','Facturacion'), ('Intereses','Intereses'),('Cuota Mensual','Cuota Mensual'))
eleccion_concepto_monotributo_iibb = (('Intereses','Intereses'),('Cuota Mensual','Cuota Mensual'))

class UserManager(BaseUserManager):
    ## Modelo nuevo para administración de usuarios
    ## Usando email como username
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Email debe ser seteado')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Super usuario debe tener is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Super usuario debe tener superuser=True.')

        return self._create_user(email, password, **extra_fields)

class Persona(models.Model):
    nombre = models.CharField(max_length=60)
    apellido = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    telefono = models.CharField(max_length=20, blank=True)
    cuit = models.CharField(max_length=13, blank=True,unique=True)
    ## por cuestiones del negocio, al momento de crear la cuenta puede ingresar la clave_afip
    ## por este motivo no la clave no está alojada en la clase AfipPersona
    clave_afip = models.CharField(max_length=64, blank=True)
    es_apto_uso_sistema = models.BooleanField(default=False)
    primer_login = models.BooleanField(default=True)

    def __str__(self):
        return self.apellido + " " + self.nombre  + " Cuit: " + self.cuit

class Usuario(AbstractUser):
    ## Email se usa como username, es decir logeo
    persona = models.ForeignKey(Persona, on_delete = models.CASCADE, null=True)
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()
## la función del token es generarlo para tener credenciales al momento de registrarse y recuperar contraseña
class Token(models.Model):
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)
    token = models.CharField(max_length=255)

class CondicionPersona(models.Model):
    gestion_activa_freelo = models.BooleanField(default=False)
    declara_iibb = models.BooleanField(default=False)
    es_monotributista = models.BooleanField(default=False)
    recibe_email = models.BooleanField(default=True)
    clave_afip_desactualizada = models.BooleanField(default=False)
    clave_iibb_desactualizada = models.BooleanField(default=False)
    persona = models.OneToOneField(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)
    def condicion_monotributo(self):
        condicion = "Alta" if self.es_monotributista == True else "Baja"
        return condicion

    def __str__(self):
        return str(self.persona) + " Condición monotributo: " + self.condicion_monotributo()

    def save(self, *args, **kwargs):
        if (self.es_monotributista) == False:
            tiene_estado_monotributo = EstadoMonotributo.objects.filter(persona=self.persona.pk)
            if tiene_estado_monotributo.exists():
                estado_monotributo = EstadoMonotributo.objects.get(persona=self.persona.pk)
                estado_monotributo.categoria = 'Sin Categoria'
                estado_monotributo.save()
        super(CondicionPersona, self).save(*args, **kwargs)

class FacturacionFreelo(models.Model):
    monto = models.DecimalField(max_digits=19, decimal_places=2)
    mes_facturado = models.DateField(auto_now=False, auto_now_add=False)
    fecha_vencimiento = models.DateField(auto_now=False, auto_now_add=False)
    facturacion_del_mes = models.DecimalField(max_digits=19, decimal_places=2)
    periodo = models.CharField(max_length=30,blank=True,null=True)
    persona = models.ForeignKey(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)
    pago_realizado = models.BooleanField(default=False)
    ## la "integración" con mercado pago es mediante un link donde ellos pagan sus mensualidad con el sistema
    link_pago = models.CharField(max_length=500,blank=True,null=True)
    freelo_pdf = models.FileField(upload_to='archivos_freelo')

    class Meta:
        unique_together = ('persona','periodo',)

    def __str__(self):
        return str(self.persona)

    def save(self, *args, **kwargs):
        self.periodo = self.mes_facturado.strftime("%Y-%m")
        if FacturacionMonotributo.objects.filter(persona=self.persona).filter(periodo=self.periodo).filter(concepto="Facturacion").exists():
            factura_instancia = FacturacionMonotributo.objects.filter(persona=self.persona).filter(periodo=self.periodo).filter(concepto="Facturacion").first()
            factura_instancia.monto = self.facturacion_del_mes
            factura_instancia.mes_facturado = self.mes_facturado
            fecha_vencimiento = self.fecha_vencimiento
            factura_instancia.save()
        else:
            FacturacionMonotributo.objects.create(persona=self.persona,monto=self.facturacion_del_mes,mes_facturado=self.mes_facturado,periodo=self.periodo,fecha_vencimiento=self.fecha_vencimiento,concepto="Facturacion")
        super(FacturacionFreelo, self).save(*args, **kwargs)

class FacturacionMonotributo(models.Model):
    monto = models.DecimalField(max_digits=19, decimal_places=2)
    mes_facturado = models.DateField(auto_now=False, auto_now_add=False)
    persona = models.ForeignKey(Persona,verbose_name=u"Usuario", on_delete=models.CASCADE)
    periodo = models.CharField(max_length=30,blank=True,null=True)
    fecha_vencimiento = models.DateField(auto_now=False, auto_now_add=False)
    cur_pdf = models.FileField(upload_to='archivos_cur',blank=True)
    pago_realizado = models.BooleanField(default=False)
    concepto = models.CharField(choices=eleccion_concepto_monotributo, max_length=25, default='Facturacion')

    class Meta:
        unique_together = ('persona','periodo','concepto',)

    def __str__(self):
        return str(self.persona)
    
    def save(self, *args, **kwargs):
        self.periodo = self.mes_facturado.strftime("%Y-%m")
        super(FacturacionMonotributo, self).save(*args, **kwargs)

class EstadoMonotributo(models.Model):
    cuota_mensual = models.DecimalField(max_digits=19, decimal_places=2,default=0)
    limite_facturacion_anual = models.DecimalField(max_digits=19, decimal_places=2,default=0)
    persona = models.OneToOneField(Persona,verbose_name=u"Usuario", on_delete=models.CASCADE)
    categoria = models.CharField(max_length=14, choices=eleccion_categoria, default='Sin Categoria')
    def estado_monotributo(self):
        condicion_persona_pk = CondicionPersona.objects.get(persona=self.persona)
        estado = "Alta" if condicion_persona_pk.es_monotributista == True else "Baja"
        return estado

    def __str__(self):
        return str(self.persona) + " Estado Monotributo: " + self.estado_monotributo()

    def clean(self):
        if self.estado_monotributo() == "Baja" and self.categoria != 'Sin Categoria':
            raise ValidationError('No puede agregar una categoría a una persona cuya condición del monotributo está como baja. Modifique la condición de la persona como monotributista para asignarle una categoría')
        super(EstadoMonotributo, self).clean()

    def save(self, *args, **kwargs):
        self.clean()
        super(EstadoMonotributo, self).save(*args, **kwargs)

class FacturacionIIBB(models.Model):
    monto_deuda = models.DecimalField(max_digits=19, decimal_places=2)
    mes_facturado = models.DateField(auto_now_add=False)
    retencion = models.DecimalField(max_digits=19, decimal_places=2,default=0)
    periodo = models.CharField(max_length=30,blank=True,null=True)
    persona = models.ForeignKey(Persona,verbose_name=u"Usuario", on_delete=models.CASCADE)
    fecha_vencimiento = models.DateField(auto_now=False, auto_now_add=False)
    pago_realizado = models.BooleanField(default=False)
    vep_pdf = models.FileField(upload_to='archivos_vep')
    concepto = models.CharField(choices=eleccion_concepto_monotributo_iibb, max_length=25, default='Facturacion')

    class Meta:
        unique_together = ('persona','periodo','concepto',)

    def __str__(self):
        return str(self.persona)

    def save(self, *args, **kwargs):
        self.periodo = self.mes_facturado.strftime("%Y-%m")
        super(FacturacionIIBB, self).save(*args, **kwargs)

class MotivoBajaFreelo(models.Model):
    nombre = models.CharField(max_length=60)
    apellido = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    telefono = models.CharField(max_length=20, blank=True) 
    razon = models.CharField(max_length=300,default='')
    fecha_baja = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.apellido) + " " + str(self.nombre)

class Domicilio(models.Model):
    provincia = models.CharField(max_length=40,default='')
    localidad = models.CharField(max_length=100,default='')
    codigo_postal = models.PositiveIntegerField()
    calle = models.CharField(max_length=100,default='')
    numero = models.PositiveIntegerField()
    piso = models.CharField(max_length=30, null=True, blank=True)
    oficina = models.CharField(max_length=30, null=True,blank=True)
    sector = models.CharField(max_length=30, null=True,blank=True)
    torre = models.CharField(max_length=30, null=True,blank=True)
    manzana = models.CharField(max_length=30, null=True,blank=True)
    persona = models.ForeignKey(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.persona)

class ObraSocial(models.Model):
    codigo = models.PositiveIntegerField()
    denominacion = models.CharField(max_length=900)

    def __str__(self):
        return str(self.codigo) + " " + str(self.denominacion)
        
class AfipPersona(models.Model):
    alta_afip = models.BooleanField(default=False)
    persona = models.ForeignKey(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)
    actividades_en_otras_jurisdicciones = models.BooleanField(default=False)
    cuit_empleador = models.CharField(max_length=13,null=True,blank=True)
    cuit_caja = models.CharField(max_length=13, null=True,blank=True)
    ley_jubilado = models.CharField(max_length=40, null=True,blank=True)
    obra_social = models.ForeignKey(ObraSocial, on_delete=models.CASCADE,null=True,blank=True)
    mes_actual_inicio_actividades = models.BooleanField(default=True)
    facturacion_anual_estimada = models.DecimalField(max_digits=19, decimal_places=2,null=True,blank=True)

    def __str__(self):
        return str(self.persona)

class ActividadesMonotributo(models.Model):
    codigo = models.PositiveIntegerField()
    actividad = models.CharField(max_length=500,default='')

    def __str__(self):
        return str(self.codigo) + " - " + self.actividad

class ActividadesMonotributoPersona(models.Model):
    actividad = models.ForeignKey(ActividadesMonotributo, on_delete=models.CASCADE)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    tipo_actividad = models.CharField(choices=eleccion_tipo_actividad, max_length=25, default='Principal')
    fecha_inicio = models.DateField(auto_now_add=False)

    def __str__(self):
        return str(self.persona) + " " + str(self.actividad) + "Tipo actividad: " + str(self.tipo_actividad)

class Familiar(models.Model):
    nombre = models.CharField(max_length=60)
    apellido = models.CharField(max_length=100)
    cuit = models.CharField(max_length=13, blank=True)
    vinculo = models.CharField(max_length=40,default='')
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    unificar_aportes_conyuge = models.BooleanField(default=False)
    def __str__(self):
        return str(self.apellido) + " " + str(self.nombre)  + " Familiar de: " + str(self.persona) + " Vínculo: " + str(self.vinculo)

class MotivoPersonaNoApta(models.Model):
    persona = models.ForeignKey(Persona,on_delete=models.CASCADE)
    motivos = models.CharField(max_length=300)

    def __str__(self):
        return str(self.persona) + " Motivos: " + str(self.motivos)

    
class ActividadesArba(models.Model):
    codigo = models.PositiveIntegerField()
    actividad = models.CharField(max_length=500,default='')

    def __str__(self):
        return str(self.codigo) + " - " + self.actividad

class ActividadesAgip(models.Model):
    codigo = models.PositiveIntegerField()
    actividad = models.CharField(max_length=500,default='')

    def __str__(self):
        return str(self.codigo) + " - " + self.actividad
    ## Registros Arba y Agip se guardan en entidades diferentes 
    ## (en el form hay domicilio y actividades)
class ArbaPersona(models.Model):

    ## Agregados para registrar ARBA
    fecha_nacimiento = models.DateField(auto_now=False,null=True, blank=True)
    sexo = models.CharField(max_length=10, null=True, blank=True,default='')
    estado_civil = models.CharField(max_length=10, null=True, blank=True,default='')
    nacionalidad = models.CharField(max_length=10, null=True, blank=True,default='')
    clave_fiscal_arba = models.CharField(max_length=100, null=True, blank=True,default='')
    persona = models.ForeignKey(Persona,verbose_name=u"Usuario",on_delete=models.CASCADE)

    def __str__(self):
        return str(self.persona)

class ActividadesArbaPersona(models.Model):
    actividad = models.ForeignKey(ActividadesArba, on_delete=models.CASCADE)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    tipo_actividad = models.CharField(choices=eleccion_tipo_actividad, max_length=25, default='Principal')
    fecha_inicio = models.DateField(auto_now_add=False)

    def __str__(self):
        return str(self.persona) + " " + str(self.actividad) + "Tipo actividad: " + str(self.tipo_actividad)

class DomicilioArba(models.Model):
    provincia = models.CharField(max_length=40,default='')
    localidad = models.CharField(max_length=100,default='')
    partido = models.CharField(max_length=100,default='')
    ruta = models.CharField(max_length=100, null=True, blank=True)
    km = models.CharField(max_length=100, null=True, blank=True)
    codigo_postal = models.PositiveIntegerField()
    calle = models.CharField(max_length=100,default='')
    numero = models.PositiveIntegerField()
    piso = models.CharField(max_length=30, null=True, blank=True)
    oficina = models.CharField(max_length=30, null=True,blank=True)
    sector = models.CharField(max_length=30, null=True,blank=True)
    torre = models.CharField(max_length=30, null=True,blank=True)
    manzana = models.CharField(max_length=30, null=True,blank=True)
    persona = models.ForeignKey(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.persona)

class AgipPersona(models.Model):
    clave_fiscal_agip = models.CharField(max_length=100, null=True, blank=True,default='')
    persona = models.ForeignKey(Persona,verbose_name=u"Usuario", on_delete=models.CASCADE)
    regimen_simplificado =  models.BooleanField(default=True)
    categoria_agip = models.CharField(max_length=2, null=True, blank=True,default='')
    limite_facturacion_anual = models.DecimalField(max_digits=19, decimal_places=2,null=True,blank=True)
    def __str__(self):
        return str(self.persona)

class ActividadesAgipPersona(models.Model):
    actividad = models.ForeignKey(ActividadesAgip, on_delete=models.CASCADE)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    tipo_actividad = models.CharField(choices=eleccion_tipo_actividad, max_length=25, default='Principal')

    def __str__(self):
        return str(self.persona) + " " + str(self.actividad) + "Tipo actividad: " + str(self.tipo_actividad)

class DomicilioAgip(models.Model):
    ruta = models.CharField(max_length=100, null=True, blank=True)
    km = models.CharField(max_length=100, null=True, blank=True)
    barrio = models.CharField(max_length=100, null=True, blank=True)
    calle = models.CharField(max_length=100)
    numero = models.PositiveIntegerField()
    piso = models.CharField(max_length=30, null=True, blank=True)
    oficina = models.CharField(max_length=30, null=True,blank=True)
    sector = models.CharField(max_length=30, null=True,blank=True)
    torre = models.CharField(max_length=30, null=True,blank=True)
    manzana = models.CharField(max_length=30, null=True,blank=True)
    persona = models.ForeignKey(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.persona)

class NotificacionesPersona(models.Model):
    persona = models.ForeignKey(Persona, verbose_name=u"Usuario", on_delete=models.CASCADE)
    leido = models.BooleanField(default=False)
    titulo = models.CharField(max_length=300)
    contenido = models.CharField(max_length=300)
    fecha_emision = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.persona) + " " + str(self.titulo) + ": " + str(self.contenido)
