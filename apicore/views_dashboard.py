from django.shortcuts import render
from rest_framework import viewsets, status
from .serializers import *
from .models import *
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, renderer_classes, parser_classes
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.generics import get_object_or_404
from rest_framework import permissions
from rest_framework import status
# from rest_framework.parsers import MultiPartParser
import datetime
from datetime import date

# variables globales
url_sitio = settings.FRONTEND_URL
response_data_correcta = 'Datos almacenados correctamente'
response_user_no_encontrado = 'Usuario no encontrado'
status_bad_request = status.HTTP_400_BAD_REQUEST
status_ok = status.HTTP_200_OK
email_emisor = settings.EMAIL_HOST_USER
fecha_actual = date.today()
## Class Views para Dashboard admin

def verificar_agente(persona):

    data_agente = {}
    data_agente["regimen_simplificado"] = False
    data_agente["agente_recaudador"] = "Sin agente"
    if AgipPersona.objects.filter(persona=persona).exists():
        ## verificar régimen para saber que mostrar
        agip_persona = AgipPersona.objects.get(persona=persona)
        data_agente["agente_recaudador"] = "AGIP"
        if agip_persona.regimen_simplificado:
            data_agente["regimen_simplificado"] = True
            data_agente["categoria_agip"]=agip_persona.categoria_agip
            data_agente["limite_facturacion_anual"]=agip_persona.limite_facturacion_anual
    if ArbaPersona.objects.filter(persona=persona).exists():
        data_agente["agente_recaudador"] = "ARBA"
    return data_agente

class UsuariosActivos(APIView):

    def get(self,request):
        lista_usuarios_aptos = Usuario.objects.filter(is_staff=False).filter(is_superuser=False)
        lista_usuarios_salida = []
        for usuario in lista_usuarios_aptos:
            condicion_persona = CondicionPersona.objects.get(persona=usuario.persona)
            # función genérica que trae a que agente pertenece la persona 
            data_agente = verificar_agente(persona=usuario.persona)

            if condicion_persona.gestion_activa_freelo:
                lista_usuarios_salida.append(
                    {
                        "pk":usuario.pk,
                        "cuit":usuario.persona.cuit,  
                        "nombre":usuario.persona.nombre,  
                        "apellido":usuario.persona.apellido,  
                        "email":usuario.persona.email,
                        "jurisdiccion":data_agente['agente_recaudador'],  
                        "regimen_simplificado":data_agente['regimen_simplificado']
                    },
                )
        return Response(lista_usuarios_salida,status=status_ok)

class UsuariosNoActivos(APIView):

    def get(self,request):
        lista_usuarios_aptos = Usuario.objects.filter(is_staff=False).filter(is_superuser=False)
        lista_usuarios_salida = []
        for usuario in lista_usuarios_aptos:
            condicion_persona = CondicionPersona.objects.get(persona=usuario.persona)

            ## definir funciones globales para detectar jurisdiccion y régimen
            data_agente = verificar_agente(persona=usuario.persona)
            if not condicion_persona.gestion_activa_freelo:
                lista_usuarios_salida.append(
                    {
                        "pk":usuario.pk,
                        "cuit":usuario.persona.cuit,  
                        "nombre":usuario.persona.nombre,  
                        "apellido":usuario.persona.apellido,  
                        "email":usuario.persona.email,
                        "jurisdiccion":data_agente['agente_recaudador'],  
                        "regimen_simplificado":data_agente['regimen_simplificado']
                    },
                )
        return Response(lista_usuarios_salida,status=status_ok)

class UsuariosNoAptos(APIView):

    def get(self,request):
        lista_usuarios_salida = []
        lista_personas_no_aptas = Persona.objects.filter(es_apto_uso_sistema=False)
        motivo_no_apto = ""
        for persona in lista_personas_no_aptas:
            if MotivoPersonaNoApta.objects.filter(persona=persona).exists():
                motivo_no_apto = MotivoPersonaNoApta.objects.filter(persona=persona).first().motivos
            else:
                motivo_no_apto = ""
            lista_usuarios_salida.append(
                {
                    "pk":persona.pk,
                    "nombre":persona.nombre,  
                    "email":persona.email,
                    "motivo":motivo_no_apto
                },
            )
        return Response(lista_usuarios_salida,status=status_ok)

class FacturacionesMonotributoUsuario(APIView):
    
    def get(self,request,pk):
        listado_facturaciones = []
        usuario = get_object_or_404(Usuario,pk=pk)
        facturaciones_monotributo = FacturacionMonotributo.objects.filter(persona=usuario.persona)
        for factura in facturaciones_monotributo:
            listado_facturaciones.append({
                    "pk":factura.pk,
                    "monto":factura.monto,
                    "comprobante_pago": request.build_absolute_uri(str(factura.cur_pdf)),
                    "periodo":factura.mes_facturado,
                    "fecha_vencimiento":factura.fecha_vencimiento,
                    "concepto":factura.concepto,
                    "estado_pago":factura.pago_realizado
                },
            )
        return Response(listado_facturaciones,status=status_ok)

class FacturacionesIIBBUsuario(APIView):

    def get(self,request,pk):
        listado_facturaciones = []
        usuario = get_object_or_404(Usuario,pk=pk)
        facturaciones_iibb = FacturacionIIBB.objects.filter(persona=usuario.persona)
        for factura in facturaciones_iibb:
            listado_facturaciones.append({
                    "pk":factura.pk,
                    "monto":factura.monto_deuda,
                    "comprobante_pago": request.build_absolute_uri(str(factura.vep_pdf)),
                    "periodo":factura.mes_facturado,
                    "fecha_vencimiento":factura.fecha_vencimiento,
                    "concepto":factura.concepto,
                    "estado_pago":factura.pago_realizado,
                    "retencion":factura.retencion
                },
            )
        return Response(listado_facturaciones,status=status_ok)

## Editar Facturaciones

class FacturacionMonotributoEditar(APIView):


    def post(self,request,pk, format=None):
        try:
            facturacion_instancia = get_object_or_404(FacturacionMonotributo,pk=pk)
            facturacion_instancia.monto = request.data.get('monto')
            if not request.data.get('comprobante_pago') == "":
                facturacion_instancia.cur_pdf = request.data.get('comprobante_pago')
            facturacion_instancia.concepto = request.data.get('concepto')
            facturacion_instancia.pago_realizado = True if not request.data.get('estado_pago') == "" else False
            facturacion_instancia.fecha_vencimiento = request.data.get('fecha_vencimiento')
            facturacion_instancia.mes_facturado = datetime.datetime.strptime(request.data.get('periodo'),"%Y-%m-%d").date()
            facturacion_instancia.save()

            dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-monotributo/usuario/'
            data_response = {}
            data_response['monto'] = facturacion_instancia.monto
            data_response['comprobante_pago'] = dominio_endpoint_descarga_archivos + (str(facturacion_instancia.cur_pdf))
            data_response['concepto'] = facturacion_instancia.concepto
            data_response['estado_pago'] = facturacion_instancia.pago_realizado
            data_response['periodo'] = facturacion_instancia.mes_facturado
            data_response['fecha_vencimiento'] = facturacion_instancia.fecha_vencimiento
            data_response['pk'] = facturacion_instancia.pk
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            e = "No se pueden repetir el periodo y concepto ingresados" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e)},status=status_bad_request)

class FacturacionIIBBEditar(APIView):


    def post(self,request,pk, format=None):
        try:
            facturacion_instancia = get_object_or_404(FacturacionIIBB,pk=pk)
            facturacion_instancia.monto_deuda = request.data.get('monto')
            if not request.data.get('comprobante_pago') == "":
                facturacion_instancia.vep_pdf = request.data.get('comprobante_pago')
            facturacion_instancia.concepto = request.data.get('concepto')
            facturacion_instancia.pago_realizado = True if not request.data.get('estado_pago') == "" else False
            facturacion_instancia.fecha_vencimiento = request.data.get('fecha_vencimiento')
            facturacion_instancia.mes_facturado = datetime.datetime.strptime(request.data.get('periodo'),"%Y-%m-%d").date()
            facturacion_instancia.save()

            dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-iibb/usuario/'
            data_response = {}
            data_response['monto'] = facturacion_instancia.monto_deuda
            data_response['comprobante_pago'] = dominio_endpoint_descarga_archivos + (str(facturacion_instancia.vep_pdf))
            data_response['concepto'] = facturacion_instancia.concepto
            data_response['estado_pago'] = facturacion_instancia.pago_realizado
            data_response['periodo'] = facturacion_instancia.mes_facturado
            data_response['fecha_vencimiento'] = facturacion_instancia.fecha_vencimiento
            data_response['pk'] = facturacion_instancia.pk
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            e = "No se pueden repetir el periodo y concepto ingresados" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e)},status=status_bad_request)

class FacturacionFreeloEditar(APIView):


    def post(self,request,pk, format=None):
        try:
            facturacion_instancia = get_object_or_404(FacturacionFreelo,pk=pk)
            facturacion_instancia.monto = request.data.get('monto_pago')
            if not request.data.get('factura') == "":
                facturacion_instancia.freelo_pdf = request.data.get('factura')
            facturacion_instancia.link_pago = request.data.get('link_pago')
            facturacion_instancia.facturacion_del_mes = request.data.get('facturacion_mes')
            facturacion_instancia.pago_realizado = True if not request.data.get('estado_pago') == "" else False
            facturacion_instancia.fecha_vencimiento = request.data.get('fecha_vencimiento')
            facturacion_instancia.mes_facturado = datetime.datetime.strptime(request.data.get('periodo'),"%Y-%m-%d").date()
            facturacion_instancia.save()

            dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-freelo/usuario/'
            data_response = {}
            data_response['monto_pago'] = facturacion_instancia.monto
            data_response['factura'] = dominio_endpoint_descarga_archivos + (str(facturacion_instancia.freelo_pdf))
            data_response['link_pago'] = facturacion_instancia.link_pago
            data_response['estado_pago'] = facturacion_instancia.pago_realizado
            data_response['periodo'] = facturacion_instancia.mes_facturado
            data_response['fecha_vencimiento'] = facturacion_instancia.fecha_vencimiento
            data_response['facturacion_mes'] = facturacion_instancia.facturacion_del_mes
            data_response['pk'] = facturacion_instancia.pk
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            e = "No se puede repetir el periodo ingresado" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e)},status=status_bad_request)

## Crear Nuevas Facturaciones

class FacturacionFreeloCrear(APIView):


    def post(self,request, format=None):
        try:
            usuario = get_object_or_404(Usuario,pk=request.data.get('persona_id'))
            persona = usuario.persona
            estado_pago = True if not request.data.get('estado_pago') == "" else False
            facturacion_instancia = FacturacionFreelo.objects.create(
                monto = request.data.get('monto_pago'),
                freelo_pdf = request.data.get('factura'),
                link_pago = request.data.get('link_pago'),
                facturacion_del_mes = request.data.get('facturacion_mes'),
                pago_realizado = estado_pago,
                fecha_vencimiento = request.data.get('fecha_vencimiento'),
                mes_facturado = datetime.datetime.strptime(request.data.get('periodo'),"%Y-%m-%d").date(),
                persona=persona
            )
    
            dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-freelo/usuario/'
            data_response = {}
            data_response['monto_pago'] = facturacion_instancia.monto
            data_response['factura'] = dominio_endpoint_descarga_archivos + (str(facturacion_instancia.freelo_pdf))
            data_response['link_pago'] = facturacion_instancia.link_pago
            data_response['estado_pago'] = facturacion_instancia.pago_realizado
            data_response['periodo'] = facturacion_instancia.mes_facturado
            data_response['fecha_vencimiento'] = facturacion_instancia.fecha_vencimiento
            data_response['facturacion_mes'] = facturacion_instancia.facturacion_del_mes
            data_response['pk'] = facturacion_instancia.pk
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            e = "No se puede repetir el periodo ingresado" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e)},status=status_bad_request)

class FacturacionIIBBCrear(APIView):


    def post(self,request, format=None):
        try:
            usuario = get_object_or_404(Usuario,pk=request.data.get('persona_id'))
            persona = usuario.persona
            estado_pago = True if not request.data.get('estado_pago') == "" else False
            facturacion_instancia = FacturacionIIBB.objects.create(
                monto_deuda = request.data.get('monto'),
                vep_pdf = request.data.get('comprobante_pago'),
                concepto = request.data.get('concepto'),
                pago_realizado = estado_pago,
                fecha_vencimiento = request.data.get('fecha_vencimiento'),
                mes_facturado = datetime.datetime.strptime(request.data.get('periodo'),"%Y-%m-%d").date(),
                persona=persona
            )
    
            dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-iibb/usuario/'
            data_response = {}
            data_response['monto'] = facturacion_instancia.monto_deuda
            data_response['comprobante_pago'] = dominio_endpoint_descarga_archivos + (str(facturacion_instancia.vep_pdf))
            data_response['concepto'] = facturacion_instancia.concepto
            data_response['estado_pago'] = facturacion_instancia.pago_realizado
            data_response['periodo'] = facturacion_instancia.mes_facturado
            data_response['fecha_vencimiento'] = facturacion_instancia.fecha_vencimiento
            data_response['pk'] = facturacion_instancia.pk
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            e = "No se puede repetir el periodo ingresado" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e)},status=status_bad_request)

class FacturacionMonotributoCrear(APIView):


    def post(self,request, format=None):
        try:
            usuario = get_object_or_404(Usuario,pk=request.data.get('persona_id'))
            persona = usuario.persona
            estado_pago = True if not request.data.get('estado_pago') == "" else False
            facturacion_instancia = FacturacionMonotributo.objects.create(
                monto = request.data.get('monto'),
                cur_pdf = request.data.get('comprobante_pago'),
                concepto = request.data.get('concepto'),
                pago_realizado = estado_pago,
                fecha_vencimiento = request.data.get('fecha_vencimiento'),
                mes_facturado = datetime.datetime.strptime(request.data.get('periodo'),"%Y-%m-%d").date(),
                persona=persona
            )
    
            dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-monotributo/usuario/'
            data_response = {}
            data_response['monto'] = facturacion_instancia.monto
            data_response['comprobante_pago'] = dominio_endpoint_descarga_archivos + (str(facturacion_instancia.cur_pdf))
            data_response['concepto'] = facturacion_instancia.concepto
            data_response['estado_pago'] = facturacion_instancia.pago_realizado
            data_response['periodo'] = facturacion_instancia.mes_facturado
            data_response['fecha_vencimiento'] = facturacion_instancia.fecha_vencimiento
            data_response['pk'] = facturacion_instancia.pk
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            e = "No se puede repetir el periodo ingresado" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e)},status=status_bad_request)

## Cambio de Régimen/Jurisdicción

class CambioRegimen(APIView):

    def post(self,request):
        try:
            usuario = get_object_or_404(Usuario,pk=request.data.get('pk'))
            persona = usuario.persona
            if AgipPersona.objects.filter(persona=persona).exists():
                agip_persona = AgipPersona.objects.filter(persona=persona).first()
                if request.data.get('organismo_recaudacion') == "agip":
                    if agip_persona.regimen_simplificado == request.data.get('regimen_simplificado'):
                        return Response({'success':False, 'response':'No se puede cambiar al mismo régimen'})
                    else:
                        if request.data.get('regimen_simplificado') == True:
                            agip_persona.clave_fiscal_agip = request.data.get('clave_fiscal')
                            agip_persona.limite_facturacion_anual = request.data.get('limite_facturacion')
                            agip_persona.categoria = request.data.get('categoria')
                            agip_persona.regimen_simplificado = True
                            agip_persona.save()
                            return Response({'success':True, 'response':'Cambio de régimen con éxito.'},status=status_ok)
                
                ## Cambio a Arba
                ArbaPersona.objects.create(persona=persona,clave_fiscal_arba=request.data.get('clave_fiscal'))

                if DomicilioAgip.objects.filter(persona=persona).exists():
                    domicilio_agip = DomicilioAgip.objects.filter(persona=persona).first()
                    DomicilioArba.objects.create(
                        ruta=domicilio_agip.ruta,
                        km=domicilio_agip.km,
                        codigo_postal=1,
                        calle=domicilio_agip.calle,
                        numero=domicilio_agip.numero,
                        piso=domicilio_agip.piso,
                        oficina=domicilio_agip.oficina,
                        sector=domicilio_agip.sector,
                        torre=domicilio_agip.torre,
                        manzana=domicilio_agip.manzana,
                        persona=persona
                    )
                    domicilio_agip.delete()
                agip_persona.delete()
                return Response({'success':True, 'response':'Cambio de Jurisdicción con éxito.'},status=status_ok)
            
            if ArbaPersona.objects.filter(persona=persona).exists():
                arba_persona = ArbaPersona.objects.filter(persona=persona).first()
                if request.data.get('organismo_recaudacion') == "agip":

                    ## Cambio a Agip
                    if request.data.get('regimen_simplificado') == True:
                        AgipPersona.objects.create(
                            persona=persona,
                            limite_facturacion_anual=request.data.get('limite_facturacion'),
                            categoria_agip= request.data.get('categoria'),
                            clave_fiscal_agip=request.data.get('clave_fiscal'),
                        )
                    else:
                        AgipPersona.objects.create(
                            persona=persona,
                            regimen_simplificado=False,
                            clave_fiscal_agip=request.data.get('clave_fiscal'),
                        )
                    if DomicilioArba.objects.filter(persona=persona).exists():
                        domicilio_arba = DomicilioArba.objects.filter(persona=persona).first()
                        DomicilioAgip.objects.create(
                            ruta=domicilio_arba.ruta,
                            km=domicilio_arba.km,
                            calle=domicilio_arba.calle,
                            numero=domicilio_arba.numero,
                            piso=domicilio_arba.piso,
                            oficina=domicilio_arba.oficina,
                            sector=domicilio_arba.sector,
                            torre=domicilio_arba.torre,
                            manzana=domicilio_arba.manzana,
                            persona=persona
                        )
                        domicilio_arba.delete()
                    arba_persona.delete()
                    return Response({'success':True, 'response':'Cambio de Jurisdicción con éxito.'},status=status_ok)
                else:
                    return Response({'success':False, 'response':'No se puede cambiar a la misma Jurisdicción'})
            return Response({'success':False, 'response':'El usuario no está inscripto en ningún régimen/jurisdicción.'})
        except Exception as e:
            return Response({'success':False, 'response':str(e)},status=status_bad_request)

class DatosSecundarios(APIView):

    def get(self,request,pk):

        try:
            usuario = get_object_or_404(Usuario,pk=pk)
            persona = usuario.persona
            data_response = []
            if AfipPersona.objects.filter(persona=persona).exists():
                afip_persona = AfipPersona.objects.filter(persona=persona).first()
                if afip_persona.obra_social is not None:
                    data_response.append({
                        "obra_social":{
                            "codigo":afip_persona.obra_social.codigo,
                            "denominacion":afip_persona.obra_social.denominacion
                        }
                    })
            if Familiar.objects.filter(persona=persona).exists():
                familiares_lista = []
                familiares_persona = Familiar.objects.filter(persona=persona)
                for familiar in familiares_persona:
                    familiares_lista.append({
                        "nombre":familiar.nombre,
                        "apellido":familiar.apellido,
                        "cuit":familiar.cuit,
                        "vinculo":familiar.vinculo,
                        "unificacion_aporte":familiar.unificar_aportes_conyuge
                    })
                data_response.append({
                    "familiares":familiares_lista
                })
            if ActividadesMonotributoPersona.objects.filter(persona=persona).exists():
                actividades_monotributo_lista = []
                actividades_monotributo = ActividadesMonotributoPersona.objects.filter(persona=persona)
                for actividad in actividades_monotributo:
                    actividades_monotributo_lista.append({
                        "fecha_inicio":actividad.fecha_inicio,
                        "codigo":actividad.actividad.codigo,
                        "nombre":actividad.actividad.actividad,
                        "primaria":actividad.tipo_actividad
                    })
                data_response.append({
                    "actividades_monotributo":actividades_monotributo_lista
                })
            if ActividadesArbaPersona.objects.filter(persona=persona).exists():
                actividades_arba_lista = []
                actividades_arba = ActividadesArbaPersona.objects.filter(persona=persona)
                for actividad in actividades_arba:
                    actividades_arba_lista.append({
                        "fecha_inicio":actividad.fecha_inicio,
                        "codigo":actividad.actividad.codigo,
                        "nombre":actividad.actividad.actividad,
                        "primaria":actividad.tipo_actividad
                    })
                data_response.append({
                    "actividades_arba":actividades_arba_lista
                })
            if ActividadesAgipPersona.objects.filter(persona=persona).exists():
                actividades_agip_lista = []
                actividades_agip = ActividadesAgipPersona.objects.filter(persona=persona)
                for actividad in actividades_agip:
                    actividades_agip_lista.append({
                        "codigo":actividad.actividad.codigo,
                        "nombre":actividad.actividad.actividad,
                        "primaria":actividad.tipo_actividad
                    })
                data_response.append({
                    "actividades_agip":actividades_agip_lista
                })
            return Response({'success':True,'response':data_response},status=status_ok)
        except Exception as e:
            return Response({'success': False,'response': str(e)},status=status_bad_request)
        
## Notificaciones para armar desde dashboard admin
class NotificacionesCrear(APIView):

    def post(self,request,format=None):
        ## validaciones iniciales
        input_validado = True
        mensaje_error = ""
        lista_usuarios_aptos = Usuario.objects.filter(is_staff=False).filter(is_superuser=False)
        if not request.data.get('lista_completa') and request.data.get('persona_id') is None:
            input_validado = False
            mensaje_error = 'Seleccione el listado completo o una persona, ambos a la vez no pueden ser seleccionados'
        
        if request.data.get('lista_completa') == True and request.data.get('persona_id') is not None:
            input_validado = False
            mensaje_error = 'Seleccione el listado completo o una persona, ambos a la vez no pueden ser seleccionados'

        if not input_validado:
            return Response({'success': False,'response': mensaje_error},status=status_bad_request)
        
        if request.data.get('lista_completa') == True:
            lista_emails = []
            for usuario in lista_usuarios_aptos:
                ## notificar
                condicion_persona = CondicionPersona.objects.get(persona=usuario.persona)
                if condicion_persona.gestion_activa_freelo:
                    NotificacionesPersona.objects.create(persona=usuario.persona,titulo=request.data.get('titulo'),contenido=request.data.get('contenido'),fecha_emision=fecha_actual)
                    if condicion_persona.recibe_email:
                        lista_emails.append(usuario.persona.email)
            if request.data.get('plantilla_para_email') is not None and bool(lista_emails):
                ## mandar por email
                titulo_email = request.data.get('titulo')
                contenido_email = request.data.get('contenido')
                texto_boton_email = request.data.get('texto_boton')
                texto_footer_email = request.data.get('texto_footer')
                html_mensaje = render_to_string('templates/{}'.format(request.data.get('plantilla_para_email')),
                {
                    'tituloNotificacion':titulo_email,
                    'subtituloNotificacion':contenido_email,
                    'textoBoton':texto_boton_email,
                    'textoFooter':texto_footer_email,
                    'url': url_sitio
                })
                mensaje_plano = strip_tags(html_mensaje)
                email_armado = EmailMultiAlternatives(titulo_email, mensaje_plano, email_emisor, bcc=lista_emails)
                email_armado.attach_alternative(html_mensaje, "text/html")
                email_armado.send()
            return Response({'success': True,'response': 'Notificaciones enviadas con éxito'},status=status_ok)

        if request.data.get('persona_id') is not None:
            usuario = get_object_or_404(Usuario,pk=request.data.get('persona_id'))
            persona = usuario.persona
            ## notificar
            condicion_persona = CondicionPersona.objects.get(persona=persona)
            if not condicion_persona.gestion_activa_freelo:
                return Response({'success': False,'response': 'El usuario no tiene gestión activa en freelo'},status=status_bad_request)
            NotificacionesPersona.objects.create(persona=persona,titulo=request.data.get('titulo'),contenido=request.data.get('contenido'))
            
            if condicion_persona.recibe_email and request.data.get('plantilla_para_email') is not None:
                ## mandar por email
                email_receptor = [persona.email]
                titulo_email = request.data.get('titulo')
                contenido_email = request.data.get('contenido')
                texto_boton_email = request.data.get('texto_boton')
                texto_footer_email = request.data.get('texto_footer')
                html_mensaje = render_to_string('templates/{}'.format(request.data.get('plantilla_para_email')),
                {
                    'tituloNotificacion':titulo_email,
                    'subtituloNotificacion':contenido_email,
                    'textoBoton':texto_boton_email,
                    'textoFooter':texto_footer_email,
                    'url': url_sitio
                })
                mensaje_plano = strip_tags(html_mensaje)
                email_armado = EmailMultiAlternatives(titulo_email, mensaje_plano, email_emisor, email_receptor)
                email_armado.attach_alternative(html_mensaje, "text/html")
                email_armado.send()
            return Response({'success': True,'response': 'Notificaciones enviadas con éxito'},status=status_ok)

class NotificacionesUsuarioCreadas(APIView):

    def get(self,request,pk):
        try:
            usuario = get_object_or_404(Usuario,pk=pk)
            persona = usuario.persona
            notificaciones = NotificacionesPersona.objects.filter(persona=persona)
            lista_notificaciones = []
            for notif in notificaciones:
                lista_notificaciones.append({
                    "titulo":notif.titulo,
                    "contenido":notif.contenido,
                    "fecha_emision":notif.fecha_emision,
                    "leido":notif.leido
                })
            return Response({'success':True, 'response':lista_notificaciones},status=status_ok)
        except Exception as e:
            return Response({'success':True, 'response':str(e)},status=status_bad_request)
        
