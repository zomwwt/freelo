from django.shortcuts import render
from rest_framework import viewsets, status
from .serializers import *
from .models import *
from django.core.mail import send_mail, EmailMultiAlternatives
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.generics import get_object_or_404
from rest_framework import permissions
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import status
from django.contrib.auth import authenticate, logout
from django.contrib.auth.models import update_last_login
import datetime
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from datetime import date
from django.db import transaction, DatabaseError
from django.db.models import Sum
from rest_framework import renderers
import random
import string
response_data_correcta = 'Datos almacenados correctamente'
response_user_no_encontrado = 'Usuario no encontrado'
status_bad_request = status.HTTP_400_BAD_REQUEST
status_ok = status.HTTP_200_OK
email_emisor = settings.EMAIL_HOST_USER
FRONTEND_URL = settings.FRONTEND_URL
# Create your views here.
fecha_actual = date.today()

## Base Render para los PDFS
class BinaryFileRenderer(renderers.BaseRenderer):
    ## prueba de media_type
    # media_type = 'application/octet-stream'
    media_type = 'application/pdf;base64'
    format = None
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data

@api_view(['GET'])
@renderer_classes([BinaryFileRenderer])
def descargar_cur(request, nombre_archivo, *args, **kwargs):
    with open('archivos_cur/{}'.format(nombre_archivo), 'rb') as report:
        return Response(
            report.read(),
            headers={'Content-Disposition': 'attachment; filename="{}"'.format(nombre_archivo)},
            content_type='application/pdf')

@api_view(['GET'])
@renderer_classes([BinaryFileRenderer])
def descargar_vep(request, nombre_archivo, *args, **kwargs):
    with open('archivos_vep/{}'.format(nombre_archivo), 'rb') as report:
        return Response(
            report.read(),
            headers={'Content-Disposition': 'attachment; filename="{}"'.format(nombre_archivo)},
            content_type='application/pdf')

@api_view(['GET'])
@renderer_classes([BinaryFileRenderer])
def descargar_freelo(request, nombre_archivo, *args, **kwargs):
    with open('archivos_freelo/{}'.format(nombre_archivo), 'rb') as report:
        return Response(
            report.read(),
            headers={'Content-Disposition': 'attachment; filename="{}"'.format(nombre_archivo)},
            content_type='application/pdf')

## ViewSets Genéricos para el api Root
class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer

class CondicionPersonaViewSet(viewsets.ModelViewSet):
    queryset = CondicionPersona.objects.all()
    serializer_class = CondicionPersonaSerializer

class EstadoMonotributoViewSet(viewsets.ModelViewSet):
    queryset = EstadoMonotributo.objects.all()
    serializer_class = EstadoMonotributoSerializer

class FacturacionFreeloViewSet(viewsets.ModelViewSet):
    queryset = FacturacionFreelo.objects.all()
    serializer_class = FacturacionFreeloSerializer

class FacturacionMonotributoViewSet(viewsets.ModelViewSet):
    queryset = FacturacionMonotributo.objects.all()
    serializer_class = FacturacionMonotributoSerializer

class FacturacionIIBBViewSet(viewsets.ModelViewSet):
    queryset = FacturacionIIBB.objects.all()
    serializer_class = FacturacionIIBBSerializer

class ActividadesMonotributoViewSet(viewsets.ModelViewSet):
    queryset = ActividadesMonotributo.objects.all()
    serializer_class = ActividadesMonotributoSerializer

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny(), ]        
        return super(ActividadesMonotributoViewSet, self).get_permissions()

class ActividadesArbaViewSet(viewsets.ModelViewSet):
    queryset = ActividadesArba.objects.all()
    serializer_class = ActividadesArbaSerializer

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny(), ]        
        return super(ActividadesArbaViewSet, self).get_permissions()

class ActividadesAgipViewSet(viewsets.ModelViewSet):
    queryset = ActividadesAgip.objects.all()
    serializer_class = ActividadesAgipSerializer

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny(), ]        
        return super(ActividadesAgipViewSet, self).get_permissions()

class DomicilioViewSet(viewsets.ModelViewSet):
    queryset = Domicilio.objects.all()
    serializer_class = DomicilioSerializer   

class ObraSocialViewSet(viewsets.ModelViewSet):
    queryset = ObraSocial.objects.all()
    serializer_class = ObraSocialSerializer

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny(), ]        
        return super(ObraSocialViewSet, self).get_permissions()

@api_view(['POST',])
@permission_classes([AllowAny])
def registrar_cuenta(request):
    if request.method == 'POST':
        serializer = RegistroSerializer(data=request.data)
        if serializer.is_valid():
            data_objeto = {}
            ## redefinir URL real del Sitio Front-end y su endpoint
            # url_sitio = request.build_absolute_uri('/activar-cuenta')
            url_sitio =  FRONTEND_URL + '/activarcuenta'
            usuario = serializer.crear_usuario(url_sitio)
            data_objeto['response'] = "Usuario creado con éxito, verifique el email para completar el registro."
            data_objeto['email'] = usuario.email
            data_objeto['success'] = True
        else:       
            data_objeto = serializer.errors
        return Response(data_objeto)

@api_view(['POST','GET'])
@permission_classes([AllowAny])
def activar_cuenta(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = Usuario.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Usuario.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        ## lo aconsejable es que el return sea un redirect a alguna vista
        return Response({"success":True})
    else:
        return Response({"success":False})

class RecuperarContraseña(APIView):
    permission_classes = [AllowAny]
    def reset_password_email(self, url_sitio,usuario):
        token = Token.objects.get(usuario=usuario)
        uid = urlsafe_base64_encode(force_bytes(usuario.pk))
        asunto = "Restaurar contraseña de su cuenta Freelo"
        link_activacion = "{}/{}/{}".format(url_sitio,uid,token.token)
        html_mensaje = render_to_string('templates/RecuperarContraseña.html',
        {
            'url':link_activacion
        })
        mensaje_plano = strip_tags(html_mensaje)
        email_receptor = [usuario.email]
        email_armado = EmailMultiAlternatives(asunto, mensaje_plano, email_emisor, email_receptor)
        email_armado.attach_alternative(html_mensaje, "text/html")
        email_armado.send()
        return True

    def post(self,request):
        data_objeto = {}
        ## chequear si exite el usuario con ese mail    
        email = request.data.get('email')
        try:
            usuario = Usuario.objects.get(email=email)
            ## redefinir URL real del Sitio Front-end y su endpoint
            # url_sitio = request.build_absolute_uri('/registrar-nueva-contrasena')
            url_sitio =  FRONTEND_URL + '/recuperarcontrasena'
            email_enviado = self.reset_password_email(url_sitio,usuario)
            if email_enviado:
                data_objeto['response'] = "Se envió un link para que pueda recuperar su contraseña a su casilla de email."
                data_objeto['email'] = usuario.email
                data_objeto['username'] = usuario.username
                data_objeto['success'] = True
        except Usuario.DoesNotExist:
            # no existe un usuario registrado con ese mail
            return Response({"response": "No hay ningun usuario registrado con este email","success":False})  
        return Response(data_objeto)

@api_view(['GET','POST'])
@permission_classes([AllowAny])
def registrar_nueva_contraseña(request, uidb64, token):
    if request.method == 'GET':
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = Usuario.objects.get(pk=uid)
            usuario_token_registrado = Token.objects.get(token=token)
            if user.id != usuario_token_registrado.usuario.id:
                return Response({'success':False, 'response':response_user_no_encontrado})
        except(TypeError, ValueError, OverflowError, Usuario.DoesNotExist, Token.DoesNotExist):
            user = None

        if user is None:
            return Response({'success':False, 'response':response_user_no_encontrado})
        return Response({'success':True, 'response':'Usuario existente, puede cambiar contraseña'})
    
    if request.method == 'POST':
        ## revalidación si el usuario cambia los valores del endpoint
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = Usuario.objects.get(pk=uid)
            usuario_token_registrado = Token.objects.get(token=token)
            if user.id != usuario_token_registrado.usuario.id:
                return Response({'success':False, 'response':response_user_no_encontrado})
        except(TypeError, ValueError, OverflowError, Usuario.DoesNotExist, Token.DoesNotExist):
            user = None
        if user is None:
            return Response({'success':False, 'response':response_user_no_encontrado})

        pw1 = request.data.get('password')
        pw2 = request.data.get('password2')
        if pw1 != pw2:
            return Response({'success':False, 'response':'Las contraseñas no coinciden'})
        user.set_password(pw1)
        user.save()
        return Response({'success':True, 'response':'Contraseña restablecida con éxito.'})

class CambiarContraseña(APIView):

    serializer_class = CambiarContraseñaSerializer
    model = Usuario
    
    def post(self, request):
        self.object = self.get_object()
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Checkear contraseña actual si es la misma
        if not self.object.check_password(serializer.data.get("contraseña_actual")):
            return Response({"success":False,"mensaje": "Error, la contraseña actual no es la registrada."}, status=status_bad_request)
        self.object.set_password(serializer.data.get("contraseña_nueva"))
        self.object.save()
        response = {
            'success': True,
            'mensaje': 'Contraseña actualizada con éxito',
        }
        return Response(response, status=status_ok)
    def get_object(self, queryset=None):
        obj = self.request.user
        return obj
class RegistrarNoAptos(APIView):
    permission_classes = [AllowAny]
    def post(self,request):
        ## se deberán almacenar todos aquellos potenciales usuarios que no son aptos para usar el sistema
        random_string  = ''.join(random.choice(request.data.get('nombre') + request.data.get('email')) for i in range(10))
        persona_no_apta = Persona.objects.create(nombre=request.data.get('nombre'),cuit=random_string)
        persona_no_apta.email = request.data.get('email')
        persona_no_apta.es_apto_uso_sistema = False
        persona_no_apta.save()
        MotivoPersonaNoApta.objects.create(persona=persona_no_apta,motivos=request.data.get('motivos'))

        return Response({'success': True,'response': response_data_correcta})

class RegistrarMonotributoNuevo(APIView):

    def post(self,request):
        ## se pueden dar dos casos distintos: A. form completo , B. ya tiene monotributo
        ## caso A => guardar todos los datos: domicilio (puede tener real y fiscal), jurisd, cuit_empleador, jubilado, cuit_caja actividades (pueden ser varias)
        ## cuit_empleador, jubilado, cuit_caja pueden ser nulos
        ## familiares guadar en Familiar, pueden ser varios, el id_persona seria aquel usuario.id_persona.pk logueado
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            persona.cuit = request.data.get('cuit')
            persona.telefono = request.data.get('telefono')
            if request.data.get('clave_afip') is not None:
                persona.clave_afip = request.data.get('clave_afip')
            persona.save()

            ## 29/07 => Si el usuario ya tiene datos cargados, y no llegó hasta el último form,
            ## puede darse el caso de que vuelva a ingresar al sistema,
            ## y tenga que cargar datos devuelta, para no generar duplicados, verificamos si ya existen 
            ## datos cargados, en ese caso, lo eliminamos y sobreescribimos
            if Domicilio.objects.filter(persona=persona).exists():
                Domicilio.objects.filter(persona=persona).delete()

            domicilio_serializer = DomicilioSerializer(data=request.data.get('domicilio'))
            domicilio_serializer.is_valid(raise_exception=True)
            domicilio_serializer.save(persona=request.user.persona)

            if AfipPersona.objects.filter(persona=persona).exists():
                AfipPersona.objects.filter(persona=persona).delete()

            afip_persona_serializer = AfipPersonaSerializer(data=request.data.get('afip_persona'))
            afip_persona_serializer.is_valid(raise_exception=True)
            if request.data.get('obra_social') is not None:
                obra_social_input = dict(request.data.get('obra_social'))
                obra_social = ObraSocial.objects.get(codigo=obra_social_input["codigo"])
                afip_persona_serializer.save(obra_social=obra_social,persona=request.user.persona)
            else:
                afip_persona_serializer.save(persona=request.user.persona)

            # ## registrar las actividades que elegió el usuario-persona
            # ## tomar la instancia de afip_persona recien generada
            # afip_persona = AfipPersona.objects.get(persona=request.user.persona)
            if ActividadesMonotributoPersona.objects.filter(persona=persona).exists():
                ActividadesMonotributoPersona.objects.filter(persona=persona).delete()

            actividades = dict(request.data.get('actividades'))
            for (k, v) in actividades.items():
                codigo_actividad = v.get('codigo')
                tipo_actividad = v.get('tipo_actividad')
                fecha_inicio = v.get('fecha_inicio')
                actividad_instancia = ActividadesMonotributo.objects.get(codigo=codigo_actividad)
                actividad_persona = ActividadesMonotributoPersona.objects.create(actividad=actividad_instancia,persona=persona,tipo_actividad=tipo_actividad,fecha_inicio=fecha_inicio)
                actividad_persona.save()
            
            if Familiar.objects.filter(persona=persona).exists():
                Familiar.objects.filter(persona=persona).delete()

            ## en el caso que eliga trabajador activo debería familiares ( si el usuario desea)
            familiares = dict(request.data.get('familiares'))
            for (k, v) in familiares.items():
                familiar_instancia = Familiar.objects.create(persona=request.user.persona)
                familiar_instancia.nombre = v.get('nombre')
                familiar_instancia.apellido = v.get('apellido')
                familiar_instancia.cuit = v.get('cuit')
                familiar_instancia.vinculo = v.get('vinculo')
                if v.get('unificar_aportes') is not None:
                    familiar_instancia.unificar_aportes_conyuge = v.get('unificar_aportes')
                familiar_instancia.save()
            
            if not EstadoMonotributo.objects.filter(persona=persona).exists():
                EstadoMonotributo.objects.create(persona=persona)

            return Response({'success': True,'response': response_data_correcta})
        except Exception as e:
            e = "Cuit debe ser único" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e),'error': str(e)},status=status_bad_request)

## Caso B => Por front muestra: Categoría, cuota mensual, límite de fact anual, y deberá guardar clave fiscal AFIP ( SOLO CLAVE SE GUARDA!)
class RegistrarMonotributoActivo(APIView):

    def post(self, request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            if persona is not None:
                persona.clave_afip = request.data.get('clave_afip')
                persona.cuit = request.data.get('cuit')
                persona.telefono = request.data.get('telefono')
                condicion_persona = CondicionPersona.objects.get(persona=persona)
                condicion_persona.es_monotributista = True

                if not AfipPersona.objects.filter(persona=persona).exists():
                    AfipPersona.objects.create(persona=persona,alta_afip=True)
                    
                ## la declaracion debe venir en formato string concatenados si es que eligío mas de una opción en el checkbox
                if not EstadoMonotributo.objects.filter(persona=persona).exists():
                    EstadoMonotributo.objects.create(persona=persona)
                condicion_persona.save()
                persona.save()
                return Response({'success': True,'response': response_data_correcta})
            else:
                return Response({'success': False,'response': response_user_no_encontrado})
        except Exception as e:
            e = "Cuit debe ser único" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e),'error': str(e)},status=status_bad_request)
        

class RegistroIIBBActivo(APIView):
    
    def post(self, request):
        persona = Persona.objects.get(pk=request.user.persona.pk)
        condicion_persona = CondicionPersona.objects.get(persona=persona)
        condicion_persona.declara_iibb = True
        condicion_persona.save()
        if request.data.get('clave_fiscal_agip') is not None:
            if AgipPersona.objects.filter(persona=persona).exists():
                agip_persona = AgipPersona.objects.get(persona=persona)
                agip_persona.clave_fiscal_agip = request.data.get('clave_fiscal_agip')
                agip_persona.save()
            else:
                AgipPersona.objects.create(clave_fiscal_agip=request.data.get('clave_fiscal_agip'),persona=persona)
        if request.data.get('clave_fiscal_arba') is not None:
            if ArbaPersona.objects.filter(persona=persona).exists():
                arba_persona = ArbaPersona.objects.get(persona=persona)
                arba_persona.clave_fiscal_arba = request.data.get('clave_fiscal_arba')
                arba_persona.save()
            else:
                ArbaPersona.objects.create(clave_fiscal_arba=request.data.get('clave_fiscal_arba'),persona=persona)
        if persona.primer_login:
            persona.primer_login = False
            persona.save()
        return Response({'success': True,'response': response_data_correcta})

class RegistroIIBBArba(APIView):

    def post(self,request):
        ## arba_persona => solo necesita fecha_nacimiento, sexo, estado_civil y nacionalidad para almacenar, el resto no es necesario
        persona = Persona.objects.get(pk=request.user.persona.pk)
        if ArbaPersona.objects.filter(persona=persona).exists():
            ArbaPersona.objects.filter(persona=persona).delete()
        arba_persona_serializer = ArbaPersonaSerializer(data=request.data.get('arba_persona'))
        arba_persona_serializer.is_valid(raise_exception=True)
        arba_persona_serializer.save(persona=persona)

        ## registro de actividades
        if ActividadesArbaPersona.objects.filter(persona=persona).exists():
            ActividadesArbaPersona.objects.filter(persona=persona).delete()
        actividades = dict(request.data.get('actividades'))
        for (k, v) in actividades.items():
            codigo_actividad = v.get('codigo')
            tipo_actividad = v.get('tipo_actividad')
            fecha_inicio = v.get('fecha_inicio')
            actividad_instancia = ActividadesArba.objects.get(codigo=codigo_actividad)
            ActividadesArbaPersona.objects.create(actividad=actividad_instancia,persona=persona,tipo_actividad=tipo_actividad,fecha_inicio=fecha_inicio)

        ## registro del domicilio arba(en realidad es el mismo pero se almacenan en entidades diferentes por reglas del negocio)
        if DomicilioArba.objects.filter(persona=persona).exists():
            DomicilioArba.objects.filter(persona=persona).delete()
        domicilio_arba_serializer = DomicilioArbaSerializer(data=request.data.get('domicilio'))
        domicilio_arba_serializer.is_valid(raise_exception=True)
        domicilio_arba_serializer.save(persona=persona)
        if persona.primer_login:
            persona.primer_login = False
            persona.save()
        return Response({'success': True,'response': response_data_correcta})

class RegistroIIBBAgip(APIView):

    def post(self,request):
        ## serializador data => regimen y categoria_agip
        persona = Persona.objects.get(pk=request.user.persona.pk)
        if AgipPersona.objects.filter(persona=persona).exists():
            AgipPersona.objects.filter(persona=persona).delete()
        agip_persona_serializer = AgipPersonaSerializer(data=request.data.get('agip_persona'))
        agip_persona_serializer.is_valid(raise_exception=True)
        agip_persona_serializer.save(persona=persona)

        ## registro de actividades
        if  ActividadesAgipPersona.objects.filter(persona=persona).exists():
            ActividadesAgipPersona.objects.filter(persona=persona).delete()
        actividades = dict(request.data.get('actividades'))
        for (k, v) in actividades.items():
            codigo_actividad = v.get('codigo')
            tipo_actividad = v.get('tipo_actividad')
            actividad_instancia = ActividadesAgip.objects.get(codigo=codigo_actividad)
            ActividadesAgipPersona.objects.create(actividad=actividad_instancia,persona=persona,tipo_actividad=tipo_actividad)

        ## registro domicilio agip
        if DomicilioAgip.objects.filter(persona=persona).exists():
            DomicilioAgip.objects.filter(persona=persona).delete()
        domicilio_agip_serializer = DomicilioAgipSerializer(data=request.data.get('domicilio'))
        domicilio_agip_serializer.is_valid(raise_exception=True)
        domicilio_agip_serializer.save(persona=persona)
        if persona.primer_login:
            persona.primer_login = False
            persona.save()
        return Response({'success': True,'response': response_data_correcta})

class FacturacionMonotributoPersona(APIView):
    
    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            facturacion_persona = FacturacionMonotributo.objects.filter(persona=persona).order_by('mes_facturado')
            facturaciones = []
            for factura in facturacion_persona:
                facturaciones.append({
                    "monto_facturado": factura.monto,
                    "mes_facturado": factura.mes_facturado,
                    "fecha_vencimiento": factura.fecha_vencimiento,
                    "pago_realizado": factura.pago_realizado,
                    "concepto":factura.concepto,
                    "cur_pdf": request.build_absolute_uri(str(factura.cur_pdf))
                })
            return Response(facturaciones,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class EstadoMonotributoPersona(APIView):

    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            estado_monotributo = EstadoMonotributo.objects.filter(persona=persona).first()
            data = {}
            data['cuota_mensual'] = estado_monotributo.cuota_mensual
            data['limite_facturacion_anual'] = estado_monotributo.limite_facturacion_anual
            data['categoria'] = estado_monotributo.categoria
            return Response(data,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class ObtenerTokenView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = ObtenerTokenSerializer

class LogOutUsuario(APIView):

    def get(self, request, format=None):
        logout(request)
        return Response(status=status_ok)


class ActividadesMonotributoPersonaData(APIView):

    def get(self,request):
        persona = Persona.objects.get(pk=request.user.persona.pk)
        actividades = []
        if ActividadesMonotributoPersona.objects.filter(persona=persona).exists():
            actividades_monotributo = ActividadesMonotributoPersona.objects.filter(persona=persona)
            for actividad_instancia in actividades_monotributo:
                actividades.append({
                    'codigo': actividad_instancia.actividad.codigo,
                    'actividad':actividad_instancia.actividad.actividad,
                    'tipo_actividad':actividad_instancia.tipo_actividad,
                    'fecha_inicio': actividad_instancia.fecha_inicio
                })
            return Response(actividades,status=status_ok)
        else:
            return Response({'response':'No posee actividades'},status=status_bad_request)

class ActividadesArbaPersonaData(APIView):

    def get(self,request):
        persona = Persona.objects.get(pk=request.user.persona.pk)
        actividades = []
        if ActividadesArbaPersona.objects.filter(persona=persona).exists():
            actividades_arba = ActividadesArbaPersona.objects.filter(persona=persona)
            for actividad_instancia in actividades_arba:
                actividades.append({
                    'codigo': actividad_instancia.actividad.codigo,
                    'actividad':actividad_instancia.actividad.actividad,
                    'tipo_actividad':actividad_instancia.tipo_actividad,
                    'fecha_inicio': actividad_instancia.fecha_inicio
                })
            return Response(actividades,status=status_ok)
        else:
            return Response({'response':'No posee actividades'},status=status_bad_request)

class ActividadesAgipPersonaData(APIView):

    def get(self,request):
        persona = Persona.objects.get(pk=request.user.persona.pk)
        actividades = []
        if ActividadesAgipPersona.objects.filter(persona=persona).exists():
            actividades_agip = ActividadesAgipPersona.objects.filter(persona=persona)
            for actividad_instancia in actividades_agip:
                actividades.append({
                    'codigo': actividad_instancia.actividad.codigo,
                    'actividad':actividad_instancia.actividad.actividad,
                    'tipo_actividad':actividad_instancia.tipo_actividad
                })
            return Response(actividades,status=status_ok)
        else:
            return Response({'response':'No posee actividades'},status=status_bad_request)

## Edicion de Información de los usuarios
class ActualizarDatosPersonales(APIView):
    def post(self,request):
        try:
            ## el usuario al actualizar el mail, actualizar su login! tener en cuenta para notificar eso
            usuario = request.user
            email = request.data.get('email')
            telefono = request.data.get('telefono')
            usuario.email = email
            usuario.save()
            persona = Persona.objects.get(pk=request.user.persona.pk)
            persona_serializer = PersonaSerializer(instance=persona,data=request.data.get('persona'))
            persona_serializer.is_valid(raise_exception=True)
            persona_serializer.save(email=email,telefono=telefono)
            condicion_persona = CondicionPersona.objects.get(persona=persona)
            condicion_persona.recibe_email = request.data.get('recibe_email')
            condicion_persona.save()
            return Response({'success':True,'response':'Datos almacenados correctamente', 'status':status_ok})
        except Exception as e:
            e = "Cuit debe ser único" if "UNIQUE" in str(e) else e
            return Response({'success': False,'response': str(e), 'status code':status_bad_request})

class ActualizarActividades(APIView):

    def post(self,request):
        try:
            ## El panel de actualización es 1, pero las actividades pueden variar de donde se consuman, porque para cada agente, régimen, varían de donde consumir los datos
            ## Por lo tanto sería bueno discriminar desde el front, las actividades de cada uno para que se realizen las actualizaciones correctas
            persona = Persona.objects.get(pk=request.user.persona.pk)
            actividades_monotributo_dict = dict(request.data.get('actividades_monotributo'))
            actividades_arba_dict = dict(request.data.get('actividades_arba'))
            actividades_agip_dict = dict(request.data.get('actividades_agip'))

            if not actividades_monotributo_dict:
                for (k, v) in actividades_monotributo_dict.items():
                    tipo_actividad = v.get('tipo_actividad')
                    fecha_inicio = v.get('fecha_inicio')
                    ## data de la Actividad Monotributo para vincular las claves
                    codigo_actividad = v.get('codigo')
                    actividad_monotributo_instancia = ActividadesMonotributo.objects.get(codigo=codigo_actividad)
                    
                    ## tomar de la lista de las actividades ya creadas, aquella que se va a actualizar a la nueva actividad
                    if ActividadesMonotributoPersona.objects.filter(actividad=actividad_monotributo_instancia,persona=persona).exists():
                        ## en el caso que sea la misma actividad, actualizar el tipo actividad y fecha.
                        actividad_instancia = ActividadesMonotributoPersona.objects.get(actividad=actividad_monotributo_instancia,persona=persona)
                        actividad_instancia.tipo_actividad = tipo_actividad
                        actividad_instancia.fecha_inicio = fecha_inicio
                        actividad_instancia.save()
                    else: ## en el caso que el usuario ingresa más actividades de las que ya tenía registrado, corresponde a generar una instancia totalmente nueva
                        ActividadesMonotributoPersona.objects.create(actividad=actividad_monotributo_instancia,persona=persona,tipo_actividad=tipo_actividad,fecha_inicio=fecha_inicio)
            
            if not actividades_arba_dict:
                for (k, v) in actividades_arba_dict.items():
                    tipo_actividad = v.get('tipo_actividad')
                    fecha_inicio = v.get('fecha_inicio')
                    codigo_actividad = v.get('codigo')
                    ## las actividades arba, agip y monotributo quedó resuelto que son diferentes
                    actividad_arba_instancia = ActividadesArba.objects.get(codigo=codigo_actividad)
                    
                    if ActividadesArbaPersona.objects.filter(actividad=actividad_arba_instancia,persona=persona).exists():
                        actividad_instancia = ActividadesArbaPersona.objects.get(actividad=actividad_arba_instancia,persona=persona)
                        actividad_instancia.tipo_actividad = tipo_actividad
                        actividad_instancia.fecha_inicio = fecha_inicio
                        actividad_instancia.save()
                    else:
                        ActividadesArbaPersona.objects.create(actividad=actividad_arba_instancia,persona=persona,tipo_actividad=tipo_actividad,fecha_inicio=fecha_inicio)
            
            if not actividades_agip_dict:
                 for (k, v) in actividades_agip_dict.items():
                    tipo_actividad = v.get('tipo_actividad')
                    codigo_actividad = v.get('codigo')
                    actividad_agip_instancia = ActividadesAgip.objects.get(codigo=codigo_actividad)
                    
                    if ActividadesAgipPersona.objects.filter(actividad=actividad_agip_instancia,persona=persona).exists():
                        actividad_instancia = ActividadesAgipPersona.objects.get(actividad=actividad_agip_instancia,persona=persona)
                        actividad_instancia.tipo_actividad = tipo_actividad
                        actividad_instancia.save()
                    else:
                        ActividadesAgipPersona.objects.create(actividad=actividad_agip_instancia,persona=persona,tipo_actividad=tipo_actividad)
                   
            return Response({'success':True,'response':'Datos almacenados correctamente', 'status':status_ok})
        except Exception as e:
            return Response({'success': False,'response': str(e), 'status code':status_bad_request})

## tanto la baja de monotributo como de freelo, se notifican (via email) a los administradores del sistema sobre la baja de los usuarios
class BajaMonotributo(APIView):
    ## si el usuario se quiere dar de baja en IIBB, que apunte al mismo endpoint con una data agregada de IIBB
    def post(self,request):
        persona = Persona.objects.get(pk=request.user.persona.pk)
        clave_afip_entry = request.data.get('clave_afip')
        contraseña_entry = request.data.get('contraseña')
        # condicion_persona = CondicionPersona.objects.get(persona=persona)
        if clave_afip_entry != persona.clave_afip:
            return Response({'success':False,'response':'Clave fiscal no coincide con la registrada actualmente'},status=status_bad_request)
        
        if not request.user.check_password(contraseña_entry):
            return Response({"success":False,"mensaje": "Error, la contraseña actual no es la registrada."}, status=status_bad_request)
        
        baja_iibb = request.data.get('baja_iibb') ## verdadero o false
        solicitud_adicional_por_iibb = ""
        if baja_iibb:
            solicitud_adicional_por_iibb = "El mismo desea darse de baja en IIBB"
            
        asunto = "Solicitud de baja Monotributo"
        mensaje = "Usuario: " + request.user.get_full_name() + ". Cuit: " + persona.cuit + ", Clave fiscal: " + clave_afip_entry + ", desea darse de baja en monotributo. " + solicitud_adicional_por_iibb
        email_receptor = [email_emisor]
        send_mail(asunto, mensaje, email_emisor, email_receptor)
        return Response({'success':True,'response':'Dado de baja correctamente'},status=status_ok)

class BajaCuentaFreelo(APIView):

    def post(self,request):
        persona = Persona.objects.get(pk=request.user.persona.pk)
        clave_afip_entry = request.data.get('clave_afip')
        contraseña_entry = request.data.get('contraseña')
        # condicion_persona = CondicionPersona.objects.get(persona=persona)
        if clave_afip_entry != persona.clave_afip:
            return Response({'success':False,'response':'Clave fiscal no coincide con la registrada actualmente'},status=status_bad_request)
        
        if not request.user.check_password(contraseña_entry):
            return Response({'success':False,"response": "Error, la contraseña actual no es la registrada."}, status=status_bad_request)
        
        asunto = "Baja de cuenta Freelo"
        mensaje = "El Usuario: " + request.user.get_full_name() + ". Cuit: " + persona.cuit + ", Clave fiscal:" + clave_afip_entry + ", solicitó darse de baja en su cuenta Freelo."
        email_receptor = [email_emisor]
        send_mail(asunto, mensaje, email_emisor, email_receptor)
        condicion_persona = CondicionPersona.objects.get(persona=persona)
        condicion_persona.gestion_activa_freelo = False
        condicion_persona.save()
        MotivoBajaFreelo.objects.create(razon=request.data.get('comentario'),fecha_baja=fecha_actual,nombre=persona.nombre,apellido=persona.apellido,email=persona.email,telefono=persona.telefono,)
        # persona.delete()
        # usuario = request.user
        # usuario.delete()
        return Response({'success':True,'response':'Dado de baja correctamente'},status=status_ok)

class FacturacionFreeloPersona(APIView):

    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            facturacion_persona = FacturacionFreelo.objects.filter(persona=persona).order_by('mes_facturado')
            facturaciones = []
            for factura in facturacion_persona:
                facturaciones.append({
                    "monto_facturado": factura.monto,
                    "mes_facturado": factura.mes_facturado,
                    "fecha_vencimiento": factura.fecha_vencimiento,
                    "pago_realizado": factura.pago_realizado,
                    "link_pago": str(factura.link_pago),
                    "freelo_pdf": request.build_absolute_uri(str(factura.freelo_pdf)) 
                })
            return Response(facturaciones,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class ActualizarClavesFiscales(APIView):

    def post(self,request):
        try:
            ## la clave para indentificar si es agip o arba, solamente consultamos en cúal de los 2 agentes tiene datos, 
            persona = Persona.objects.get(pk=request.user.persona.pk)
            condicion_persona = CondicionPersona.objects.get(persona=persona)
            clave_afip = request.data.get('clave_afip')
            clave_iibb = request.data.get('clave_iibb')  
            if clave_afip is not None:
                persona.clave_afip = clave_afip
                condicion_persona.clave_afip_desactualizada = False
                persona.save()
            if clave_iibb is not None:
                ## consultar a que agente corresponde el usuario
                condicion_persona.clave_iibb_desactualizada = False
                if ArbaPersona.objects.filter(persona=persona).exists():
                    arba_persona = ArbaPersona.objects.get(persona=persona)
                    arba_persona.clave_fiscal_arba = clave_iibb
                    arba_persona.save()
                if AgipPersona.objects.filter(persona=persona).exists():
                    agip_persona = AgipPersona.objects.get(persona=persona)
                    agip_persona.clave_fiscal_agip = clave_iibb
                    agip_persona.save()
            condicion_persona.save()
            return Response({'success':True,'response':'Clave/s actualizada/s'},status=status_ok)

        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class FacturacionesIIBBPersona(APIView):
    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            facturacion_persona = FacturacionIIBB.objects.filter(persona=persona).order_by('mes_facturado')
            facturaciones = []
            for factura in facturacion_persona:
                facturaciones.append({
                    "monto_facturado": factura.monto_deuda,
                    "mes_facturado": factura.mes_facturado,
                    "retencion": factura.retencion,
                    "fecha_vencimiento": factura.fecha_vencimiento,
                    "pago_realizado": factura.pago_realizado,
                    "concepto":factura.concepto,
                    "vep_pdf": request.build_absolute_uri(str(factura.vep_pdf)) 
                })
            return Response(facturaciones,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class TipoIIBBPersona(APIView):
    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            data_agente = {}
            if AgipPersona.objects.filter(persona=persona).exists():
                ## verificar régimen para saber que mostrar
                agip_persona = AgipPersona.objects.get(persona=persona)
                data_agente["agente_recaudador"] = "AGIP"
                data_agente["regimen_simplificado"] = False
                if agip_persona.regimen_simplificado:
                    data_agente["regimen_simplificado"] = True
                    data_agente["categoria_agip"]=agip_persona.categoria_agip
                    data_agente["limite_facturacion_anual"]=agip_persona.limite_facturacion_anual
            if ArbaPersona.objects.filter(persona=persona).exists():
                data_agente["agente_recaudador"] = "ARBA"
            return Response(data_agente,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class CondicionPersonaData(APIView):

    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            condicion_persona = CondicionPersona.objects.filter(persona=persona).first()
            data = {}
            data['gestion_activa_freelo'] = condicion_persona.gestion_activa_freelo
            data['declara_iibb'] = condicion_persona.declara_iibb
            data['es_monotributista'] = condicion_persona.es_monotributista
            data['recibe_email'] = condicion_persona.recibe_email
            data['clave_afip_desactualizada'] = condicion_persona.clave_afip_desactualizada
            data['clave_iibb_desactualizada'] = condicion_persona.clave_iibb_desactualizada
            return Response(data,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

## notificaciones usuarios
class NotificacionPersonaData(APIView):

    def get(self,request):
        try:
            persona = Persona.objects.get(pk=request.user.persona.pk)
            notificaciones = NotificacionesPersona.objects.filter(persona=persona)
            lista_notificaciones = []
            for notificacion in notificaciones:
                lista_notificaciones.append({
                    "pk_notificacion":notificacion.pk,
                    "leido": notificacion.leido,
                    "fecha_emision": notificacion.fecha_emision.strftime("%Y-%m-%d, %H:%M:%S"),
                    "titulo": notificacion.titulo,
                    "contenido": notificacion.contenido
                })
            return Response(lista_notificaciones,status=status_ok)
        except Exception as e:
            response = {'success': False,'response': str(e)}
            return Response(response, status=status_bad_request)

class NotificacionLeida(APIView):

    def post(self,request,pk_notificacion):
        instancia_notificacion = get_object_or_404(NotificacionesPersona, pk=pk_notificacion)
        instancia_notificacion.leido = True
        instancia_notificacion.save()
        return Response({'success':True,'response':'Notificacion marcada'},status=status_ok)

#Clases para el dashboard admin

#usuario: es el formato definido en: https://docs.google.com/document/d/1XHWvNiRBo4uQXv4Na_CIxYuI7RTP1hCuKhjuCiTAhkg/edit

class FacturacionesFreeloVMUsuario(APIView):

    permission_classes = [AllowAny,]

    def get(self,request,pk):
        usuario = get_object_or_404(Usuario, pk=pk)
        print(usuario)
        persona = usuario.persona
        if persona is None:
            return Response({'success':False,'response':'usuario no tiene persona vinculada.'},status=status_ok)
        facturas = FacturacionFreelo.objects.filter(persona=persona)
        facturasvm = []
        dominio_endpoint_descarga_archivos = request.build_absolute_uri('/')[:-1] + '/facturaciones-freelo/usuario/'

        for factura in facturas:
            facturasvm.append({
                    "pk":factura.pk,
                    "facturacion_mes":factura.facturacion_del_mes,
                    "link_pago":factura.link_pago,
                    "periodo":factura.mes_facturado,
                    "fecha_vencimiento":factura.fecha_vencimiento,
                    "estado_pago":factura.pago_realizado,
                    "monto_pago":factura.monto,
                    "factura":dominio_endpoint_descarga_archivos + (str(factura.freelo_pdf))
                }
            )
        return Response({'success':True,'response':facturasvm},status=status_ok)
        



class UsuarioVM(APIView):

    permission_classes = [AllowAny,]
    
    def get(self,request,pk):
        usuario = get_object_or_404(Usuario, pk=pk)
        persona = usuario.persona
        if persona is None : 
            return Response({'success':False,'response':'usuario no tiene persona vinculada.'},status=status_ok)
        
        condicion = CondicionPersona.objects.filter(persona = persona).first()
        if condicion is None :
            return Response({'success':False,'response':'persona no tiene condición vinculada.'},status=status_ok)

        estado_monotributo = EstadoMonotributo.objects.filter(persona = persona).first()
        if estado_monotributo is None :
            return Response({'success':False,'response':'persona no tiene un estado monotributo vinculado.'},status=status_ok)
        
        if not persona.es_apto_uso_sistema:
            usuario_vm = {
                "es_no_apto":True,
                "datos_personales":{
                    "nombre":persona.nombre,
                    "apellido":persona.apellido,
                    "email":persona.email
                }
            }
            return Response({'success':True,'response':usuario_vm},status=status_ok)


        es_agip = AgipPersona.objects.filter(persona=persona).exists()
        es_arba = ArbaPersona.objects.filter(persona=persona).exists()
        
        arba_persona = ArbaPersona.objects.get(persona=persona) if es_arba else None
        agip_persona = AgipPersona.objects.get(persona=persona) if es_agip else None

        clave_organismo_facturacion = arba_persona.clave_fiscal_arba if es_arba else agip_persona.clave_fiscal_agip if es_agip else None

        entidad = "agip" if es_agip else "arba" if es_arba else None
        
        categoria = None
        limite_facturacion = None
        regimen_simplificado = False
        if es_agip:
            persona_agip = AgipPersona.objects.get(persona=persona)
            regimen_simplificado = persona_agip.regimen_simplificado
            categoria = persona_agip.categoria_agip
            limite_facturacion = persona_agip.limite_facturacion_anual
            
        domicilio_general = Domicilio.objects.filter(persona = persona).first()
        domicilio_general = {
            "provincia": domicilio_general.provincia,
            "localidad":domicilio_general.localidad,
            "codigo_postal":domicilio_general.codigo_postal,
            "calle":domicilio_general.calle,
            "numero":domicilio_general.numero,
            "piso":domicilio_general.piso,
            "oficina":domicilio_general.oficina,
            "sector":domicilio_general.sector,
            "torre":domicilio_general.torre,
            "manzana":domicilio_general.manzana,
            "pk":domicilio_general.pk
        } if domicilio_general is not None else {
                    "provincia": "",
                    "localidad":"",
                    "codigo_postal":"",
                    "calle":"",
                    "numero":"",
                    "piso":"",
                    "oficina":"",
                    "sector":"",
                    "torre":"",
                    "manzana":"",
                    "pk":0
                }
        domicilio_arba = DomicilioArba.objects.filter(persona = persona).first()
        domicilio_arba =  {
                    "provincia":domicilio_arba.provincia,
                    "localidad":domicilio_arba.localidad,
                    "codigo_postal":domicilio_arba.codigo_postal,
                    "calle":domicilio_arba.calle,
                    "numero":domicilio_arba.numero,
                    "ruta":domicilio_arba.ruta,
                    "km":domicilio_arba.km,
                    "partido":domicilio_arba.partido,
                    "piso":domicilio_arba.piso,
                    "oficina":domicilio_arba.oficina,
                    "sector":domicilio_arba.sector,
                    "torre":domicilio_arba.torre,
                    "manzana":domicilio_arba.manzana,
                    "pk":domicilio_arba.pk
                }if domicilio_arba is not None else {
                    "provincia": "",
                    "localidad":"",
                    "codigo_postal":"",
                    "calle":"",
                    "numero":"",
                    "ruta":"",
                    "km":"",
                    "partido":"",
                    "piso":"",
                    "oficina":"",
                    "sector":"",
                    "torre":"",
                    "manzana":"",
                    "pk":0
                }
        domicilio_agip = DomicilioAgip.objects.filter(persona = persona).first()
        domicilio_agip = {
                    "calle":domicilio_agip.calle,
                    "numero":domicilio_agip.numero,
                    "barrio":domicilio_agip.barrio,
                    "ruta":domicilio_agip.ruta,
                    "km":domicilio_agip.km,
                    "piso":domicilio_agip.piso,
                    "oficina":domicilio_agip.oficina,
                    "sector":domicilio_agip.sector,
                    "torre":domicilio_agip.torre,
                    "manzana":domicilio_agip.manzana,
                    "pk":domicilio_agip.pk                
                    } if domicilio_agip is not None else {
                    "calle":"",
                    "numero":"",
                    "barrio":"",
                    "ruta":"",
                    "km":"",
                    "piso":"",
                    "oficina":"",
                    "sector":"",
                    "torre":"",
                    "manzana":"",
                    "pk":0
                }

        usuario_vm = {
            "es_no_apto":False,
            "datos_personales":{
                "nombre": persona.nombre,
                "apellido":persona.apellido,
                "email":persona.email,
                "cuit":persona.cuit,
                "direccion":{
                    "domicilio_general":domicilio_general,
                    "domicilio_arba":domicilio_arba,
                    "domicilio_agip":domicilio_agip
                },
                "domicilio_fiscal":persona.email,
                "fecha_registro":usuario.date_joined,
                "telefono":persona.telefono,
                "notificaciones_mail": condicion.recibe_email
            },
            "datos_monotributo":{
                "clave": persona.clave_afip,
                "clave_desactualizada":condicion.clave_afip_desactualizada,
                "categoria":estado_monotributo.categoria,             
                "limite_facturacion":estado_monotributo.limite_facturacion_anual
            },
            "organismo_recaudacion": None if not entidad  else {
                "entidad": entidad,
                "regimen_simplificado":regimen_simplificado,
                "datos":{
                    "clave":clave_organismo_facturacion,
                    "clave_desactualizada":condicion.clave_iibb_desactualizada,
                    "categoria": categoria,
                    "limite_facturacion":agip_persona.limite_facturacion_anual if es_agip else None
                }
            },
            "datos_freelo":{
                "gestion_activa": condicion.gestion_activa_freelo
            }
        }

        return Response({'success':True,'response':usuario_vm},status=status_ok)

class UsuarioVMEditar(APIView):

    def post(self,request,pk):
        usuario = Usuario.objects.filter(pk=pk).first()
        if usuario is None:
            return Response({'success':False,'response':'usuario no existe'},status=status_ok)
        usuariovm = request.data
        es_no_apto = usuariovm.get('es_no_apto')
        if es_no_apto == True:
            persona = usuario.persona
            if es_no_apto == persona.es_apto_uso_sistema:
                return Response({'success':False,'response':'el usuario especificado como no apto es apto'},status=status_ok)
            if persona is None:
                return Response({'success':False,'response':'usuario no tiene vinculada una persona'},status=status_ok)

            datos_personales = usuariovm.get('datos_personales')
            if datos_personales is not None:

                nombre = datos_personales.get('nombre')
                apellido = datos_personales.get('apellido')
                email = datos_personales.get('email')
                #update
                if nombre is not None :
                    persona.nombre = nombre

                if apellido is not None :
                    persona.apellido = apellido

                if email is not None :
                    persona.email = email 

            try:
                persona.save()
            except Exception as e:
                return Response({'success':False,'response':str(e)},status=status_ok)
            return Response({'success':True,'response':'actualización exitosa de usuario no apto'},status=status_ok)

        if es_no_apto == False:
            persona = None
            estado_monotributo = None
            domicilio = None
            domicilio_arba = None
            domicilio_agip = None
            condicion_persona = None
            agip_persona = None
            arba_persona = None
            persona = usuario.persona
            if persona is None:
                return Response({'success':False,'response':'el usuario no tiene una persona vinculada'},status=status_ok)
            es_agip = AgipPersona.objects.filter(persona=persona).exists()
            es_arba = ArbaPersona.objects.filter(persona=persona).exists()
            
            arba_persona = ArbaPersona.objects.get(persona=persona) if es_arba else None
            agip_persona = AgipPersona.objects.get(persona=persona) if es_agip else None

            if es_no_apto == persona.es_apto_uso_sistema:
                return Response({'success':False,'response':'el usuario especificado como apto es no apto'},status=status_ok)
            if persona is None:
                return Response({'success':False,'response':'usuario no tiene vinculada una persona'},status=status_ok)
            condicion_persona = CondicionPersona.objects.filter(persona = persona).first()
            if condicion_persona is None:
                return Response({'success':False,'response':'la persona vinculada al usuario no tiene vinculada una condicion'},status=status_ok)
            datos_personales = usuariovm.get('datos_personales')
            if datos_personales is not None:
                nombre = datos_personales.get("nombre")
                if nombre is not None:
                    persona.nombre = nombre
                apellido = datos_personales.get("apellido")
                if apellido is not None:
                    persona.apellido = apellido
                email = datos_personales.get("email")
                domicilio_fiscal = datos_personales.get('domicilio_fiscal')
                if email != domicilio_fiscal:
                    return Response({'success':False,'response':'email y domicilio deben coincidir'},status=status_ok)
                if email is not None:
                    persona.email = email
                cuit = datos_personales.get("cuit")
                if cuit is not None:
                    persona.cuit = cuit
                fecha_registro = datos_personales.get('fecha_registro')
                if fecha_registro is not None:
                    usuario.date_joined = fecha_registro
                telefono = datos_personales.get('telefono')
                if telefono is not None:
                    persona.telefono = telefono
                condicion_persona.recibe_email = datos_personales.get('notificaciones_mail')
                domicilio_vm = datos_personales.get("domicilio")
                if domicilio_vm is not None:
                    domicilio_general = domicilio_vm.get('domicilio_general')
                    if domicilio_general is not None:
                        pk_domicilio = domicilio_general.get('pk')
                        if pk_domicilio is None or pk_domicilio == "" :
                            return Response({'success':False,'response':'pk domicilio formato incorrecto'},status=status_ok)
                        domicilio = Domicilio.objects.filter(pk = pk_domicilio).first()
                        if domicilio is not None:
                            provincia = domicilio_general.get('provincia')
                            localidad = domicilio_general.get('localidad')
                            codigo_postal = domicilio_general.get('codigo_postal')
                            calle = domicilio_general.get('calle')
                            numero = domicilio_general.get('numero')
                            piso = domicilio_general.get('piso')
                            oficina = domicilio_general.get('oficina')
                            sector = domicilio_general.get('sector')
                            torre = domicilio_general.get('torre')
                            manzana = domicilio_general.get('manzana')
                            if provincia is not None:
                                domicilio.provincia = provincia
                            if localidad is not None:
                                domicilio.localidad = localidad
                            if codigo_postal is not None:
                                domicilio.codigo_postal = codigo_postal
                            if calle is not None:
                                domicilio.calle = calle
                            if numero is not None:
                                domicilio.numero = numero
                            if piso is not None:
                                domicilio.piso = piso
                            if oficina is not None:
                                domicilio.oficina = oficina
                            if sector is not None:
                                domicilio.sector = sector
                            if torre is not None:
                                domicilio.torre = torre
                            if manzana is not None:
                                domicilio.manzana = manzana
                        else:
                            ## crear nuevo domicilio
                            domicilio_nuevo = Domicilio.objects.create(
                                provincia=domicilio_general.get('provincia'),
                                localidad=domicilio_general.get('localidad'),
                                codigo_postal=domicilio_general.get('codigo_postal'),
                                calle=domicilio_general.get('calle'),
                                numero=domicilio_general.get('numero'),
                                piso=domicilio_general.get('piso'),
                                oficina=domicilio_general.get('oficina'),
                                sector=domicilio_general.get('sector'),
                                torre=domicilio_general.get('torre'),
                                manzana=domicilio_general.get('manzana'),
                                persona=persona
                            )
                            domicilio_general.update({'pk':f"{domicilio_nuevo.pk}"})
                            # return Response({'success':False,'response':'domicilio no encontrado con la pk suministrada'},status=status_ok)
                    domicilio_arba_vm = domicilio_vm.get('domicilio_arba')
                    if domicilio_arba_vm is not None:
                        pk_domicilio_arba = domicilio_arba_vm.get('pk')
                        if pk_domicilio_arba is None or pk_domicilio_arba == "" :
                            return Response({'success':False,'response':'pk domicilio arba formato incorrecto'},status=status_ok)
                        domicilio_arba = DomicilioArba.objects.filter(pk = pk_domicilio_arba).first()
                        if domicilio_arba is not None:
                            provincia = domicilio_arba_vm.get('provincia')
                            localidad = domicilio_arba_vm.get('localidad')
                            codigo_postal = domicilio_arba_vm.get('codigo_postal')
                            calle = domicilio_arba_vm.get('calle')
                            numero = domicilio_arba_vm.get('numero')
                            ruta = domicilio_arba_vm.get('ruta')
                            km = domicilio_arba_vm.get('km')
                            partido = domicilio_arba_vm.get('partido')
                            piso = domicilio_arba_vm.get('piso')
                            oficina = domicilio_arba_vm.get('oficina')
                            sector = domicilio_arba_vm.get('sector')
                            torre = domicilio_arba_vm.get('torre')
                            manzana = domicilio_arba_vm.get('manzana')
                            if provincia is not None:
                                domicilio_arba.provincia = provincia
                            if localidad is not None:
                                domicilio_arba.localidad = localidad
                            if codigo_postal is not None:
                                domicilio_arba.codigo_postal = codigo_postal
                            if calle is not None:
                                domicilio_arba.calle = calle
                            if numero is not None:
                                domicilio_arba.numero = numero
                            if ruta is not None:
                                domicilio_arba.ruta = ruta
                            if km is not None:
                                domicilio_arba.km = km
                            if partido is not None:
                                domicilio_arba.partido = partido
                            if piso is not None:
                                domicilio_arba.piso = piso
                            if oficina is not None:
                                domicilio_arba.oficina = oficina
                            if sector is not None:
                                domicilio_arba.sector = sector
                            if torre is not None:
                                domicilio_arba.torre = torre
                            if manzana is not None:
                                domicilio_arba.manzana = manzana
                        else:
                            return Response({'success':False,'response':'domicilio arba no encontrado con la pk suministrada'},status=status_ok)
                    domicilio_agip_vm = domicilio_vm.get('domicilio_agip')
                    if domicilio_agip_vm is not None:
                        pk_domicilio_agip = domicilio_agip_vm.get('pk') 
                        if pk_domicilio_agip is None or pk_domicilio_agip == "" :
                            return Response({'success':False,'response':'pk domicilio agip formato incorrecto'},status=status_ok)

                        domicilio_agip = DomicilioAgip.objects.filter(pk = pk_domicilio_agip).first()
                        if domicilio_agip is not None:
                            calle = domicilio_agip_vm.get('calle')
                            numero = domicilio_agip_vm.get('numero')
                            barrio = domicilio_agip_vm.get('barrio')
                            ruta = domicilio_agip_vm.get('ruta')
                            km = domicilio_agip_vm.get('km')
                            piso = domicilio_agip_vm.get('piso')
                            oficina = domicilio_agip_vm.get('oficina')
                            sector = domicilio_agip_vm.get('sector')
                            torre = domicilio_agip_vm.get('torre')
                            manzana = domicilio_agip_vm.get('manzana')
                            if calle is not None:
                                domicilio_agip.calle = calle
                            if numero is not None:
                                domicilio_agip.numero = numero
                            if barrio is not None:
                                domicilio_agip.barrio = barrio
                            if ruta is not None:
                                domicilio_agip.ruta = ruta
                            if km is not None:
                                domicilio_agip.km = km
                            if piso is not None:
                                domicilio_agip.piso = piso
                            if oficina is not None:
                                domicilio_agip.oficina = oficina
                            if sector is not None:
                                domicilio_agip.sector = sector
                            if torre is not None:
                                domicilio_agip.torre = torre
                            if manzana is not None:
                                domicilio_agip.manzana = manzana
                        else:
                            return Response({'success':False,'response':'domicilio agipb no encontrado con la pk suministrada'},status=status_ok)
            datos_monotributo_vm = usuariovm.get('datos_monotributo')
            estado_monotributo = EstadoMonotributo.objects.filter(persona = persona).first()
            if estado_monotributo is None:
                return Response({'success':False,'response':'la persona vinculada al usuario no tiene vinculado un estado monotributo'},status=status_ok)
            if datos_monotributo_vm is not None:
                condicion_persona.es_monotributista = True
                clave = datos_monotributo_vm.get('clave')
                if clave is not None:
                    persona.clave_afip = clave
                clave_desactualizada = datos_monotributo_vm.get('clave_desactualizada')
                if clave_desactualizada is not None:
                    condicion_persona.clave_afip_desactualizada = clave_desactualizada
                categoria = datos_monotributo_vm.get('categoria')
                if categoria is not None:
                    estado_monotributo.categoria = categoria
                limite_facturacion = datos_monotributo_vm.get('limite_facturacion') 
                if limite_facturacion is not None:
                    estado_monotributo.limite_facturacion_anual = limite_facturacion
            organismo_recaudacion_vm = usuariovm.get('organismo_recaudacion')
            if organismo_recaudacion_vm is not None:
                entidad = organismo_recaudacion_vm.get('entidad')  #arba|agip
                if not entidad in ['arba','agip'] :
                    return Response({'success':False,'response':'la entidad especificada en organismo recaudacion debe ser arba o agip'},status=status_ok)
                if entidad == 'arba' and not es_arba:
                    return Response({'success':False,'response':'la entidad especificada es arba sin embargo la persona no tiene asociada una persona arba'},status=status_ok)
                if entidad == 'agip' and not es_agip:
                    return Response({'success':False,'response':'la entidad especificada es agip sin embargo la persona no tiene asociada una persona agip'},status=status_ok)
                regimen_simplificado = organismo_recaudacion_vm.get('regimen_simplificado') #bool
                if regimen_simplificado is not None:
                    agip_persona.regimen_simplificado = regimen_simplificado
                datos_vm = organismo_recaudacion_vm.get('datos')
                if datos_vm is not None:
                    clave = datos_vm.get('clave')
                    if clave is not None:
                        if entidad == 'agip':
                            agip_persona.clave_fiscal_agip = clave
                        if entidad == 'arba':
                            arba_persona.clave_fiscal_arba = clave
                    clave_desactualizada = datos_vm.get('clave_desactualizada')
                    if clave_desactualizada is not None:
                        condicion_persona.clave_iibb_desactualizada = clave_desactualizada
                    categoria = datos_vm.get('categoria')
                    if categoria is not None:
                        if entidad == 'agip':
                            agip_persona.categoria_agip = categoria
                        if entidad == 'arba':
                            return Response({'success':False,'response':'persona arba no acepta el valor categoría'},status=status_ok)
                    limite_facturacion = datos_vm.get('limite_facturacion')
                    if limite_facturacion is not None:
                        if entidad == 'agip':
                            agip_persona.limite_facturacion_anual = limite_facturacion
                        if entidad == 'arba':
                            return Response({'success':False,'response':'persona arba no acepta el límite facturación'},status=status_ok)
            datos_freelo_vm = usuariovm.get('datos_freelo')
            if datos_freelo_vm is not None:
                gestion_activa_vm = datos_freelo_vm.get('gestion_activa')
                if gestion_activa_vm is not None:
                    condicion_persona.gestion_activa_freelo = gestion_activa_vm
            #guardado transaccional
            try:
                with transaction.atomic():
                    if persona is not None:
                        entidad_guardada = 'persona'
                        persona.save()
                    if condicion_persona is not None:
                        entidad_guardada = 'condicion persona'
                        condicion_persona.save()
                    if estado_monotributo is not None:
                        entidad_guardada = 'estado monotributo'
                        estado_monotributo.save()
                    if domicilio is not None:
                        entidad_guardada = 'domicilio'
                        domicilio.save()
                    if domicilio_arba is not None:
                        entidad_guardada = 'domicilio arba '
                        domicilio_arba.save()
                    if domicilio_agip is not None:
                        entidad_guardada = 'domicilio agip'
                        domicilio_agip.save()
                    if agip_persona is not None:
                        entidad_guardada = 'agip persona'
                        agip_persona.save()
                    if arba_persona is not None:
                        entidad_guardada = 'arba persona'
                        arba_persona.save()
                return Response({'success':True,'response':usuariovm},status=status_ok)
            except Exception as e:
                return Response({'success':False,'response':f"{entidad_guardada}: {str(e)}"},status=status_ok)

class UsuarioVMEliminar(APIView):

    def post(self,request,pk):
        usuario = get_object_or_404(Usuario, pk=pk)
        persona = usuario.persona

        try:
            with transaction.atomic():
                if persona is not None:
                    persona.delete()
                else:
                    usuario.delete()
                
                return Response({'success':True,'response':"eliminado exitoso"},status=status_ok)

        except Exception as e:
            return Response({'success':False,'response':f"{str(e)}"},status=status_ok)

class FacturacionFreeloEliminar(APIView):

    def post(self,request,pk):
        factura = get_object_or_404(FacturacionFreelo, pk=pk)
        try:
            with transaction.atomic():
                if FacturacionMonotributo.objects.filter(persona=factura.persona).filter(periodo=factura.periodo).filter(concepto="Facturacion").exists():
                    factura_instancia = FacturacionMonotributo.objects.filter(persona=factura.persona).filter(periodo=factura.periodo).filter(concepto="Facturacion").first()
                    factura_instancia.delete()
                factura.delete()
                return Response({'success':True,'response':"eliminado exitoso"},status=status_ok)

        except Exception as e:
            return Response({'success':False,'response':f"{str(e)}"},status=status_ok)

class FacturacionMonotributoEliminar(APIView):

    def post(self,request,pk):
        factura = get_object_or_404(FacturacionMonotributo, pk=pk)
        try:
            with transaction.atomic():
                factura.delete()
                return Response({'success':True,'response':"eliminado exitoso"},status=status_ok)

        except Exception as e:
            return Response({'success':False,'response':f"{str(e)}"},status=status_ok)

class FacturacionIIBBEliminar(APIView):

    def post(self,request,pk):
        factura = FacturacionIIBB.objects.filter(pk = pk).first()
        if factura is None:
            factura = get_object_or_404(FacturacionIIBB, pk=pk)
        
        try:
            with transaction.atomic():
                factura.delete()
                return Response({'success':True,'response':"eliminado exitoso"},status=status_ok)

        except Exception as e:
            return Response({'success':False,'response':f"{str(e)}"},status=status_ok)


