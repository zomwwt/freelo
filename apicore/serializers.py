from rest_framework import serializers
from .models import *
from django.core.mail import send_mail, EmailMultiAlternatives
from .tokens import account_activation_token
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.html import strip_tags
from django.utils.encoding import force_bytes, force_text
from django.conf import settings
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
email_emisor = settings.EMAIL_HOST_USER

class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Persona
        fields = ('nombre', 'apellido', 'email', 'telefono', 'cuit','clave_afip',)
        extra_kwargs = {
            'nombre': {'required': False},
            'apellido': {'required': False},
            'email': {'required': False},
            'telefono': {'required': False},
            'cuit': {'required': False},
            'clave_afip': {'required': False},
            }
class CondicionPersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = CondicionPersona
        fields = ('gestion_activa_freelo','declara_iibb','es_monotributista','recibe_email','persona',)

class EstadoMonotributoSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoMonotributo
        fields = ('limite_facturacion_anual','persona','estado_monotributo')

class FacturacionFreeloSerializer(serializers.ModelSerializer):
    class Meta:
        model = FacturacionFreelo
        fields = ('monto','mes_facturado','fecha_vencimiento','persona','pago_realizado')

class FacturacionMonotributoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FacturacionMonotributo
        fields = ('monto','mes_facturado','persona','fecha_vencimiento','cur_pdf','pago_realizado')

class FacturacionIIBBSerializer(serializers.ModelSerializer):
    class Meta:
        model = FacturacionIIBB
        fields = ('monto_deuda','mes_facturado','retencion','persona','fecha_vencimiento','pago_realizado','vep_pdf')

class ActividadesMonotributoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActividadesMonotributo
        fields = ('codigo', 'actividad')

class ActividadesArbaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActividadesArba
        fields = ('codigo', 'actividad')

class ActividadesAgipSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActividadesAgip
        fields = ('codigo', 'actividad')

class ObraSocialSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObraSocial
        fields = ('codigo','denominacion')

class DomicilioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Domicilio
        fields = ('provincia','localidad','codigo_postal','calle','numero','piso','oficina','sector','torre','manzana',)

class DomicilioArbaSerializer(serializers.ModelSerializer):
    class Meta:
        model = DomicilioArba
        fields = ('provincia','localidad','partido','ruta','km','codigo_postal','calle','numero','piso','oficina','sector','torre','manzana',)

class ArbaPersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ArbaPersona
        fields = ('fecha_nacimiento','sexo','estado_civil','nacionalidad','clave_fiscal_arba')

class AgipPersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgipPersona
        fields = ('categoria_agip','regimen_simplificado','clave_fiscal_agip')

class DomicilioAgipSerializer(serializers.ModelSerializer):
    class Meta:
        model = DomicilioAgip
        fields = ('ruta','km','barrio','calle','numero','piso','oficina','sector','torre','manzana',)

class AfipPersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = AfipPersona
        fields = ('alta_afip','actividades_en_otras_jurisdicciones','obra_social','cuit_empleador','cuit_caja','ley_jubilado','mes_actual_inicio_actividades','facturacion_anual_estimada')

class RegistroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('email','password','first_name','last_name')
    ## en el mensaje se puede embeber un html con el contenido en un futuro
    def crear_usuario(self,url_sitio):
        

        user = Usuario(email=self.validated_data['email'])
        user.password = self.validated_data['password']
        user.first_name = self.validated_data['first_name']
        user.last_name = self.validated_data['last_name']
        user.set_password(self.validated_data['password'])
        user.is_active = False
        user.save()
        ## crear un registro en personas y vincular al usuario registrado
        ## por ser cuit único, se genera una persona con la pk del usuario temporalmente
        persona = Persona.objects.create(nombre=self.validated_data['first_name'],apellido=self.validated_data['last_name'],email=self.validated_data['email'],cuit=user.pk)
        persona.es_apto_uso_sistema = True
        persona.save()

        ## Se crea por defecto una condicion de las personas en freelo
        CondicionPersona.objects.create(persona=persona)
        user.persona = persona
        user.save()
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        asunto = "Email verificacion de cuenta"
        
        token = account_activation_token.make_token(user)
        link_activacion = "{}/{}/{}".format(url_sitio,uid,token)
        Token.objects.create(usuario=user, token=token)
        ## el link se deberá modificar acorde al deploy final del front end, ahi es donde deberá redirigir 
        html_mensaje = render_to_string('templates/ConfirmacionCuenta.html',
        {
            'url':link_activacion
        })
        mensaje_plano = strip_tags(html_mensaje)
        email_receptor = [user.email]
        email_armado = EmailMultiAlternatives(asunto, mensaje_plano, email_emisor, email_receptor)
        email_armado.attach_alternative(html_mensaje, "text/html")
        email_armado.send()
        ## notificar por mail nuevo usuario registrado
        asunto_nuevo_usuario = "Nuevo usuario registrado!"
        mensaje_nuevo_usuario = "El Usuario: " + user.get_full_name() + ", email: " + user.email + ", acaba de registrarse en freelo."
        email_receptor_emisor = [email_emisor]
        send_mail(asunto_nuevo_usuario, mensaje_nuevo_usuario, email_emisor, email_receptor_emisor)
        return user

class ObtenerTokenSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        # Extra data para login
        if self.user.persona is not None:
            condicion_persona = CondicionPersona.objects.get(persona=self.user.persona)
            data['nombre'] = self.user.persona.nombre
            data['apellido'] = self.user.persona.apellido
            data['email'] =self.user.persona.email
            data['telefono'] = self.user.persona.telefono
            data['cuit']=self.user.persona.cuit
            data['es_admin']=self.user.is_staff
            data['recibe_mail'] = condicion_persona.recibe_email
            data['es_apto_uso_sistema'] =self.user.persona.es_apto_uso_sistema
            data['primer_login'] = self.user.persona.primer_login
        else:
            data['nombre'] = self.user.first_name
            data['apellido'] = self.user.last_name
            data['email'] =self.user.email
            data['es_admin']=self.user.is_staff
        return data

class CambiarContraseñaSerializer(serializers.Serializer):
    model = Usuario
    contraseña_actual = serializers.CharField(required=True)
    contraseña_nueva = serializers.CharField(required=True)
        