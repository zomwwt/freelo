#!/usr/bin/env bash
# start-server.sh
python manage.py migrate
(gunicorn FreeloApp.wsgi --user nginx --bind 0.0.0.0:8000 --limit-request-line 8190 --workers 3) &
nginx -g "daemon off;"
